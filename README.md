# Configuración del proyecto

### Configuración del proxy

El servidor con el API del backend para el entorno de desarrollo se configura en el fichero: **proxy.config.json** 

Se debe crear este fichero a partir de la plantilla: **proxy.config.json.template**, modificando el campo: *target*

### Configuración del entorno de producción

El entorno de producción se configura en el fichero: **src/environments/environment.prod.ts** 

Se debe crear este fichero a partir de la plantilla: **src/environments/environment.prod.ts.template**, modificando el campo: *baseUrl*

### Configuración de las herramientas de encriptación

Editar el fichero: **node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js** y cambiar la línea:

```
// old:
node: false,

// new:
node: { crypto: true, stream: true },
```

Más información: [https://github.com/tensorflow/tfjs/issues/494](https://github.com/tensorflow/tfjs/issues/494)

### Configuración de las personalizaciones

Consulte el fichero: **src/app/extensions/Readme.md**

### Configuración de Angular

Es necesario tener localmente instalado en Angular CLI. Seguir los pasos de: [https://angular.io/guide/setup-local](https://angular.io/guide/setup-local)
 
Una vez instalado el Angular:

```shell
cd vincofw_front
npm install
```

### Ejecutar la aplicación en desarrollo

```shell
npm start
```


