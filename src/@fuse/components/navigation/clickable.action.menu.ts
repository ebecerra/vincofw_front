import {Router} from "@angular/router";
import {AppCacheService} from "../../../app/modules/vincofw/services/cache.service";
import {Input} from "@angular/core";
import {FuseNavigationItem} from "../../types";
import {AdMenuModel} from "../../../app/modules/vincofw/models/ad.menu.model";

export class ClickableActionMenu {

    @Input()
    item: FuseNavigationItem;

    constructor(
        protected _router: Router,
        protected _cacheService: AppCacheService
    ) {

    }

    public navigateToMenu(adMenu: AdMenuModel) {
        this._router.navigate(['/tab', adMenu.idMenu]);
    }
}