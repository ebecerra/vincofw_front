import { Component, HostBinding, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import {Router} from "@angular/router";
import {ClickableActionMenu} from "../../clickable.action.menu";
import {AppCacheService} from "../../../../../app/modules/vincofw/services/cache.service";
import {AppCoreSettings} from "../../../../../app/modules/vincofw/configs/app.core.settings";

@Component({
    selector   : 'fuse-nav-horizontal-collapsable',
    templateUrl: './collapsable.component.html',
    styleUrls  : ['./collapsable.component.scss'],
    animations : fuseAnimations
})
export class FuseNavHorizontalCollapsableComponent extends ClickableActionMenu implements OnInit, OnDestroy
{
    fuseConfig: any;
    isOpen = false;
    public activeLanguage = null;

    @HostBinding('class')
    classes = 'nav-collapsable nav-item';

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        protected _router: Router,
        protected _cacheService: AppCacheService,
        private _coreSettings: AppCoreSettings,
        private _fuseConfigService: FuseConfigService
    ) {
        super(_router, _cacheService);
        this.activeLanguage = "";
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._cacheService.onMessagesReloaded.subscribe(() => {
            this.activeLanguage = this._coreSettings.idLanguage;
        });
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (config) => {
                    this.fuseConfig = config;
                }
            );
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Open
     */
    @HostListener('mouseenter')
    open(): void
    {
        this.isOpen = true;
    }

    /**
     * Close
     */
    @HostListener('mouseleave')
    close(): void
    {
        this.isOpen = false;
    }
}
