import { Component, HostBinding } from '@angular/core';
import {ClickableActionMenu} from "../../clickable.action.menu";
import {Router} from "@angular/router";
import {AppCacheService} from "../../../../../app/modules/vincofw/services/cache.service";

@Component({
    selector   : 'fuse-nav-horizontal-item',
    templateUrl: './item.component.html',
    styleUrls  : ['./item.component.scss']
})
export class FuseNavHorizontalItemComponent extends ClickableActionMenu
{
    @HostBinding('class')
    classes = 'nav-item';

    /**
     * Constructor
     */
    constructor(
        _router: Router,
        protected _cacheService: AppCacheService
    ) {
        super(_router, _cacheService);
    }
}
