import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {merge, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseNavigationService} from '@fuse/components/navigation/navigation.service';
import {FuseNavigation, FuseNavigationItem} from "../../types";
import {isNullOrUndefined} from "util";
import {AdMenuModel} from "../../../app/modules/vincofw/models/ad.menu.model";
import {AppCoreSettings} from "../../../app/modules/vincofw/configs/app.core.settings";
import {AdMenuService} from "../../../app/modules/vincofw/services/ad.menu.service";
import {AuthService} from "../../../app/modules/vincofw/authentication/auth.service";

@Component({
    selector       : 'fuse-navigation',
    templateUrl    : './navigation.component.html',
    styleUrls      : ['./navigation.component.scss'],
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FuseNavigationComponent implements OnInit
{
    @Input()
    layout = 'vertical';

    @Input()
    navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseNavigationService: FuseNavigationService,
        private _authService: AuthService,
        private _menuService: AdMenuService,
        private _settings: AppCoreSettings
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this._menuService.listMenus(this._authService.selectedRole, this._settings.idLanguage).subscribe(menus => {
            this.setMainNavigation(menus);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Load the navigation either from the input or from the service
        this.navigation = this.navigation || this._fuseNavigationService.getCurrentNavigation();

        // Subscribe to the current navigation changes
        this._fuseNavigationService.onNavigationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {

                // Load the navigation
                this.navigation = this._fuseNavigationService.getCurrentNavigation();

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Subscribe to navigation item
        merge(
            this._fuseNavigationService.onNavigationItemAdded,
            this._fuseNavigationService.onNavigationItemUpdated,
            this._fuseNavigationService.onNavigationItemRemoved
        ).pipe(takeUntil(this._unsubscribeAll))
         .subscribe(() => {

             // Mark for check
             this._changeDetectorRef.markForCheck();
         });
    }

    private setMainNavigation(menuResult: AdMenuModel[]) {
        // Register the navigation to the service
        this._fuseNavigationService.unregister('main');
        let newNavigation = this.transformMenu(menuResult);
        // Register the navigation to the service
        this._fuseNavigationService.register('main', newNavigation);
        // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation('main');
    }

    private transformMenu(menuResult: AdMenuModel[]): FuseNavigation[]  {
        let navigation: FuseNavigation[] = [];
        menuResult.forEach(current => {
            let newNav = {
                id       : current.value,
                title    : current.name,
                translate: current.name,
                relatedMenu: current,
                icon     : current.icon,
                type     :  (isNullOrUndefined(current.subMenus) || current.subMenus.length === 0)
                    ? 'item' : 'collapsable',
                children : this.transformSubmenu(current.subMenus)
            } as FuseNavigation;
            navigation.push(newNav);
        });
        return navigation;
    }

    private transformSubmenu(subMenus: AdMenuModel[]) {
        let navigation: FuseNavigationItem[] = [];
        subMenus.forEach(current => {
            let newNav = {
                id       : current.value,
                title    : current.name,
                translate: current.name,
                relatedMenu: current,
                icon     : current.icon,
                // url      : '/sample', // TODO ,
                type     :  (isNullOrUndefined(current.subMenus) || current.subMenus.length === 0)
                    ? 'item' : 'collapsable',
                children : this.transformSubmenu(current.subMenus)
            } as FuseNavigationItem;
            navigation.push(newNav);
        });
        return navigation;
    }
}
