import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Platform} from '@angular/cdk/platform';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FuseConfigService} from '@fuse/services/config.service';
import {FuseNavigationService} from '@fuse/components/navigation/navigation.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseSplashScreenService} from '@fuse/services/splash-screen.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseConfig} from '../@fuse/types';
import {isNullOrUndefined} from 'util';
import {AuthService} from './modules/vincofw/authentication/auth.service';
import {AppCacheService} from './modules/vincofw/services/cache.service';
import {GlobalEventsService} from './modules/vincofw/services/global.events.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    fuseConfig: any;
    navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    // Public
    public userLogged: boolean;

    constructor(
        @Inject(DOCUMENT) private document: any,
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _platform: Platform,
        private _authService: AuthService,
        private _cacheService: AppCacheService,
        private _globalEventsService: GlobalEventsService
    ) {
        this.userLogged = this._authService.isLogged(true);
        this._authService.loginModified$.subscribe(next => {
            this.userLogged = this._authService.isLogged();
            if (this.userLogged) {
                this.setConfigLayout();
            }
        });
        // Add languages
        this._translateService.addLangs(['en']);

        // Set the default language
        this._translateService.setDefaultLang('en');

        // Use a language
        this._translateService.use('en');

        // Add is-mobile class to the body if the platform is mobile
        if (this._platform.ANDROID || this._platform.IOS) {
            this.document.body.classList.add('is-mobile');
        }

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to config changes
        this._fuseConfigService.config.pipe(takeUntil(this._unsubscribeAll)).subscribe((config) => {
            this.fuseConfig = config;

            // Boxed
            if (this.fuseConfig.layout.width === 'boxed') {
                this.document.body.classList.add('boxed');
            } else {
                this.document.body.classList.remove('boxed');
            }

            // Color theme - Use normal for loop for IE11 compatibility
            for (let i = 0; i < this.document.body.classList.length; i++) {
                const className = this.document.body.classList[i];

                if (className.startsWith('theme-')) {
                    this.document.body.classList.remove(className);
                }
            }

            this.document.body.classList.add(this.fuseConfig.colorTheme);
        });

        this.setConfigLayout();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    private setConfigLayout(): void {
        this._authService.getLoggedClient().then(client => {
            if (!isNullOrUndefined(client)) {
                const fuseConfig: FuseConfig = {
                    // Color themes can be defined in src/app/app.theme.scss
                    colorTheme: client.guiColorTheme,
                    customScrollbars: true,
                    layout: {
                        style: 'vertical-layout-1',
                        width: client.guiWidth === 'fullwidth'  ? 'fullwidth' : 'boxed',
                        navbar: {
                            primaryBackground: client.navbarPrimaryColor + '-' + client.navbarPrimaryColorVariant,
                            secondaryBackground: client.navbarSecondaryColor + '-' + client.navbarSecondaryColorVariant,
                            folded: false,
                            hidden: false,
                            position: client.navbarPosition === 'left' ? 'left' :  (client.navbarPosition === 'right' ? 'right' : 'top'),
                            variant: 'vertical-style-1'
                        },
                        toolbar: {
                            customBackgroundColor: false,
                            background: client.toolbarColor + '-' + client.toolbarColorVariant,
                            hidden: false,
                            position: client.toolbarPosition === 'above' ? 'above' :
                                (client.toolbarPosition === 'above-static' ? 'above-static' :
                                    (client.toolbarPosition === 'above-fixed' ? 'above-fixed' :
                                        (client.toolbarPosition === 'below' ? 'below' :
                                            (client.toolbarPosition === 'below-static' ? 'below-static' : 'below-fixed'))))
                        },
                        footer: {
                            customBackgroundColor: true,
                            background: client.footerColor + '-' + client.footerColorVariant,
                            hidden: client.footerHidden,
                            position: client.footerPosition === 'above' ? 'above' :
                                (client.footerPosition === 'above-static' ? 'above-static' :
                                    (client.footerPosition === 'above-fixed' ? 'above-fixed' :
                                        (client.footerPosition === 'below' ? 'below' :
                                            (client.footerPosition === 'below-static' ? 'below-static' : 'below-fixed'))))
                        },
                        sidepanel: {
                            hidden: false,
                            position: 'right'
                        }
                    }
                };

                this._fuseConfigService.config = fuseConfig;

                console.log('App is loaded and ready');
                this._globalEventsService.notifyAppStarted();
            }
        });
    }
}
