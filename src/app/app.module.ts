import {NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {TranslateModule} from '@ngx-translate/core';
import 'hammerjs';
import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';
import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {AppRoutingModule} from './modules/routing.module';
import {AuthGuard} from './modules/vincofw/authentication/auth.guard.service';
import {LoginModule} from './modules/login/login.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RequestLoadingInterceptor} from './navigation/request.loading.interceptor';
import {AuthService} from './modules/vincofw/authentication/auth.service';
import {AdUserService} from './modules/vincofw/services/ad.user.service';
import {AdLanguageService} from './modules/vincofw/services/ad.language.service';
import {AppCoreSettings} from './modules/vincofw/configs/app.core.settings';
import {BaseSettingsModel} from './modules/vincofw/models/base.settings.models';
import {AuthHeaderInterceptor} from './modules/vincofw/authentication/auth.header.interceptor';
import {AppLoadModule} from './app.load.module';
import {fuseConfig} from 'app/fuse-config';
import {MaterialModule} from './material.module';
import {MatIconRegistry} from '@angular/material/icon';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {APP_CONFIG, DATE_FORMATS, DATE_LOCALE} from './extensions/custom/custom.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    providers: [
        AuthGuard,
        AuthService,
        AdUserService,
        AppCoreSettings,
        AdLanguageService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthHeaderInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: RequestLoadingInterceptor, multi: true },
        { provide: BaseSettingsModel, useValue: APP_CONFIG },
        { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS },
        { provide: MAT_DATE_LOCALE, useValue: DATE_LOCALE || 'es-ES'},
    ],
    imports: [
        AppLoadModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FlexLayoutModule,
        RouterModule,
        AppRoutingModule,
        TranslateModule.forRoot(),
        // Material moment date module
        MatMomentDateModule,

        // Material
        MaterialModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        LoginModule
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
    constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
        matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
    }
}
