import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {AppCacheService} from '../../../../../../modules/vincofw/services/cache.service';
import {AdMenuService} from "../../../../../../modules/vincofw/services/ad.menu.service";
import {AdMenuModel} from "../../../../../../modules/vincofw/models/ad.menu.model";
import {AppCoreSettings} from "../../../../../../modules/vincofw/configs/app.core.settings";

@Component({
    selector   : 'nav-menu-filter',
    templateUrl: './menu.filter.component.html',
    styleUrls  : ['./menu.filter.component.scss']
})
export class MenuFilterComponent implements OnInit, OnDestroy {

    // Private
    private _unsubscribeAll: Subject<any>;

    search: string = '';
    placeholder: string;
    menu: AdMenuModel[] = [];
    show: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _coreSettings: AppCoreSettings,
        private _cacheService: AppCacheService,
        private _menuService: AdMenuService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.show = this._coreSettings.settings.SHOW_MENU_FILTER;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.placeholder = this._cacheService.getTranslation('AD_btnSearch');
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    clearSearch() {
        this.search = '';
        this.menu = [];
    }

    searchMenu() {
        this.menu = this.search.length > 1 ? this._menuService.findMenu(this.search) : [];
    }

    navigateToMenu(item) {
        this._router.navigate(['/tab', item.idMenu]);
    }

}
