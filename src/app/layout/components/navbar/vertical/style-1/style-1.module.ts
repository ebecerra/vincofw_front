import {NgModule} from '@angular/core';
import {MatButtonModule, MatIconModule} from '@angular/material';
import {FuseNavigationModule} from '@fuse/components';
import {FuseSharedModule} from '@fuse/shared.module';
import {NavbarVerticalStyle1Component} from 'app/layout/components/navbar/vertical/style-1/style-1.component';
import {TranslateModule} from '@ngx-translate/core';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MenuFilterComponent} from './menu.filter/menu.filter.component';

@NgModule({
    declarations: [
        NavbarVerticalStyle1Component,
        MenuFilterComponent
    ],
    imports: [
        MatButtonModule,
        TranslateModule.forChild(),
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        ReactiveFormsModule,

        FuseSharedModule,
        FuseNavigationModule
    ],
    exports     : [
        NavbarVerticalStyle1Component
    ]
})
export class NavbarVerticalStyle1Module
{
}
