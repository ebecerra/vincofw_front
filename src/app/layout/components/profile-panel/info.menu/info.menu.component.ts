import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AppCacheService} from '../../../../modules/vincofw/services/cache.service';
import {AppCoreSettings} from '../../../../modules/vincofw/configs/app.core.settings';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdUserService} from '../../../../modules/vincofw/services/ad.user.service';
import {AuthService} from '../../../../modules/vincofw/authentication/auth.service';
import {AppErrorStateMatcher} from '../../../../services/app.error.state.matcher';
import {AdUserModel} from '../../../../modules/vincofw/models/ad.user.model';
import {UtilitiesService} from '../../../../modules/vincofw/services/utilities.service';
import {NotificationService} from '../../../../modules/vincofw/notification/notification.service';
import {Router} from '@angular/router';

class UserInfo {
    name: string;
    email: string;
    phone: string;
    phoneMobile: string;
    fax: string;
}

@Component({
    selector: 'info-menu',
    templateUrl: './info.menu.component.html',
    styleUrls: ['./info.menu.component.scss']
})
export class InfoMenuComponent implements OnInit, OnDestroy {

    infoForm: FormGroup;
    matcher = new AppErrorStateMatcher();
    user: AdUserModel = null;
    userInfo: UserInfo;
    message: string = null;

    @Output() close: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private _cacheService: AppCacheService,
        private _settings: AppCoreSettings,
        private _authService: AuthService,
        private _notificationService: NotificationService,
        private _userService: AdUserService,
        private _utilitiesService: UtilitiesService,
        private _router: Router
    ) {
        this.infoForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            email: new FormControl(),
            phone: new FormControl(),
            phoneMobile: new FormControl(),
            fax: new FormControl()
        });
    }

    ngOnInit(): void {
        let userInfo = this._authService.getUserInfo();
        this._userService.get(userInfo.idUserLogged).subscribe(user => {
            this.user = user;
            this.userInfo = {
                name: user.name,
                email: user.email,
                phone: user.phone,
                phoneMobile: user.phoneMobile,
                fax: user.fax
            };
            this._utilitiesService.initFormValues(this.infoForm, this.userInfo);
        });
    }

    ngOnDestroy(): void {
    }

    confirmChanges() {
        this.message = null;
        let userToSave = this._utilitiesService.getItemToSave(this.infoForm, this.userInfo);
        if (userToSave) {
            this.user.name = userToSave.name;
            this.user.email = userToSave.email;
            this.user.phone = userToSave.phone;
            this.user.phoneMobile = userToSave.phoneMobile;
            this.user.fax = userToSave.fax;
            this._userService.updateProfile(this.user).subscribe(() => {
                this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
                this._authService.clearCredentials();
                this.close.emit();
                window.location.reload();
            },
            error => {
                this.message = this._utilitiesService.getValidationsErrors(error.error || error, null, null);
                console.log(error);
            });
        }
    }
}
