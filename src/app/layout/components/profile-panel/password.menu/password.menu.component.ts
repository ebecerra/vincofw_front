import {AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppErrorStateMatcher} from '../../../../services/app.error.state.matcher';
import {AuthService} from '../../../../modules/vincofw/authentication/auth.service';
import {AppCacheService} from '../../../../modules/vincofw/services/cache.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../../../modules/vincofw/notification/notification.service';

@Component({
    selector: 'password-menu',
    templateUrl: './password.menu.component.html',
    styleUrls: ['./password.menu.component.scss']
})
export class PasswordMenuComponent implements OnInit, AfterViewInit, OnDestroy {
    passwordForm: FormGroup;
    matcher = new AppErrorStateMatcher();
    passwordChanged = false;
    passwordChangeError = false;
    serverError = false;
    message = '';

    @Output() close: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private _authService: AuthService,
        private _cacheService: AppCacheService,
        private _notificationService: NotificationService,
        private _router: Router
    ) {
        this.createFormGroup();
    }

    /**
     * On init
     */
    ngOnInit(): void {
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
    }

    createFormGroup() {
        this.passwordForm = new FormGroup({
            oldPassword: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl()
        });
        this.passwordForm.controls.confirmPassword.setValidators([
            Validators.required, this.notEquals.bind(this)
        ]);

        this.passwordForm.controls['password'].valueChanges.subscribe(data => {
            this.passwordForm.controls['confirmPassword'].updateValueAndValidity();
        });
    }

    notEquals( control: FormControl): { [s: string]: boolean} {
        if (control.value !== this.password.value) {
            return {
                notEquals: true
            }
        }

        return null;
    }

    confirmChanges() {
        this.passwordChanged = false;
        this.passwordChangeError = false;
        this.serverError = false;
        this.passwordForm.updateValueAndValidity();
        Object.keys(this.passwordForm.controls).forEach(key => {
            this.passwordForm.controls[key].markAsTouched();
            this.passwordForm.controls[key].markAsDirty();
        });
        if (this.passwordForm.valid) {
            const security = this._cacheService.getPreferenceValue('PasswordCheckMode', 'BASIC');
            this._authService.changePassword(this.oldPassword.value, this.password.value, security).subscribe(result => {
                if (result.success) {
                    this.passwordChanged = true;
                    this.passwordChangeError = false;
                    this.passwordForm.reset({
                        oldPassword: '',
                        password: '',
                        confirmPassword: ''
                    });
                    Object.keys(this.passwordForm.controls).forEach(key => {
                        this.passwordForm.get(key).setErrors(null);
                    });
                    this._notificationService.showSuccess(this._cacheService.getTranslation('AD_GlobalPasswordChanged'));
                    this.close.emit();
                } else {
                    if (result.message === 'not_logged') {
                        this._router.navigate(['/nopage']).then();
                    } else {
                        this.serverError = true;
                        this.message = result.message;
                    }
                }
            }, async error => {
                this.serverError = true;
                this.message = error.error.message;
            });
        }
    }

    get oldPassword() {
        return this.passwordForm.get('oldPassword');
    }

    get password() {
        return this.passwordForm.get('password');
    }

    get confirmPassword() {
        return this.passwordForm.get('confirmPassword');
    }
}
