import {NgModule} from '@angular/core';
import {FuseSharedModule} from '@fuse/shared.module';
import {ProfilePanelComponent} from 'app/layout/components/profile-panel/profile-panel.component';
import {InfoMenuComponent} from './info.menu/info.menu.component';
import {PasswordMenuComponent} from './password.menu/password.menu.component';
import {ProfileMenuComponent} from './profile.menu/profile.menu.component';
import {VincofwModule} from '../../../modules/vincofw/vincofw.module';
import {MaterialModule} from '../../../material.module';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    declarations: [
        ProfilePanelComponent,
        InfoMenuComponent,
        PasswordMenuComponent,
        ProfileMenuComponent
    ],
    providers: [],
    imports: [
        FuseSharedModule,
        PipesModule,
        VincofwModule,
        MaterialModule
    ],
    exports: [
        ProfilePanelComponent,
        InfoMenuComponent,
        PasswordMenuComponent,
        ProfileMenuComponent
    ]
})
export class ProfilePanelModule {
}
