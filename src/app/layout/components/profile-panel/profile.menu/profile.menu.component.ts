import {AfterViewChecked, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {MatCheckboxChange} from '@angular/material';
import {AuthUserModel} from '../../../../modules/vincofw/models/auth.user.model';
import {AdClientModel} from '../../../../modules/vincofw/models/ad.client.model';
import {AdClientService} from '../../../../modules/vincofw/services/ad.client.service';
import {AdUserService} from '../../../../modules/vincofw/services/ad.user.service';
import {AuthService} from '../../../../modules/vincofw/authentication/auth.service';
import {AppCoreSettings} from '../../../../modules/vincofw/configs/app.core.settings';
import {AppCacheService} from '../../../../modules/vincofw/services/cache.service';
import {MenuSelectItemModel} from '../../../../modules/vincofw/components/menu.select/menu.select.component';
import {AdLanguageService} from '../../../../modules/vincofw/services/ad.language.service';
import {GlobalEventsService} from '../../../../modules/vincofw/services/global.events.service';

@Component({
    selector: 'profile-menu',
    templateUrl: './profile.menu.component.html',
    styleUrls: ['./profile.menu.component.scss']
})
export class ProfileMenuComponent implements OnInit, AfterViewChecked, OnDestroy {
    roles: MenuSelectItemModel[];
    activeRole: MenuSelectItemModel;
    languages: MenuSelectItemModel[];
    activeLang: MenuSelectItemModel;
    userInfo: AuthUserModel;
    activeClient: AdClientModel;
    updateProfile: boolean;
    message: string = null;

    @Output() close: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private _settings: AppCoreSettings,
        private _cacheService: AppCacheService,
        private _coreSettings: AppCoreSettings,
        private _authService: AuthService,
        private _userService: AdUserService,
        private _languageService: AdLanguageService,
        private _clientService: AdClientService,
        private _globalEventsService: GlobalEventsService
    ) {
        // Set the defaults
        this.roles = [];
        this.languages = [];
        this.userInfo = _authService.getUserInfo();
        if (this.userInfo.idClient) {
            this._clientService.get(this._authService.getLoggedIdClient()).subscribe(client => {
                this.activeClient = client;
            });
            if (this._authService.getActiveRole()) {
                this._languageService.getClientLanguages(this._authService.getActiveRole().idClient).subscribe(result => {
                    this.languages = [];
                    result.forEach(lang => {
                        this.languages.push({
                            id: lang.idLanguage,
                            name: lang.name,
                            imageCss: 'flag flag-' + lang.countrycode
                        });
                    });
                    this.getActiveLanguage();
                });
            }
        }
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.updateProfile = false;
        this.getActiveRoles();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
    }

    onSetDefault($event: MatCheckboxChange): void {
        this.updateProfile = $event.checked;
    }

    ngAfterViewChecked(): void {
    }

    confirmChanges(): void {
        this.message = null;
        if (this.updateProfile) {
            this._userService.changeProfile(this._authService.getUserInfo().idUserLogged, this.activeLang.id, this.activeRole.id).subscribe(() => {
                this.changeRoleLanguage();
            });
        } else {
            this.changeRoleLanguage();
        }
    }

    selectRole(selected: MenuSelectItemModel): void{
        this.activeRole = selected;
    }

    selectLanguage(selected: MenuSelectItemModel): void {
        this.activeLang = selected;
    }

    private getActiveLanguage(): void {
        this.activeLang = this.languages.find(current => current.id === this._settings.idLanguage);
    }

    private getActiveRoles(): void {
        this.roles = [];
        this._authService.getUserInfo().roles.forEach(role => {
            this.roles.push({
                id: role.idRole,
                name: role.description
            });
        });
        this.activeRole = this.roles.find(r => r.id === this._authService.selectedRole);
    }

    private changeRoleLanguage(): void {
        this._globalEventsService.notifyCloseAllTabs();
        if (this._settings.idLanguage !== this.activeLang.id) {
            this._languageService.idLanguage = this.activeLang.id;
        }
        const userInfo = this._authService.getUserInfo();
        const role = userInfo.roles.find(current => {
            return current.idRole === this.activeRole.id;
        });
        if (!isNullOrUndefined(role)) {
            if (this._authService.selectedRole !== this.activeRole.id) {
                const password = this._authService.decryptPassword(this._settings.secretKey, userInfo.crytpo);
                this._authService.getJWT(this.activeRole.id + '$' + userInfo.username, password, userInfo.rememberMe).subscribe(token => {
                    if (token) {
                        this.login(userInfo.username, role.idClient);
                    } else {
                        this.message = this._cacheService.getTranslation('AD_MsgUserPasswordInvalid');
                    }
                });
            } else {
                this.close.emit();
                this._settings.setItem('reloading', 'Y');
                window.location.reload();
            }
        } else {
            console.log('Not found the role: ' + this.activeRole.id);
            this.close.emit();
        }
    }

    private login(username: string, idClient: string): void {
        this._authService.login(username, this.activeRole.id, this.activeLang.id).subscribe(result => {
            if (!result.isLoggedIn) {
                this.message = this._cacheService.getTranslation('AD_MsgUserPasswordInvalid');
            } else {
                this._coreSettings.idClient = idClient;
                this.close.emit();
                this._settings.setItem('reloading', 'Y');
                window.location.reload();
            }
        }, async error => {
            this.message = this._cacheService.getTranslation('AD_MsgUserPasswordInvalid');
        });
    }
}
