import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FuseConfigService} from '@fuse/services/config.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {navigation} from 'app/navigation/navigation';
import {AuthUserModel} from '../../../modules/vincofw/models/auth.user.model';
import {AppCoreSettings} from '../../../modules/vincofw/configs/app.core.settings';
import {AuthService} from '../../../modules/vincofw/authentication/auth.service';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ToolbarComponent implements OnInit, OnDestroy {
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    navigation: any;
    applicationLogo: string;
    userPhotoUrl: string;

    // Private
    private _unsubscribeAll: Subject<any>;
    public loggedUser: AuthUserModel;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _settings: AppCoreSettings,
        private _router: Router,
        private _authService: AuthService
    ) {
        this.navigation = navigation;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.applicationLogo = this._settings.appLogoUrl;
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        this.loggedUser = this._authService.getUserInfo();
        this.userPhotoUrl = this.loggedUser && this.loggedUser.photoUrl ? this.loggedUser.photoUrl : 'assets/images/user_icon.png';
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    logout(): void {
        this._authService.logout().then(() => {
            window.location.reload();
        });
    }

}
