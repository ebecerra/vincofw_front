import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MatButtonModule, MatIconModule, MatMenuModule, MatTabsModule, MatToolbarModule} from '@angular/material';

import {FuseSearchBarModule, FuseShortcutsModule} from '@fuse/components';
import {FuseSharedModule} from '@fuse/shared.module';

import {ToolbarComponent} from 'app/layout/components/toolbar/toolbar.component';
import {CustomModule} from '../../../extensions/custom/custom.module';

@NgModule({
    declarations: [
        ToolbarComponent
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
        MatTabsModule,

        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule,
        CustomModule
    ],
    exports: [
        ToolbarComponent
    ]
})
export class ToolbarModule {
}
