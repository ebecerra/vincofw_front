import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {FuseSidebarModule} from '@fuse/components/index';
import {FuseSharedModule} from '@fuse/shared.module';

import {ContentModule} from 'app/layout/components/content/content.module';
import {CustomModule} from 'app/extensions/custom/custom.module';
import {NavbarModule} from 'app/layout/components/navbar/navbar.module';
import {ToolbarModule} from 'app/layout/components/toolbar/toolbar.module';

import {VerticalLayout3Component} from 'app/layout/vertical/layout-3/layout-3.component';
import {ProfilePanelModule} from '../../components/profile-panel/profile-panel.module';
import {VincofwModule} from '../../../modules/vincofw/vincofw.module';

@NgModule({
    declarations: [
        VerticalLayout3Component
    ],
    imports     : [
        RouterModule,

        FuseSharedModule,
        FuseSidebarModule,

        ContentModule,
        CustomModule,
        NavbarModule,
        ProfilePanelModule,
        VincofwModule,
        ToolbarModule
    ],
    exports     : [
        VerticalLayout3Component
    ]
})
export class VerticalLayout3Module
{
}
