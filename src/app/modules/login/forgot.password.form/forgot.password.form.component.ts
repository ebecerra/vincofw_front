import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppErrorStateMatcher} from '../../../services/app.error.state.matcher';
import {NotificationService} from '../../vincofw/notification/notification.service';
import {AuthService} from '../../vincofw/authentication/auth.service';
import {AppCacheService} from '../../vincofw/services/cache.service';

@Component({
    selector: 'forgot-password-form',
    templateUrl: './forgot.password.form.component.html',
    styleUrls: ['./forgot.password.form.component.scss']
})
export class ForgotPasswordFormComponent implements OnInit, OnDestroy{

    public serverMessage = '';
    public serverError = false;
    public loginForm: FormGroup;
    public matcher = new AppErrorStateMatcher();

    @Output() formChange = new EventEmitter();

    constructor(
        private _authService: AuthService,
        private _notificationService: NotificationService,
        private _router: Router,
        private _cacheService: AppCacheService
    ) {
        this.loginForm = this.createFormGroup();
    }

    createFormGroup(): FormGroup {
        return new FormGroup({
            email: new FormControl('', [
                Validators.required,
                Validators.email
            ]),
        });
    }

    get email(): AbstractControl | null {
        return this.loginForm.get('email');
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    doRetrievePassword(): void {
        this.serverError = false;
        if (this.loginForm.valid) {
            this._authService.recoverPassword(this.email.value).then(
                (response) => {
                    if (response.success) {
                        this._notificationService.showMessage(this._cacheService.getTranslation('AD_GlobalMailPasswordSent'));
                        this.doLogin();
                    } else {
                        this.serverMessage = this._cacheService.getTranslation(response.message);
                        this.serverError = true;
                    }
                }, (response) => {
                    this._notificationService.showMessage(this._cacheService.getTranslation(response.message));
                }
            );
        }
    }

    doLogin(): void {
        this.formChange.emit('login');
    }
}
