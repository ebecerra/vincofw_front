import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {AppErrorStateMatcher} from '../../../services/app.error.state.matcher';
import {AuthService} from '../../vincofw/authentication/auth.service';
import {AppCacheService} from '../../vincofw/services/cache.service';
import {AppCoreSettings} from '../../vincofw/configs/app.core.settings';
import {BaseSettingsModel} from '../../vincofw/models/base.settings.models';
import {AdTerminalService} from '../../vincofw/services/ad.terminal.service';

@Component({
    selector: 'login-form',
    templateUrl: './login.form.component.html',
    styleUrls: ['./login.form.component.scss'],
})
export class LoginFormComponent implements OnInit, OnDestroy{

    // Public
    showLoginError: boolean;
    loginForm: FormGroup;
    matcher = new AppErrorStateMatcher();
    mode = 'login';

    @Output() formChange = new EventEmitter();

    constructor(
        private _authService: AuthService,
        private _appSettings: BaseSettingsModel,
        private _router: Router,
        private _cacheService: AppCacheService,
        private _settings: AppCoreSettings,
        private _terminalService: AdTerminalService
    ) {
        this.loginForm = this.createFormGroup();
        this.showLoginError = false;
    }

    createFormGroup(): FormGroup {
        return new FormGroup({
            user: new FormControl('', [
                Validators.required
            ]),
            password: new FormControl('', [
                Validators.required
            ]),
            rememberMe: new FormControl('', [
            ])
        });
    }

    get user(): AbstractControl | null {
        return this.loginForm.get('user');
    }

    get password(): AbstractControl | null  {
        return this.loginForm.get('password');
    }

    get rememberMe(): AbstractControl | null  {
        return this.loginForm.get('rememberMe');
    }


    ngOnInit(): void {
        if (this._authService.isLogged()) {
            this._router.navigate(['/']);
        }
    }

    ngOnDestroy(): void {
    }

    doLogin(): void {
        const that = this;
        this.showLoginError = false;
        if (this.loginForm.valid) {
            this._authService.getJWT(this.user.value, this.password.value, this.rememberMe.value).subscribe(token => {
                if (token) {
                    this._authService.login(this.user.value).subscribe(result => {
                        if (result.isLoggedIn) {
                            this._authService.loadPermissions().then(() => {
                                const ta = this._authService.hasPermission('TerminalAuthentication');
                                this._terminalService.checkTerminalAuthentication(ta).then(success => {
                                    if (success) {
                                        this._authService.notifyLogin(true);
                                        that._router.navigate(['/home']);
                                    } else {
                                        this.mode = 'terminal';
                                    }
                                });
                            });
                        } else {
                            this.showLoginError = true;
                        }
                    }, async error => {
                        this.showLoginError = true;
                    });
                } else {
                    this.showLoginError = true;
                }
            });
        }
    }

    doRecover(): void {
        this.formChange.emit('recover');
    }
}
