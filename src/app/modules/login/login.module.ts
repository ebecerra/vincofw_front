import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginFormComponent} from './login.form/login.form.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ForgotPasswordFormComponent} from './forgot.password.form/forgot.password.form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {VincofwModule} from '../vincofw/vincofw.module';
import {LogoutComponent} from './logout/logout.component';
import {MaterialModule} from '../../material.module';
import {PipesModule} from '../../pipes/pipes.module';
import {TerminalComponent} from './terminal/terminal.component';

@NgModule({
    declarations: [
        ForgotPasswordFormComponent,
        LoginFormComponent,
        LogoutComponent,
        TerminalComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        PipesModule,
        VincofwModule,
        MaterialModule
    ],
    exports: [
        ForgotPasswordFormComponent,
        LoginFormComponent,
        LogoutComponent,
        TerminalComponent
    ]
})
export class LoginModule {
}
