import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../vincofw/authentication/auth.service';
import {AppCacheService} from '../../vincofw/services/cache.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
      private _cacheService: AppCacheService,
      private _router: Router,
      private _authService: AuthService
  ) { }

  ngOnInit(): void {
      this._authService.logout().then(
          result => {
              this._router.navigate(['/login']);
          }
      );
  }

}
