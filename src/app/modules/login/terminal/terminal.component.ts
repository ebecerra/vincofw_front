import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdTerminalService} from '../../vincofw/services/ad.terminal.service';
import {AppCoreSettings} from '../../vincofw/configs/app.core.settings';
import {AppCacheService} from '../../vincofw/services/cache.service';
import {AuthService} from '../../vincofw/authentication/auth.service';

@Component({
    selector: 'app-terminal',
    templateUrl: './terminal.component.html',
    styleUrls: ['./terminal.component.scss'],
})
export class TerminalComponent {

    errorMsg: string = null;
    terminalForm: FormGroup;

    constructor(
        private _authService: AuthService,
        private _cacheService: AppCacheService,
        private _terminalService: AdTerminalService,
        private _settings: AppCoreSettings,
        private _router: Router
    ) {
        this.terminalForm = new FormGroup({
            terminalIdentifier: new FormControl('', [
                Validators.required
            ]),
            username: new FormControl('', [
                Validators.required
            ]),
            password: new FormControl('', [
                Validators.required
            ])
        });
    }

    doLink(): void {
        if (this.terminalForm.valid) {
            const formValue = this.terminalForm.getRawValue();
            const idRole = this._settings.getItem('selectedRole');
            const password = this._authService.encryptPassword(formValue.username, formValue.password);
            this._terminalService.link(formValue.username, password, idRole, formValue.terminalIdentifier).subscribe(response => {
                if (response.success) {
                    this._settings.setItem('terminalIdentifier', formValue.terminalIdentifier);
                    this._settings.setItem('linkedIdentifier', response.linkedIdentifier);
                    this._router.navigate(['/home']);
                } else {
                    this.errorMsg = this._cacheService.getTranslation(response.error);
                }
            });
        }
    }
}
