import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './vincofw/authentication/auth.guard.service';
import {LoginModule} from './login/login.module';
import {TabManagerComponent} from './vincofw/tab.manager/tab.manager/tab.manager.component';
import {TabManagerModule} from './vincofw/tab.manager/tab.manager.module';
import {LogoutComponent} from './login/logout/logout.component';
import {LoginPageComponent} from '../extensions/custom/components/login.page/login.page.component';
import {TerminalComponent} from './login/terminal/terminal.component';

const routes: Routes = [
    { path: 'login', component: LoginPageComponent },
    { path: 'logout', component: LogoutComponent },
    { path: 'terminal', component: TerminalComponent, canActivate: [ AuthGuard ]},
    { path: 'home', component: TabManagerComponent, canActivate: [ AuthGuard ]},
    {
        path: 'tab',
        canActivate: [ AuthGuard ],
        children: [
            {
                path: ':idMenu',
                component: TabManagerComponent
            }
        ]
    },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', component: LoginPageComponent }
];

@NgModule({
    declarations: [ ],
    imports: [
        LoginModule,
        TabManagerModule,
        RouterModule.forRoot(routes, { useHash: true })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
