import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {AppCoreSettings} from '../configs/app.core.settings';

/**
 * Checks if the route can be activated.
 * A route can be activated if the user is logged in
 */
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private _settings: AppCoreSettings,
        private _authService: AuthService,
        private _router: Router
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        const that = this;
        return new Promise<boolean>((resolve, reject) => {
            const isLogged = that._authService.isLogged();
            if (!isLogged) {
                const userInfo = that._authService.getUserInfo();
                if (userInfo && that._authService.isExpiredToken()) {
                    if (userInfo.crytpo) {
                        const password = that._authService.decryptPassword(that._settings.secretKey, userInfo.crytpo);
                        that._authService.getJWT(userInfo.username, password, userInfo.rememberMe, userInfo.idClient).subscribe(() => {
                            resolve(true);
                        },
                        () => {
                            that._router.navigate(['/login']);
                            resolve(false);
                        });
                    } else {
                        that._router.navigate(['/login']);
                        resolve(false);
                    }
                } else {
                    that._router.navigate(['/login']);
                    resolve(false);
                }
            } else {
                resolve(isLogged);
            }
        });
    }
}
