import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AuthService} from './auth.service';

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

    constructor(
        private authService: AuthService,
        private settings: AppCoreSettings
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.headers && req.headers.get('Authorization')) {
            return next.handle(req);
        } else {
            const jwtToken = this.settings.getItem('jwtToken');
            if (jwtToken) {
                if (!this.authService.isExpiredToken()) {
                    const cloned = req.clone({
                        headers: req.headers.set('Authorization', 'Bearer ' + jwtToken)
                    });
                    return next.handle(cloned);
                } else {
                    this.authService.clearCredentials();
                }
            }
            return next.handle(req);
        }
    }
}
