import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs-compat/add/observable/empty';
import {catchError, map} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import {isNullOrUndefined} from 'util';
import {JwtHelperService} from '@auth0/angular-jwt';
import {AuthLoginResponse, AuthRoleModel, AuthUserModel} from '../models/auth.user.model';
import {ControllerResultModel} from '../models/controller.result.model';
import {AdPermissionModel} from '../models/ad.permission.model';
import {AdClientModel} from '../models/ad.client.model';
import {AdUserService} from '../services/ad.user.service';
import {AdLanguageService} from '../services/ad.language.service';
import {AdPreferenceService} from '../services/ad.preference.service';
import {AdClientService} from '../services/ad.client.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdTerminalService} from '../services/ad.terminal.service';

import * as CryptoJS from 'crypto-js';
import * as moment_ from 'moment';

const moment = moment_;

@Injectable()
export class AuthService {

    private loginModifiedSource = new Subject<boolean>();
    private _client: AdClientModel = new AdClientModel();
    private url: string;
    private _cachePermissions: AdPermissionModel[] = [];

    loginModified$ = this.loginModifiedSource.asObservable();

    constructor(
        private _http: HttpClient,
        private _userService: AdUserService,
        private _languageService: AdLanguageService,
        private _clientService: AdClientService,
        private _settings: AppCoreSettings,
        private _preferenceService: AdPreferenceService,
        private _terminalService: AdTerminalService
    ) {
        console.log('AuthService created ...');
        this.url = this._settings.getRestHostLogin();
    }

    /******************************************************************************************************************
     * Public methods
     *****************************************************************************************************************/

    /**
     * Clear cache values
     */
    clearCache(): void {
    }

    /**
     * Checks if the role matches the active role
     *
     * @param property Property to check
     * @returns {boolean} True if user has the permission and is allowed
     */
    hasPermission(property: string): boolean {
        return this._cachePermissions.findIndex(value => value.property === property && value.allow) >= 0;
    }

    /**
     * Load permission for role
     */
    loadPermissions(): Promise<void> {
        return new Promise(resolve => {
            this._preferenceService.permissions(this.selectedRole).subscribe(response => {
                this._cachePermissions = response;
                resolve();
            });
        });
    }

    /**
     * Change password
     *
     * @param oldPassword Old password
     * @param newPassword New password
     * @param security Security level
     */
    public changePassword(oldPassword: string, newPassword: string, security: string): Observable<ControllerResultModel> {
        if (!this.isLogged()) {
            return Observable.of({ success: false, errCode: -1, message: 'not_logged', properties: null });
        }

        const path = this.getLoggedIdClient() + '/' + this.getUserInfo().idUserLogged;
        const url = this._settings.getRestHostCore() + 'ad_user/' + path + '/change_password';
        let params = new HttpParams();
        params = params.append('size', '' + newPassword.length)
            .append('security', security)
            .append('oldPassword', this.encryptPassword(path, oldPassword))
            .append('newPassword', this.encryptPassword(path, newPassword));
        return this._http.post(url, null, { params: params })
            .map((response: any) => {
                if (response.success) {
                    const userInfo = this.getUserInfo();
                    userInfo.crytpo = this.encryptPassword(this._settings.secretKey, newPassword);
                    this._settings.setItem('userInfo', JSON.stringify(userInfo));
                }
                return response as ControllerResultModel;
            });
    }

    /**
     * Initialize user loading
     */
    public initUserLoad(): Promise<any> {
        return new Promise((resolve, reject) => {
            const userInfo = this.getUserInfo();
            const jwtToken =  this._settings.getItem('jwtToken');
            if (userInfo && jwtToken) {
                this._settings.idClient = userInfo.idClient;
                if (this.isExpiredToken()) {
                    if (userInfo.crytpo) {
                        const password = this.decryptPassword(this._settings.secretKey, userInfo.crytpo);
                        this._settings.removeItem('expireAt');
                        this._settings.removeItem('jwtToken');
                        this.getJWT(userInfo.username, password, userInfo.rememberMe, userInfo.idClient).subscribe(() => {
                            this.loadPermissionsAndCheckTerminal(resolve, reject);
                        },
                        error => {
                            this.clearCredentials();
                            reject();
                        });
                    } else {
                        this.clearCredentials();
                        resolve();
                    }
                } else {
                    this.loadPermissionsAndCheckTerminal(resolve, reject);
                }
            } else {
                resolve();
            }
        });
    }

    /**
     * Get JWT authentication token
     *
     * @param username User name
     * @param password User password
     * @param rememberMe Set to true to store do autologin
     * @param idClient Client identifier
     */
    public getJWT(username: string, password: string, rememberMe: boolean = false, idClient: string = null): Observable<string> {
        return this._http.post(
            this._settings.getRestHostCore() + 'authenticate',
            {
                username: username,
                password: this.encryptPassword(username, password)
            }).pipe(map(response => {
                let token: string = response['token'];
                const helper = new JwtHelperService();
                const decodedToken = helper.decodeToken(token);
                if (decodedToken) {
                    const userInfo = this.getUserInfo() || new AuthUserModel();
                    userInfo.idUserLogged = decodedToken.sub;
                    userInfo.idClient = idClient;
                    userInfo.name = decodedToken.name;
                    userInfo.email = decodedToken.email;
                    userInfo.username = username.indexOf('$') > 0 ? username.split('$')[1] : username;
                    userInfo.crytpo = this.encryptPassword(this._settings.secretKey, password);
                    userInfo.rememberMe = rememberMe;
                    userInfo.privileges = decodedToken.privileges;
                    if (!userInfo.roles) {
                        userInfo.roles = [];
                    }
                    this._settings.setItem('jwtToken', token);
                    this._settings.setItem('userInfo', JSON.stringify(userInfo));
                    this._settings.setItem('expireAt', JSON.stringify(decodedToken.exp));
                } else {
                    console.log('Invalid authentication token');
                    token = null;
                }
                return token;
            }),
            catchError(err => {
                console.log(err);
                return Observable.of(null);
            })
        );
    }

    /**
     * Performs the authentication login logic
     *
     * @param username User name
     * @param idRole Role identifier
     * @param idLanguage Language identifier
     */
    public login(username: string, idRole: string = null, idLanguage: string = null): Observable<AuthLoginResponse> {
        let params = new HttpParams();
        params = params.append('login', username);
        return this._http.get(this.url + 'login/privileges/', { params: params }).pipe(
            map(response => {
                const responseProperties = response['properties'];
                const userInfo = this.getUserInfo();
                userInfo.roles = responseProperties.roles;
                userInfo.photoUrl = responseProperties.user.photoUrl && responseProperties.user.photoUrl.indexOf('http') === 0 ? responseProperties.user.photoUrl : null;
                userInfo.defaultRole = responseProperties.user.defaultIdRole;
                userInfo.defaultLanguage = responseProperties.user.defaultIdLanguage;
                this.selectedRole = idRole ? idRole : userInfo.defaultRole;
                userInfo.idClient = userInfo.roles.find(current => current.idRole === this.selectedRole).idClient;
                this._settings.idClient = userInfo.idClient;
                this._settings.setItem('userInfo', JSON.stringify(userInfo));
                this._languageService.getClientLanguages(userInfo.idClient, true).subscribe(() => {
                    this._languageService.idLanguage = idLanguage ? idLanguage : userInfo.defaultLanguage;
                });
                const loginResponse = new AuthLoginResponse();
                loginResponse.isLoggedIn = true;
                loginResponse.serverResponse = responseProperties;
                return loginResponse;
            }),
            catchError(err => {
                this.clearCredentials();
                return Observable.of(err);
            })
        );
    }

    /**
     * Performs the authentication logout logic
     */
    public logout(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._settings.idClient = '';
            this._http.get(this._settings.getRestHostLogin() + 'login/logout', {})
                .catch( (error, caught) => {
                    this.clearCredentials();
                    reject(error);
                    return Observable.empty();
                })
                .subscribe( (data) => {
                    this.clearCredentials();
                    resolve(data);
                });
        });
    }

    /**
     * Notify login event
     *
     * @param success Success login or not
     */
    public notifyLogin(success: boolean): void {
        this.loginModifiedSource.next(success);
    }

    /**
     * Clear user credentials
     */
    public clearCredentials(): void {
        this._cachePermissions = [];
        this._settings.removeItem('userInfo');
        this._settings.removeItem('jwtToken');
        this._settings.removeItem('expireAt');
        this._settings.removeItem('selectedRole');
        this.loginModifiedSource.next(false);
    }

    /**
     * Check if user is logged
     *
     * @param emitEvent If emit login modified event
     */
    public isLogged(emitEvent: boolean = false): boolean {
        const token = this._settings.getItem('jwtToken');
        if (isNullOrUndefined(token)) {
            return false;
        }
        const result = !isNullOrUndefined(this._settings.getItem('expireAt')) && moment().unix() <= this.getExpiration();
        if (emitEvent) {
            this.loginModifiedSource.next(result);
        }
        return result;
    }

    /**
     * Get user information
     */
    public getUserInfo(): AuthUserModel {
        const userInfo = this._settings.getItem('userInfo');
        return userInfo ? JSON.parse(userInfo) : null;
    }

    /**
     * Check if current user has a privilege
     *
     * @param priv Privilege
     */
    public hasPrivilege(priv: string): boolean {
        if (this.isLogged()) {
            const userInfo = this.getUserInfo();
            return !isNullOrUndefined(userInfo.privileges.find(p => p === priv));
        }
        return false;
    }

    /**
     * Get client identifier for logged user and role
     */
    public getLoggedIdClient(): string {
        if (this.isLogged()) {
            const userInfo = this.getUserInfo();
            if (!isNullOrUndefined(userInfo) && !isNullOrUndefined(userInfo.roles) && userInfo.roles.length > 0) {
                const selected = userInfo.roles.find(current => {
                    return current.idRole === this.selectedRole;
                });
                if (!isNullOrUndefined(selected)){
                    return selected.idClient;
                }
                return this.getUserInfo().roles[0].idClient;
            }
        }
        return null;
    }

    /**
     * Get user identifier for logged user
     */
    public getLoggedIdUser(): string {
        return this.isLogged() ? this.getUserInfo().idUserLogged : null;
    }

    /**
     * Get client information for logged user and role
     */
    public getLoggedClient(): Promise<AdClientModel> {
        return new Promise((resolve, reject) => {
            const loggedIdClient = this.getLoggedIdClient();
            if (this._client.idClient === loggedIdClient) {
                resolve(this._client);
            } else {
                if (isNullOrUndefined(loggedIdClient)) {
                    resolve(null);
                } else {
                    this._clientService.get(loggedIdClient).subscribe(result => {
                        this._client = result;
                        resolve(result);
                    }, error => {
                        reject(error);
                    });
                }
            }
        });
    }

    /**
     * Init the recover password flow
     *
     * @param email User Email
     */
    public recoverPassword(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._languageService.getClientLanguages().subscribe(result => {
                const language = result.find(current => {
                    return current.idLanguage === this._settings.idLanguage;
                });
                let languageCode = null;
                if (!isNullOrUndefined(language)) {
                    languageCode = language.iso2;
                }
                this._userService.recover(email, languageCode).subscribe(resultRecover => {
                    resolve(resultRecover);
                }, error => {
                    console.error(error);
                    reject(error);
                });
            }, error => {
                console.error(error);
                reject(error);
            });
        });

    }

    public isExpiredToken(): boolean {
        const expiration = this._settings.getItem('expireAt');
        if (!isNullOrUndefined(expiration)) {
            return moment().unix() > this.getExpiration();
        }
        return true;
    }

    getExpiration(): any {
        const expiration = this._settings.getItem('expireAt');
        return JSON.parse(expiration);
    }

    public getActiveRole(): AuthRoleModel {
        return this.getUserInfo().roles.find( r => r.idRole === this.selectedRole);
    }

    public set selectedRole(idRole: string){
        this._settings.setItem('selectedRole', idRole);
    }

    public get selectedRole(): string{
        return this._settings.getItem('selectedRole');
    }

    /**
     * Encrypt a password
     *
     * @param passPhrase Password phrase
     * @param plainText Clear text
     */
    public encryptPassword(passPhrase: string, plainText: string): string {
        const iv = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
        const salt = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
        const key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), { keySize: 4, iterations: 1000 });
        const encrypted = CryptoJS.AES.encrypt(plainText, key, { iv: CryptoJS.enc.Hex.parse(iv) });
        return btoa(iv + '::' + salt + '::' + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    }

    /**
     * Decrypt a password
     *
     * @param passPhrase Password phrase
     * @param cipherText Cipher text
     */
    public decryptPassword(passPhrase: string, cipherText: string): string {
        cipherText = atob(cipherText);
        const tokens = cipherText.split('::');
        if (tokens.length === 3) {
            const iv = tokens[0];
            const salt = tokens[1];
            const key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), { keySize: 4, iterations: 1000 });
            const cipherParams = CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(tokens[2]) });
            const decrypted = CryptoJS.AES.decrypt(cipherParams, key, { iv: CryptoJS.enc.Hex.parse(iv) });
            return decrypted.toString(CryptoJS.enc.Utf8);
        }
        return null;
    }

    private loadPermissionsAndCheckTerminal(resolve, reject): void {
        this.loadPermissions().then(() => {
            const ta = this.hasPermission('TerminalAuthentication');
            this._terminalService.checkTerminalAuthentication(ta).then(success => {
                if (success) {
                    resolve();
                } else {
                    this.clearCredentials();
                    reject();
                }
            });
        });
    }

}
