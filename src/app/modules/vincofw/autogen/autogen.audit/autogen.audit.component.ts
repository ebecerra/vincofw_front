import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import {HttpParams} from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import {AdTableDtoModel} from '../../models/dto/ad.table.dto.model';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {SortOrder} from '../../models/ad.base.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

@Component({
    selector: 'autogen-audit',
    templateUrl: './autogen.audit.component.html',
    styleUrls: ['./autogen.audit.component.scss']
})
export class AutogenAuditComponent implements OnInit {

    item: any;
    service: AutogenService;
    auditService: AutogenService;
    properties: any = null;
    table: AdTableDtoModel;
    fields: AdFieldDtoModel[];
    filterForm: FormGroup;
    referenceUser: AdReferenceDtoModel = null;
    auditItems: any[];
    selectedItem: any;
    modifiedColumns: any[];

    constructor(
        private _cacheService: AppCacheService,
        private _autogenService: AutogenService,
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
    ) {
        this.item = data.item;
        this.service = data.service;
        this.table = data.table;
        this.fields = data.fields;

        this.filterForm = new FormGroup({
            created: new FormControl(),
            idUser: new FormControl()
        });
        this.filterForm.valueChanges.subscribe(values => {
            this.filterAudit(values);
        });
    }

    ngOnInit(): void {
        this.service.loadRowAudit(this.item.id).subscribe(result => {
            this.properties = result.properties;
        });
        if (this.table.audit) {
            this._cacheService.getReferenceInfo([VINCOFW_CONFIG.REFERENCES.TABLE_USER]).subscribe(references => {
                this.referenceUser = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.TABLE_USER);
            });
        }
        this._cacheService.loadTableInfo(VINCOFW_CONFIG.TABLES.AD_AUDIT).subscribe(table => {
            this.auditService = this._autogenService.bindedService(table);
            this.filterAudit({
                created: null,
                idUser: null
            });
        });
    }

    showAudit(item: any): void {
        this.selectedItem = item;
        this.modifiedColumns = [];
        if (this.selectedItem){
            const oldValue = JSON.parse(this.selectedItem.oldValue);
            const newValue = JSON.parse(this.selectedItem.newValue);
            const keys = Object.keys(newValue);
            Object.keys(oldValue).forEach(k => {
                if (keys.indexOf(k) === -1) {
                    keys.push(k);
                }
            });
            keys.forEach(key => {
                if (oldValue[key] !== newValue[key]) {
                    this.modifiedColumns.push({
                        oldValue: oldValue[key],
                        newValue: newValue[key],
                        field: this.fields.find(fld => fld.columnName === key)
                    });
                }
            });
        }
    }

    private filterAudit(values: any): void {
        let q = 'idRecord=' + this.item.id;
        q += (',idTable=' + this.table.idTable);
        if (values['idUser']) {
            q += (',idUser=' + values['idUser']);
        }
        if (values['created']) {
            const date = values['created'].format('DD/MM/YYYY');
            q += (',created=' + date + '~' + date + ',');
        }
        let params = new HttpParams();
        params = params.append('q', q);
        this.auditService.list(params, [{ property: 'created', direction: SortOrder.DESC }], 100).subscribe(response => {
            this.auditItems = response.content;
            this.showAudit(this.auditItems.length === 0 ? null : this.auditItems[0]);
        });
    }
}
