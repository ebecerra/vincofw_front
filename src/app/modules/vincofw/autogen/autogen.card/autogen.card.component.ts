import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {ConditionalService} from '../../services/conditional.service';
import {isNullOrUndefined} from 'util';
import {AppCacheService} from '../../services/cache.service';
import {AdRowFieldModel} from '../../models/ad.grid.model';
import {AutogenFieldsComponent} from '../autogen.fields/autogen.fields.component';

@Component({
    selector: 'autogen-card',
    templateUrl: './autogen.card.component.html',
    styleUrls: ['./autogen.card.component.scss']
})
export class AutogenCardComponent implements OnInit, OnDestroy {

    rows: AdRowFieldModel[];

    @Input() item: any;
    @Input() fields: AdFieldDtoModel[];
    @Input() gridColumns: string;

    /**
     * Constructor
     */
    constructor(
        private _conditionalService: ConditionalService,
        private _cacheService: AppCacheService
    ) {

    }

    ngOnInit(): void {
        this.rows = AutogenFieldsComponent.buildFormGrid(this.gridColumns, this.fields);
        this.fields.forEach(fld => {
            if (fld.displaylogic) {
                fld.runtimeDisplaylogic = this._conditionalService.replaceJSConditional(fld.displaylogic, null, null, this.item);
                if (this._conditionalService.isCompleteConditional(fld.runtimeDisplaylogic)) {
                    // tslint:disable-next-line:no-eval
                    fld.runtimeDisplayed = eval(fld.runtimeDisplaylogic);
                } else {
                    fld.runtimeDisplayed = false;
                }
            } else {
                fld.runtimeDisplayed = true;
            }
            const key = 'cardValue$' + fld.columnName;
            this.item[key] = this.item[fld.columnName];
            if (isNullOrUndefined(this.item[key])) {
                this.item[key] = '&nbsp;';
            }
            switch (fld.relatedColumn.reference.rtype) {
                case 'YESNO':
                case 'LIST':
                case 'TABLEDIR':
                case 'TABLE':
                case 'PASSWORD':
                case 'NUMBER':
                case 'DATE':
                case 'TIME':
                case 'TIMESTAMP':
                    this._cacheService.getReferenceValue(
                        this.item[key], fld.relatedColumn.reference.idReference, this.item, fld.relatedColumn
                    ).then(value => this.item[key] = value);
            }
        });
    }

    ngOnDestroy(): void {
    }

}
