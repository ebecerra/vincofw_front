import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AutogenComponent} from '../autogen.component';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AuthService} from '../../authentication/auth.service';
import {AdChartDtoModel} from '../../models/dto/ad.chart.dto.model';
import {AdColumnDtoModel} from '../../models/dto/ad.column.dto.model';
import {AutogenUsedinchildModel} from '../../models/autogen.usedinchild.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {ConditionalService} from '../../services/conditional.service';
import {LocalstoreService} from '../../services/localstore.service';
import {AdChartService} from '../../services/ad.chart.service';
import {UtilitiesService} from '../../services/utilities.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {EventChartSelectModel} from '../../models/event/event.chart.model';
import {AutogenFieldsComponent} from '../autogen.fields/autogen.fields.component';
import {AdRowFieldModel} from '../../models/ad.grid.model';

import * as moment_ from 'moment';

const moment = moment_;

@Component({
  selector: 'autogen-chart',
  templateUrl: './autogen.chart.component.html',
  styleUrls: ['./autogen.chart.component.scss']
})
export class AutogenChartComponent extends AutogenComponent implements OnInit, OnDestroy {

    private filters: any = null;
    private filterColumns: AdColumnDtoModel[];

    rows: AdRowFieldModel[];
    referenceChartType: AdReferenceDtoModel = null;

    @ViewChild('drawer') drawer;

    @Input() loadOnInit: boolean;
    @Input() usedInChild: AutogenUsedinchildModel[];
    @Input() showInPopup = false;
    @Input() parentItem: any;

    constructor(
      protected _fuseConfigService: FuseConfigService,
      protected _notificationService: NotificationService,
      protected _cacheService: AppCacheService,
      protected _autogenService: AutogenService,
      private _localstoreService: LocalstoreService,
      private _conditionalService: ConditionalService,
      private _authService: AuthService,
      private _utilitiesService: UtilitiesService,
      private _globalEventsService: GlobalEventsService,
      private _chartService: AdChartService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
    }

    ngOnInit(): void {
        this.onInit();
        this._cacheService.getReferenceInfo([VINCOFW_CONFIG.REFERENCES.LIST_CHART_TYPES]).subscribe(references => {
            this.referenceChartType = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.LIST_CHART_TYPES);
        });
    }

    protected relatedTabLoaded(): void {
        this.tab.tab.charts.forEach(chart => {
            if (chart.displaylogic) {
                const runtimeDisplaylogic = this._conditionalService.replaceJSConditional(chart.displaylogic, null, null, this.parentItem);
                // tslint:disable-next-line:no-eval
                chart.displayed = eval(runtimeDisplaylogic);
            } else {
                chart.displayed = true;
            }
            chart.columnReferenceType = 'CHART';
            if (chart.ctype.indexOf('BAR_CHARTS_') === 0) {
                chart.chartType = 'BAR_CHARTS';
            }
            if (chart.ctype.indexOf('PIE_CHART_') === 0) {
                chart.chartType = 'PIE_CHART';
            }
            if (chart.ctype.indexOf('LINEAREA_') === 0) {
                chart.chartType = 'LINEAREA';
            }
            if (chart.ctype.indexOf('OTHER_') === 0) {
                chart.chartType = 'OTHER';
                if (chart.ctype === 'OTHER_LIST') {
                    chart.runtimeColumns = chart.columns.filter(col => col.displayed);
                    chart.gridColumnsStyle = `1fr repeat(${chart.runtimeColumns.length - 1}, 15rem)`;
                }
            }
        });
        this.rows = AutogenFieldsComponent.buildFormGrid(this.tab.tab.gridColumns, this.tab.tab.charts);
        this.doLoadContent();
    }

    /**
     * Refresh data content
     */
    doRefresh(): void {
        this.doLoadContent();
    }

    filterChanged(filters: any): void {
        this.filters = filters;
        this._localstoreService.saveTabInfo(this.tab.idTab, this.filters, null, null);
        this.doLoadContent();
    }

    fillFilter(event): void {
        this.filterColumns = event.columns;
        this.doLoadContent();
    }

    changeChartType(chart: AdChartDtoModel, chartType: string): void {
        const prevChartType = chart.ctype;
        chart.ctype = chartType;
        setTimeout(() => {
            this._globalEventsService.notifyChartChangeType(chart.idChart, prevChartType);
        });
    }

    getChartTypeName(ct: string): string {
        if (this.referenceChartType && this.referenceChartType.refList) {
            const name = this.referenceChartType.refList.find(rf => rf.value === ct);
            return name ? name.name : ct;
        }
        return ct;
    }

    doSelect(data: EventChartSelectModel): void {
        const chart = this.tab.tab.charts.find(ch => ch.idChart === data.idChart);
        if (chart) {
            let extra = chart.keyField + '=';
            if (chart.mode.indexOf('DATE') === 0 && chart.runtimeResult.dateMode === 'DAY') {
                extra += moment(data.code, 'YYYYMMDD').format('DD/MM/YYYY');
            } else {
                extra += data.code;
            }
            chart.linked.forEach(linked => {
                const linkedChart = this.tab.tab.charts.find(ch => ch.idChart === linked && ch.displayed);
                if (linkedChart) {
                    linkedChart.runtimeFilter = data.name;
                    this.doChartLoadContent(linkedChart, extra);
                }
            });
        }
    }

    /**
     * Loads the chart content
     */
    private doLoadContent(): void {
        if (this.filterColumns && this.filters) {
            const visibleCharts = this.tab.tab.charts.filter(chart => chart.displayed);
            visibleCharts.forEach(chart => {
                chart.runtimeFilter = '';
                this.doChartLoadContent(chart, null);
            });
        }
    }

    /**
     * Load chart content
     *
     * @param chart Chart
     * @param extra Extra filters
     */
    private doChartLoadContent(chart: AdChartDtoModel, extra: string): void {
        chart.runtimeError = null;
        let constraints = this._utilitiesService.getFilterParams(this.filters, this.filterColumns);
        if (extra) {
            if (constraints[constraints.length - 1] !== ',') {
                constraints += ',';
            }
            constraints += extra;
        }
        this._chartService.evaluate(chart.idChart, this._authService.getLoggedIdUser(), constraints).subscribe(data => {
            if (data['success']) {
                chart.runtimeResult = data;
                if (chart.ctype === 'OTHER_LIST') {
                    chart.runtimeListValues = [];
                    chart.runtimeResult.values.forEach(value => {
                        const item = {};
                        value.values.forEach((vl, index) => {
                            item[chart.columns[index].name] = this._utilitiesService.getValueByReference(chart.columns[index].reference, vl);
                        });
                        chart.runtimeListValues.push(item);
                    });
                }
            } else {
                chart.runtimeError = this._cacheService.getTranslation(data['error']);
            }
        });
    }
}
