import {Component, Input, OnDestroy} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {FuseConfigService} from '../../../../@fuse/services/config.service';
import {AdTabMainDtoModel} from '../models/dto/ad.tab.main.dto.model';
import {AdReferenceDtoModel} from '../models/dto/ad.reference.dto.model';
import {AppCacheService} from '../services/cache.service';
import {AutogenService} from '../services/autogen.service';
import {NotificationService} from '../notification/notification.service';

@Component({
    template: ``
})
export class AutogenComponent implements OnDestroy {

    protected fuseConfig: any;
    public tab: AdTabMainDtoModel;
    protected _service: AutogenService;
    protected _unsubscribeAll: Subject<any>;
    protected title: string;
    protected initialize = false;
    public loadedReferences: AdReferenceDtoModel[];

    constructor(
        protected _fuseConfigService: FuseConfigService,
        protected _notificationService: NotificationService,
        protected _cacheService: AppCacheService,
        protected _autogenService: AutogenService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    @Input()
    set relatedTab(tab: AdTabMainDtoModel) {
        console.log('AutogenComponent (' + this.constructor.name + '): relatedTab(' + tab.name + ')');
        if (isNullOrUndefined(tab.tab)) {
            this._cacheService.getTabInfo(tab.idWindow, tab.idTab).subscribe(data => {
                this.loadRelatedTab(data);
            });
        } else {
            this.loadRelatedTab(tab);
        }
    }

    @Input()
    set tabTitle(title: string) {
        this.title = title;
    }

    private loadRelatedTab(tab: AdTabMainDtoModel): void {
        if (isNullOrUndefined(this.tab) || this.tab.tab.table.idTable !== tab.tab.table.idTable) {
            this.tab = tab;
            if (tab.ttype !== 'USERDEFINED' && tab.ttype !== 'CHART') {
                this._service = this._autogenService.bindedService(tab.tab.table);
                const referenceIds = this.tab.tab.table.columns.map(current => current.reference.idReference);
                const distinct = [];
                referenceIds.forEach(current => {
                    if (isNullOrUndefined(distinct.find(r => r === current))) {
                        distinct.push(current);
                    }
                });
                this._cacheService.getReferenceInfo(distinct)
                    .pipe(takeUntil(this._unsubscribeAll)).subscribe(references => {
                    this.loadedReferences = references;
                    this.tab.tab.table.columns.forEach(column => {
                        if (column.reference.rtype === 'BUTTON' || column.reference.rtype === 'LIST'
                            || column.reference.rtype === 'TABLE' || column.reference.rtype === 'TABLEDIR'
                        ) {
                            const ref = this.loadedReferences.find(reference => reference.idReference === column.reference.idReference);
                            if (ref) {
                                column.reference.refButton = ref.refButton;
                                column.reference.refList = ref.refList;
                                column.reference.refTable = ref.refTable;
                            }
                        }
                    });
                    this.relatedTabLoaded();
                });
            }
            if (tab.ttype === 'CHART') {
                this.relatedTabLoaded();
            }
        }
    }

    /**
     * Execute when the related tab is loaded
     */
    protected relatedTabLoaded(): void {

    }

    ngOnDestroy(): void {
        console.log('AutogenComponent (' + this.constructor.name + '): ngOnDestroy(' + (this.tab ? this.tab.name : '') + ')');
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Base 'ngOnInit' must be called from the descendents
     */
    protected onInit(): void {
        this.initialize = true;
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
    }
}
