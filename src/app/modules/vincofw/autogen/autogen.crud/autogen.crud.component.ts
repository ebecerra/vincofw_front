import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AutogenComponent} from '../autogen.component';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AutogenEditElementsModel} from '../models/autogen.edit.elements.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {AutogenUsedinchildModel} from '../../models/autogen.usedinchild.model';

@Component({
  selector: 'autogen-crud',
  templateUrl: './autogen.crud.component.html',
  styleUrls: ['./autogen.crud.component.scss']
})
export class AutogenCrudComponent extends AutogenComponent implements OnInit, OnDestroy {

    public selectedItems: AutogenEditElementsModel = null;
    public showMode = 'datatable';

    @Input() parentItem: any;
    @Input() loadOnInit: boolean;
    @Input() usedInChild: AutogenUsedinchildModel[];
    @Input() showInPopup = false;

    constructor(
      protected _fuseConfigService: FuseConfigService,
      protected _notificationService: NotificationService,
      protected _cacheService: AppCacheService,
      protected _autogenService: AutogenService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
    }

    ngOnInit(): void {
        this.onInit();
    }

    doEditItems(elements: AutogenEditElementsModel): void {
        this.selectedItems = elements;
        this.showMode = 'form';
    }

    doListItems(): void {
        this.showMode = 'datatable';
        this.loadOnInit = true;
    }
}
