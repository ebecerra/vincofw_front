import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AutogenComponent} from '../autogen.component';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {UtilitiesService} from '../../services/utilities.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AdHookNames, HookService} from '../../services/hook.service';

@Component({
    selector: 'autogen-datatable-header',
    templateUrl: './autogen.datatable.header.component.html',
    styleUrls: ['./autogen.datatable.header.component.scss']
})
export class AutogenDatatableHeaderComponent extends AutogenComponent implements OnInit, OnDestroy {

    public customButtons: AdFieldDtoModel[];
    btnSelectColumns = false;

    @Input() mode: string;
    @Input() btnRefresh = true;
    @Input() btnExportItem = true;
    @Input() btnSortable = false;
    @Input() btnMultiSelect = false;
    @Input() btnNewItem = true;
    @Input() btnEditItem = true;
    @Input() btnDeleteItem = true;

    @Output() refresh: EventEmitter<any> = new EventEmitter();
    @Output() exportItem: EventEmitter<any> = new EventEmitter();
    @Output() sort: EventEmitter<any> = new EventEmitter();
    @Output() multiSelect: EventEmitter<any> = new EventEmitter();
    @Output() newItem: EventEmitter<any> = new EventEmitter();
    @Output() editItem: EventEmitter<any> = new EventEmitter();
    @Output() deleteItem: EventEmitter<any> = new EventEmitter();
    @Output() selectColumns: EventEmitter<any> = new EventEmitter();
    @Output() listItems: EventEmitter<any> = new EventEmitter();
    @Output() jsCode: EventEmitter<any> = new EventEmitter();
    @Output() multiSelectSave: EventEmitter<any> = new EventEmitter();
    @Output() multiSelectCancel: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor(
        protected _fuseConfigService: FuseConfigService,
        protected _notificationService: NotificationService,
        protected _cacheService: AppCacheService,
        protected _autogenService: AutogenService,
        private _globalEventsService: GlobalEventsService,
        private _utilitiesService: UtilitiesService,
        private _hookService: HookService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
    }

    ngOnInit(): void {
        // Filter custom buttons
        this.customButtons = [];
        this.tab.tab.fields.forEach(fld => {
            if (fld.displayed && fld.relatedColumn.reference.rtype === 'BUTTON' && fld.relatedColumn.reference.refButton.showMode === 'IN_LIST') {
                this.customButtons.push(fld);
            }
        });
        if (this.tab.tab.gui.tableType === 'custom-table') {
            this.btnSelectColumns = false;
        } else {
            this._cacheService.getPreference('CanChangeViewColumns').subscribe(value => {
                this.btnSelectColumns = this._utilitiesService.getBooleanValue(value.value);
            });
        }
        if (this.tab.tab.uipattern === 'READONLY' || this.tab.tab.uipattern === 'EDIT' || this.tab.tab.uipattern === 'EDIT_DELETE') {
            this.btnNewItem = false;
        }
        if (this.tab.tab.uipattern === 'READONLY' || this.tab.tab.uipattern === 'EDIT' || this.tab.tab.uipattern === 'EDIT_ADD') {
            this.btnDeleteItem = false;
        }
    }

    doRefresh(): void {
        this.refresh.emit();
    }

    doExportItem(): void {
        this.exportItem.emit();
    }

    doSort(): void {
        this.sort.emit();
    }

    doMultiSelect(): void {
        this.multiSelect.emit();
    }

    doNewItem(): void {
        this._hookService.execHook(
            AdHookNames.AD_CAN_NEW_ITEM, {
                tab: this.tab.tab,
                origin: 'LIST'
            }
        ).then(response => {
            if (response.success) {
                this.newItem.emit();
            }
        }).catch(() => {
            // Nothing to do
        });
    }

    doEditItem(): void {
        this.editItem.emit();
    }

    doDeleteItem(): void {
        this.deleteItem.emit();
    }

    doSelectColumns(): void {
        this.selectColumns.emit();
    }

    doList(): void {
       this.listItems.emit();
    }

    doMultiSelectSave(): void {
        this.multiSelectSave.emit();
    }

    doMultiSelectCancel(): void {
        this.multiSelectCancel.emit();
    }

    doCustomAction(btn): void {
        switch (btn.relatedColumn.reference.refButton.btype) {
            case 'PROCESS':
                this._globalEventsService.notifyProcessOpen(btn.relatedColumn.reference.refButton.idProcess, btn.relatedColumn.reference.refButton.btype);
                break;

            case 'JAVASCRIPT':
                console.log('Execute JS: ' + btn.relatedColumn.reference.refButton.jsCode);
                this.jsCode.emit(btn.relatedColumn.reference.refButton.jsCode);
                break;

            default:
                this._notificationService.showWarning('Custom button actions (' + btn.caption + ') invalid type: ' + btn.relatedColumn.reference.refButton.btype);
        }
    }
}
