import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {HttpParams} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {takeUntil} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {Page} from '../../models/page';
import {PagedContentModel, SortData} from '../../models/ad.base.model';
import {AutogenUsedinchildModel} from '../../models/autogen.usedinchild.model';
import {AdTabMainDtoModel} from '../../models/dto/ad.tab.main.dto.model';
import {AutogenService} from '../../services/autogen.service';
import {UtilitiesService} from '../../services/utilities.service';
import {ConditionalService} from '../../services/conditional.service';
import {AppCacheService} from '../../services/cache.service';
import {AppCoreSettings} from '../../configs/app.core.settings';
import {LocalstoreService} from '../../services/localstore.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AdHookNames, HookService} from '../../services/hook.service';
import {AuthService} from '../../authentication/auth.service';
import {GridFieldsComponent} from '../grid.fields/grid.fields.component';
import {AutogenMultiselectComponent} from '../autogen.multiselect/autogen.multiselect.component';
import {NotificationService} from '../../notification/notification.service';
import {AutogenComponent} from '../autogen.component';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

@Component({
    selector: 'autogen-datatable',
    templateUrl: './autogen.datatable.component.html',
    styleUrls: ['./autogen.datatable.component.scss']
})
export class AutogenDatatableComponent extends AutogenComponent implements OnInit, OnDestroy {

    rows = [];
    columns: any[] = [];
    selected: any[] = [];
    sortable = false;
    multiSelect = false;
    sortStart: number = null;
    sortIncrement: number = null;
    sortColumnName: string = null;
    mode = 'list';
    sortTab: AdTabMainDtoModel;
    multiSelectTab: AdTabMainDtoModel;
    page = new Page();

    // Private
    private _filters: any = null;
    private _sort: SortData[];
    private _content: PagedContentModel<any>;
    private subscriptions: Subscription[] = [];
    private parentTab: AdTabMainDtoModel;
    private contentLoaded: boolean;
    private tabActive: boolean;

    @ViewChild('multiselect') multiselectComponent: AutogenMultiselectComponent;

    // MatPaginator Inputs
    pageSizeOptions: number[] = [10, 20, 50, 100];

    @Input() parentItem: any;
    @Input() loadOnInit: boolean;
    @Input() usedInChild: AutogenUsedinchildModel[];

    @Input()
    set content(content: PagedContentModel<any>) {
        this._content = content;
        // Assign the data to the data source for the table to render
        this.rows = content.content;
        this.page.pageNumber = this._content.number;
        this.page.totalElements = content.totalElements;
    }

    get content(): PagedContentModel<any> {
        return this._content;
    }

    @Output()
    public selectedElementsChanged: EventEmitter<any[]>;

    @Output()
    public editItems: EventEmitter<any> = new EventEmitter();

    @ViewChild('drawer') drawer;

    /**
     * Constructor
     */
    constructor(
        protected _fuseConfigService: FuseConfigService,
        protected _notificationService: NotificationService,
        protected _cacheService: AppCacheService,
        protected _autogenService: AutogenService,
        private _authService: AuthService,
        private _coreSettings: AppCoreSettings,
        private _utilitiesService: UtilitiesService,
        private _localstoreService: LocalstoreService,
        private _conditionalService: ConditionalService,
        private _globalEventsService: GlobalEventsService,
        private _hookService: HookService,
        private _bottomSheet: MatBottomSheet
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
        this.selectedElementsChanged = new EventEmitter();
        this._content = new PagedContentModel<any>();
        this.subscriptions.push(
            this._globalEventsService.itemChanged$.subscribe(data => {
                if (this.parentTab && this.parentTab.idTab === data.idTab && this.tabActive) {
                    console.log('AutogenDatatableComponent: itemChanged$(' + this.tab.name + ')');
                    this.doLoadContent(data.item);
                }
            })
        );
        this.subscriptions.push(
            this._globalEventsService.selectedTabChange$.subscribe(data => {
                if (this.parentTab && this.parentTab.idTab === data.idFormTab) {
                    this.tabActive = this.tab.idTab === data.idTab;
                    console.log('AutogenDatatableComponent: selectedTabChange$(' + this.tab.name + ', active = ' + this.tabActive + ')');
                    if (this.tabActive) {
                        this.doLoadContent(this.parentItem);
                    }
                }
            })
        );
    }

    ngOnInit(): void {
        console.log('AutogenDatatableComponent: ngOnInit(' + this.tab.name + ', loadOnInit = ' + this.loadOnInit + ')');
        this.contentLoaded = false;
        this.tabActive = this.loadOnInit;
        this.page = this._localstoreService.loadPage(this.tab.idTab);
        const sort = this._localstoreService.loadSort(this.tab.idTab);
        this._sort = sort ? [sort] : null;
        const window = this._cacheService.getWindowFromCache(this.tab.idWindow);
        this.parentTab = this._cacheService.getParentTab(window.mainTab, this.tab.idTab);
        // Evaluate tab.sqlwhere
        if (this.tab.tab.sqlwhere) {
            this.tab.tab.runtimeSqlwhere = this._conditionalService.replaceJSConditional(this.tab.tab.sqlwhere, null, null, this.parentItem);
        }
        if (this.loadOnInit && !this.tab.tab.hasFilters) {
            this.doLoadContent(this.parentItem);
        }
        // Sortable and multiple select
        if (!isNullOrUndefined(this.tab.tab.table.idSortColumn)) {
            this.sortTab = this.findChildTab(window.mainTab.childTabs, this.tab.tab.table.idTable, 'SORTABLE');
            this.sortable = !isNullOrUndefined(this.sortTab);
            if (this.sortable) {
                const sortColumn = this.tab.tab.table.columns.find(col => col.idColumn === this.tab.tab.table.idSortColumn);
                if (isNullOrUndefined(sortColumn)) {
                    this.sortable = false;
                    this.sortTab = null;
                } else {
                    this.sortColumnName = sortColumn.name;
                }
            }
        }
        this.multiSelectTab = this.findChildTab(window.mainTab.childTabs, this.tab.tab.table.idTable, 'MULTISELECT');
        this.multiSelect = !isNullOrUndefined(this.multiSelectTab);
        this.onInit();
    }

    ngOnDestroy(): void {
        console.log('AutogenDatatableComponent: ngOnDestroy(' + this.tab.name + ')');
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.subscriptions.forEach(subscrition => subscrition.unsubscribe());
    }

    /**
     * Find child tab (recursive)
     *
     * @param childTabs Child tabs list
     * @param idTable Table identifier
     * @param uipattern Tab UI Pattern
     */
    private findChildTab(childTabs: AdTabMainDtoModel[], idTable: string, uipattern: string): AdTabMainDtoModel {
        const result = childTabs.find(tab => tab.uipattern === uipattern && tab.idTable === idTable);
        if (isNullOrUndefined(result)) {
            for (const tab of childTabs) {
                if (tab.childTabs && tab.childTabs.length > 0) {
                    return this.findChildTab(tab.childTabs, idTable, uipattern);
                }
            }
        }
        return result;
    }

    /**
     * Get filters
     *
     * @param parentItem Parent item value
     */
    private getFilterParams(parentItem): Promise<HttpParams> {
        return new Promise((resolve, reject) => {
            let q = '';
            if (parentItem && this.tab.tab.tablevel > 0) {
                // Filter table for parent item
                let linkedToParent = this.tab.tab.table.columns.filter(column => column.linkParent);
                if (linkedToParent.length > 1) {
                    const pkName = this.parentTab.tab.table.primaryKey.name;
                    linkedToParent = linkedToParent.filter(c => c.name === pkName);
                }
                if (linkedToParent.length > 0) {
                    linkedToParent.forEach(column => {
                        const linkColumn = this.parentTab.tab.table.columns.find(col => col.name === column.name || col.name === column.linkColumn);
                        if (linkColumn) {
                            q += (q === '' ? '' : ',') + column.name + '=' + parentItem[linkColumn.name];
                        }
                    });
                } else {
                    console.error('No columns linked to parent. Parent: ' + this.parentTab.name + ', Tab: ' + this.tab.name);
                }
            }
            if (this.tab.tab.table.idTable === VINCOFW_CONFIG.TABLES.AD_FILE || this.tab.tab.table.idTable === VINCOFW_CONFIG.TABLES.AD_IMAGE) {
                q += ',idTable=' + this.tab.tab.idParentTable;
            }
            const qExtra = this._utilitiesService.getFilterParams(this._filters, this.tab.tab.table.columns);
            q = q + (q !== '' ? ',' : '') + qExtra;
            this._hookService.execHook(
                AdHookNames.AD_QUERY_TABLE, {
                    tableName: this.tab.tab.table.name,
                    constraints: q
                }
            ).then(response => {
                if (response.success) {
                    response.results.forEach(r => q += ',' + r);
                    if (this.tab.tab.runtimeSqlwhere) {
                        q += ',$extended$={' + this.tab.tab.runtimeSqlwhere + '}';
                    }
                    let parameters = new HttpParams();
                    parameters = parameters.set('q', q);
                    resolve(parameters);
                } else {
                    reject();
                }
            });
        });
    }

    /**
     * Loads the table content
     *
     * @param parentItem Parent item value
     */
    private doLoadContent(parentItem): void {
        if (!isNullOrUndefined(this._service) && !isNullOrUndefined(this.page.size)) {
            if (isNullOrUndefined(this.usedInChild)) {
                this.usedInChild = [];
            }
            const visibleColumns = this._utilitiesService.getGridColumns(this.tab.tab.table.columns, this.tab.tab.fields, this.tab.tab.table.actions);
            visibleColumns.forEach(column => {
                if (column.reference.rtype === 'LIST' && !column.reference.refList) {
                    console.log('TODO: load list values');
                }
                if (column.reference.rtype === 'TABLEDIR') {
                    this._cacheService.getReferenceValues(column.reference, this.parentItem).subscribe();
                }
            });
            this.getFilterParams(parentItem).then(parameters => {
                this.contentLoaded = true;
                this._service.list(parameters, this._sort, this.page.size, this.page.pageNumber + 1)
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe(result => {
                        this.content = result;
                        if (this.usedInChild.length > 0) {
                            const parent = {};
                            this.usedInChild.forEach(used => parent[used.name] = used.value);
                            this.content.content = this.content.content.map(item => {
                                return {
                                    ...parent,
                                    ...item
                                };
                            });
                        }
                        if (this.sortable) {
                            this.setSortFilters();
                        }
                    }
                );
            });
        }
    }

    /**
     * Show data grid
     */
    doListItems(): void {
        this.sortStart = null;
        this.sortIncrement = null;
        if (this.mode === 'sort') {
            this.doLoadContent(this.parentItem);
        }
        this.mode = 'list';
    }

    /**
     * Refresh data content
     */
    doRefresh(): void {
        this.doLoadContent(this.parentItem);
    }

    /**
     * Export table content
     */
    doExportData(): void {
        if (!isNullOrUndefined(this._service)) {
            const user = this._authService.getUserInfo();
            if (user) {
                this.getFilterParams(this.parentItem).then(parameters => {
                    this._service.exportData(parameters, this._sort, this.tab.idTab, user.idUserLogged)
                        .pipe(
                            takeUntil(this._unsubscribeAll)
                        )
                        .subscribe(data => this._utilitiesService.downloadFile(data, this.tab.tab.table.standardName + '.xls'));
                });
            }
        }
    }

    /**
     * Open sort tab
     */
    doSort(): void {
        this.setSortFilters();
        this.mode = 'sort';
    }

    /**
     * Open multiple select items tab
     */
    doMultiSelect(): void {
        this.mode = 'multiselect';
    }

    /**
     * Create a new item
     */
    doNewItem(): void {
        this.editItems.emit({ items: null, index: 0 });
    }

    /**
     * Edit a selected items
     */
    doEditItem(): void {
        if (this.selected.length > 0) {
            if (this.selected.length === 1) {
                const idx = this.content.content.findIndex(value => value.id === this.selected[0].id);
                this.editItems.emit({ items: this.content.content, index: idx });
            } else {
                this.editItems.emit({ items: this.selected, index: 0 });
            }
        } else {
            const msg = this._cacheService.getTranslation('AD_msgSelectItem');
            this._notificationService.showWarning(msg, 'right', 'top');
        }
    }

    /**
     * Delete selected items
     */
    doDelete(): void {
        if (this.selected.length > 0) {
            if (!isNullOrUndefined(this._service)) {
                const ids = [],
                    names = [],
                    field = this._utilitiesService.getIdentifierColumn(this.tab.tab.table.columns, this.tab.tab.fields);
                this.selected.forEach(item => {
                    ids.push(item.id);
                    names.push(item[field]);
                    // TODO: Cargar las referencias
                });
                const dialogRef = this._notificationService.confirmDialog(
                    this._cacheService.getTranslation('AD_GlobalConfirm'),
                    this._cacheService.getTranslation('AD_msgConfirmDeleteItems'),
                    'warning', 'confirm-icon-warn', names,
                    this.selected.length > 1 ?
                        this._cacheService.getTranslation('AD_msgConfirmDeleteMultiple', [this.selected.length]) :
                        this._cacheService.getTranslation('AD_msgConfirmDelete')
                );
                dialogRef.afterClosed().subscribe(result => {
                    if (result === 'OK') {
                        this._service.delete(ids)
                            .pipe(takeUntil(this._unsubscribeAll))
                            .subscribe(
                                data => {
                                    console.log('Delete items is OK');
                                    this.doLoadContent(this.parentItem);
                                },
                                response => {
                                    console.error('Status:' + response.status + ', Error: ' + response.statusText);
                                    const msg = this._utilitiesService.getValidationsErrors(response.error, this.tab.tab.table.columns, this.tab.tab.fields);
                                    this._notificationService.showError(msg, 'right', 'top');
                                }
                            );
                    }
                });
            }
        } else {
            const msg = this._cacheService.getTranslation('AD_msgSelectItem');
            this._notificationService.showWarning(msg, 'right', 'top');
        }
    }

    /**
     * Change grid columns
     */
    doSelectColumns(): void {
        let backdrop = false;
        const bottomSheet = this._bottomSheet.open(
            GridFieldsComponent, {
                data: {
                    idTab: this.tab.tab.idTab,
                    buttonType: this.tab.tab.gui.button,
                    fields: this.tab.tab.fields
                }
            });
        bottomSheet.afterDismissed().subscribe(data => {
            if (!backdrop) {
                this._cacheService.removeWindowFromCache(this.tab.idWindow);
                this._globalEventsService.requestReloadedWindow(this.tab.idWindow);
            }
        });
        bottomSheet.backdropClick().subscribe(data => {
            backdrop = true;
        });
    }

    /**
     * Execute JS / TS code
     *
     * @param code Code name
     */
    doJSCode(code): void {
        this._globalEventsService.notifyExecuteCode(code, 'LIST', this.selected);
    }

    /**
     * Change page size
     */
    doChangePageSize(): void {
        this._localstoreService.saveTabInfo(this.tab.idTab, null, this.page, null);
        this.doLoadContent(this.parentItem);
    }

    /**
     * Save multiselection form
     */
    multiSelectSave(): void {
        this.multiselectComponent.doSave();
    }

    /**
     * Cancel multiselection form
     */
    multiSelectCancel(): void {
        this.mode = 'list';
    }

    notImplementedMessage(): void {
        this._notificationService.showWarning('No se ha implementado / To be implemented');
    }

    /**
     * Handles the sort event
     */
    sortData(sortData: SortData[] = []): void {
        this._sort = sortData;
        if (sortData && sortData.length > 0) {
            this._localstoreService.saveTabInfo(this.tab.idTab, null, null, sortData[0]);
        }
        this.doLoadContent(this.parentItem);
    }

    doSelect(elements: any[]): void {
        this.selected = elements;
        this.selectedElementsChanged.emit(this.selected);
    }

    doEditSelected(elements: any[]): void {
        this.doSelect(elements);
        this.doEditItem();
    }

    filterChanged(filters: any): void {
        this._filters = filters;
        this.page.pageNumber = 0;
        this._localstoreService.saveTabInfo(this.tab.idTab, null, this.page, null);
        this.doLoadContent(this.parentItem);
    }

    changePage(page: Page): void {
        if (this.page.pageNumber !== page.pageNumber) {
            this.page = JSON.parse(JSON.stringify(page));
            this.doLoadContent(this.parentItem);
            this._localstoreService.saveTabInfo(this.tab.idTab, null, this.page, null);
        } else {
            this.page = JSON.parse(JSON.stringify(page));
        }
    }

    /**
     * Set sort filter values
     */
    private setSortFilters(): void {
        this.sortStart = this._coreSettings.MAX_SEQNO;
        this._content.content.forEach(item => {
            if (item[this.sortColumnName] < this.sortStart) {
                this.sortStart = item[this.sortColumnName];
            }
        });
        if (this.sortStart === this._coreSettings.MAX_SEQNO) {
            this.sortStart = 10;
        }
        this.sortIncrement = 10;
        if (this._content.content.length > 1) {
            const diff = this._content.content[1][this.sortColumnName] - this._content.content[0][this.sortColumnName];
            if (diff >= 1000) {
                this.sortIncrement = 1000;
            } else if (diff >= 500) {
                this.sortIncrement = 500;
            } else if (diff >= 100) {
                this.sortIncrement = 100;
            } else if (diff >= 50) {
                this.sortIncrement = 50;
            } else if (diff >= 10) {
                this.sortIncrement = 10;
            } else if (diff >= 1) {
                this.sortIncrement = 1;
            }
        }
    }

}
