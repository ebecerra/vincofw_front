import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdGUIDtoModel} from '../../models/dto/ad.gui.dto.model';
import {AdTableDtoModel} from '../../models/dto/ad.table.dto.model';
import {ControllerResultModel} from '../../models/controller.result.model';
import {AppCacheService} from '../../services/cache.service';
import {AdHookNames, HookService} from '../../services/hook.service';
import {UtilitiesService} from '../../services/utilities.service';
import {NotificationService} from '../../notification/notification.service';
import {HelpShowComponent} from '../../components/help.show/help.show.component';
import {TranslationDialogComponent} from '../translation.dialog/translation.dialog.component';
import {FormEditorDialogComponent} from '../form.editor.dialog/form.editor.dialog.component';
import {AdRowColumnModel, AdRowFieldModel, AdRowGridTemplateColumnsModel} from '../../models/ad.grid.model';

@Component({
    selector: 'autogen-fields',
    templateUrl: './autogen.fields.component.html',
    styleUrls: ['./autogen.fields.component.scss']
})
export class AutogenFieldsComponent implements OnInit {

    constructor(
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _hookService: HookService,
        private _utilitiesService: UtilitiesService,
        private _bottomSheet: MatBottomSheet,
        private _dialog: MatDialog
    ) {
    }

    @Input() allFields: AdFieldDtoModel[];
    @Input() fields: AdFieldDtoModel[];
    @Input() item: any;
    @Input() gui: AdGUIDtoModel;
    @Input() autogenForm: FormGroup;
    @Input() table: AdTableDtoModel;
    @Input() showInPopup = false;
    @Input() gridColumns: string;

    rows: AdRowFieldModel[];
    rowId: string;

    private static buildGridColumns(gridColumns): AdRowGridTemplateColumnsModel[] {
        const result = [];
        const rows = gridColumns.split('\n');
        let lastRow = '3|repeat(3, 1fr)';
        rows.forEach(row => {
            let item = row.trim();
            if (item === '' || item === '-' || item === '.') {
                item = lastRow;
            }
            let itemInfo = item.split('|');
            if (itemInfo.length !== 2) {
                itemInfo = lastRow.split('|');
            }
            let columns = parseInt(itemInfo[0], 10);
            const gridTemplateColumns = itemInfo[1].trim();
            if (isNaN(columns) || columns < 1 || columns > 6) {
                columns = 3;
            }
            result.push({
                columns: columns,
                gridColumns: gridTemplateColumns
            });
            lastRow = `${columns}|${gridTemplateColumns}`;
        });
        return result;
    }

    private static getEmptyRow(rowTemplates: AdRowGridTemplateColumnsModel[], rowIndex: number): AdRowFieldModel {
        const row = rowIndex < rowTemplates.length ? rowTemplates[rowIndex] : rowTemplates[rowTemplates.length - 1];
        return {
            cssGridTemplateColumns: row.gridColumns,
            cssGrid: 'fields-w' + row.columns + '_',
            colWidth: row.columns,
            columns: []
        };
    }

    private static getFinalRowCssGrid(columns: number, cssGrid: string): string {
        const cssGridTokens = cssGrid.split('_');
        const cols = cssGridTokens[1].split('-');
        let i;
        for (i = cols.length + 1; i <= columns; i++) {
            cols.push('' + i);
        }
        return cssGridTokens[0] + '_' + cols.join('-');
    }

    public static buildFormGrid(gridColumns: string, fields: AdRowColumnModel[]): AdRowFieldModel[] {
        const gridTemplateColums = AutogenFieldsComponent.buildGridColumns(gridColumns);
        let i, count = 0, colIndex = 1, rowIndex = 0;
        let row: AdRowFieldModel = AutogenFieldsComponent.getEmptyRow(gridTemplateColums, rowIndex);
        const rows = [];
        fields.forEach(fld => {
            if (fld.displayed && fld.columnReferenceType !== 'BUTTON') {
                if ((fld.startnewrow || row.colWidth < count + fld.span) && row.columns.length > 0) {
                    row.cssGrid = AutogenFieldsComponent.getFinalRowCssGrid(row.colWidth, row.cssGrid);
                    rows.push(row);
                    rowIndex++;
                    row = AutogenFieldsComponent.getEmptyRow(gridTemplateColums, rowIndex);
                    count = 0;
                    colIndex = 1;
                }
                fld.cssGridArea = 'fields-area-' + colIndex;
                // @ts-ignore
                row.columns.push(fld);
                count += fld.span;
                row.cssGrid += (colIndex === 1 ? '' : '-') + colIndex;
                if (fld.span > 1) {
                    for (i = 1; i < fld.span; i++) {
                        row.cssGrid += '-' + colIndex;
                    }
                }
                colIndex += fld.span;
            }
        });
        row.cssGrid = AutogenFieldsComponent.getFinalRowCssGrid(row.colWidth, row.cssGrid);
        rows.push(row);
        return rows;
    }

    ngOnInit(): void {
        this.fields.forEach(fld => {
            if (fld.relatedColumn.primaryKey) {
                this.rowId = this.item[fld.columnName];
            }
        });
        this.rows = AutogenFieldsComponent.buildFormGrid(this.gridColumns, this.fields);
    }

    doAction(event, fld): void {
        if (event === 'help') {
            this._bottomSheet.open(HelpShowComponent, { data: { message: fld.relatedColumn.description } });
        } else if (event === 'translate') {
            const pk = this.allFields.find(f => f.relatedColumn.primaryKey);
            this._dialog.open(TranslationDialogComponent, {
                width: '75%',
                disableClose: true,
                data: {
                    idTable: fld.relatedColumn.idTable,
                    idColumn: fld.relatedColumn.idColumn,
                    rowkey: this.item[pk.relatedColumn.name],
                    editType: fld.relatedColumn.reference.rtype === 'HTML' ? 'HTML' : (fld.relatedColumn.reference.rtype === 'TEXT' ? 'TEXTAREA' : 'TEXT')
                }
            });
        } else {
            this._notificationService.showWarning('Acción: ' + event + ', Campo: ' + fld.caption);
        }
    }

    doOpenWindow(event, fld: AdFieldDtoModel): void {
        const dialogRef = this._dialog.open(FormEditorDialogComponent, {
            width: '80%',
            data: {
                rowkey: this.item[fld.columnName],
                menu: event,
                field: fld,
                parentItem: this.item
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result.success) {
                if (fld.relatedColumn.reference.rtype === 'TABLEDIR') {
                    this._cacheService.getReferenceValues(fld.relatedColumn.reference, this.item, true).subscribe(values => {
                        this._hookService.execHook(
                            AdHookNames.AD_LIST_CHANGE_VALUES, {
                                fieldName: fld.columnName,
                                values: values
                            }
                        ).then();
                        const value = result.item[fld.relatedColumn.reference.refTable.keyName];
                        if (this.item[fld.columnName] !== value) {
                            this.autogenForm.get(fld.columnName).setValue(value);
                        }
                    });
                } else if (fld.relatedColumn.reference.rtype === 'TABLE') {
                    this._utilitiesService.setFormValue(this.autogenForm, fld.columnName, fld.relatedColumn.reference, fld.readonly, result.item, fld.relatedColumn);
                }
            }
        });
    }

    doComplete(event: ControllerResultModel): void {
        if (event.success) {
            const value = {};
            value[event.properties.field] = event.properties.field + '$' + event.properties.id + '$' + event.properties.name;
            this.autogenForm.patchValue(value);
        }
    }

    doDelete(fld: AdFieldDtoModel): void {
        this.item[fld.columnName] = null;
    }
}
