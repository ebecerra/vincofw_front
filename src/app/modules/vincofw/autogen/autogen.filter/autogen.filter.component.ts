import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {AdTabDtoModel} from '../../models/dto/ad.tab.dto.model';
import {AdColumnDtoModel} from '../../models/dto/ad.column.dto.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdFormEditorModel} from '../../models/ad.form.editor.model';
import {AdBaseModel} from '../../models/ad.base.model';
import {AdReferenceKeyValueModel} from '../../models/dto/ad.reference.key.value.model';
import {AppCacheService} from '../../services/cache.service';
import {LocalstoreService} from '../../services/localstore.service';
import {UtilitiesService} from '../../services/utilities.service';
import {ConditionalService} from '../../services/conditional.service';
import {AdHookNames, HookService} from '../../services/hook.service';
import {AdReferenceService} from '../../services/ad.reference.service';
import {AppCoreSettings} from '../../configs/app.core.settings';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

@Component({
    selector: 'autogen-filter',
    templateUrl: './autogen.filter.component.html',
    styleUrls: ['./autogen.filter.component.scss']
})
export class AutogenFilterComponent implements OnInit, OnDestroy {

    // Private
    private _orientation: string;
    private _unsubscribeAll: Subject<any>;
    private editors: AdFormEditorModel[];
    private fields: AdFieldDtoModel[];

    tab: AdTabDtoModel;
    autogenForm: FormGroup;
    filterColumns: AdColumnDtoModel[];
    filterValues: any;
    groupedSearch = false;

    @Input() delay = 300;
    @Input() mode: string;
    @Input() parentItem: any = null;

    @Input()
    set orientation(orientation: string){
        if (orientation.toLocaleLowerCase() !== 'vertical' && orientation.toLocaleLowerCase() !== 'horizontal') {
            throw new Error('Invalid orientation value, should be "horizontal" or "vertical"');
        }
        this._orientation = orientation.toLocaleLowerCase();
    }

    get orientation(): string{
        return this._orientation;
    }

    @Input()
    set relatedTab(tab: AdTabDtoModel) {
        console.log('AutogenFilterComponent: relatedTab(' + tab + ')');
        if (isNullOrUndefined(this.tab)) {
            this.tab = tab;
            const storeFilterValues = this._localstoreService.loadFilters(this.tab.idTab);
            if (this.tab.table && this.tab.table.idTable) {
                this.filterColumns = this.tab.table.columns.filter(column => column.showinsearch && !column.groupedsearch);
                const groupedSearchFields = this.tab.table.columns.filter(column => column.showinsearch && column.groupedsearch);
                if (groupedSearchFields.length > 0) {
                    this.groupedSearch = true;
                    this.filterValues['search'] = isNullOrUndefined(storeFilterValues['search']) ? '' : storeFilterValues['search'];
                }
            } else if (this.tab.chartFilters) {
                this.tab.chartFilters.forEach(flt => {
                    const field = new AdFieldDtoModel();
                    field.idField = flt.idChartFilter;
                    field.idColumn = flt.idChartFilter;
                    field.caption = flt.caption;
                    field.seqno = flt.seqno;
                    field.displayed = flt.displayed;
                    field.displaylogic = flt.displaylogic;
                    field.span = 1;
                    field.filterRange = flt.ranged;
                    field.columnName = flt.name;
                    field.filterDefaultValue = flt.valuedefault;
                    field.filterDefaultValue2 = flt.valuedefault2;
                    field.valuemin = flt.valuemin;
                    field.valuemax = flt.valuemax;
                    const item = new AdColumnDtoModel();
                    item.idColumn = flt.idChartFilter;
                    item.name = flt.name;
                    item.description = flt.description;
                    item.mandatory = flt.mandatory;
                    item.reference = flt.reference;
                    item.relatedField = field;
                    field.relatedColumn = item;
                    this.filterColumns.push(item);
                });
            }
            this.fields = [];
            this.filterColumns.forEach(current => {
                this.fields.push(current.relatedField);
                if (this._utilitiesService.isColumnRangeFilter(current)) {
                    this.resetFilterValue('$from$' + current.name, current.relatedField.filterDefaultValue, storeFilterValues, current.reference.rtype);
                    this.resetFilterValue('$to$' + current.name, current.relatedField.filterDefaultValue2, storeFilterValues, current.reference.rtype);
                } else {
                    this.resetFilterValue(current.name, current.relatedField.filterDefaultValue, storeFilterValues, current.reference.rtype);
                }
            });
            this.onFillFilter.emit({ columns: this.filterColumns });
            const idReferences = this.filterColumns.filter(col => !col.reference.base).map(current => current.reference.idReference);
            if (idReferences.length > 0) {
                this._cacheService.getReferenceInfo(idReferences).pipe(takeUntil(this._unsubscribeAll)).subscribe(references => {
                    this.doFilter();
                });
            } else {
                this.doFilter();
            }
        }
    }

    @Output() onFilterChanged: EventEmitter<any> = new EventEmitter();
    @Output() onFillFilter: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor(
        private _coreSettings: AppCoreSettings,
        private _cacheService: AppCacheService,
        private _localstoreService: LocalstoreService,
        private _conditionalService: ConditionalService,
        private _utilitiesService: UtilitiesService,
        private _referenceService: AdReferenceService,
        private _hookService: HookService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.filterColumns = [];
        this.filterValues = { };
        this._orientation = 'horizontal';
    }

    ngOnInit(): void {
        console.log('AutogenFilterComponent: ngOnInit()');
        // Create reactive form
        const formElements = { };
        this.editors = [];
        Object.keys(this.filterValues).forEach(key => {
            formElements[key] = new FormControl();
            this.editors.push(new AdFormEditorModel(key));
        });
        this.autogenForm = new FormGroup(formElements);
        // Set form values
        Object.keys(this.filterValues).forEach(key => {
            let colName = key;
            if (key.indexOf('$from$') === 0) {
                colName = key.substring(6);
            }
            if (key.indexOf('$to$') === 0) {
                colName = key.substring(4);
            }
            const column = this.filterColumns.find(col => col.name === colName);
            if (column) {
                this._utilitiesService.setFormValue(this.autogenForm, key, column.reference, false, this.filterValues, column);
            } else {
                this.autogenForm.get(key).setValue(this.filterValues[key]);
            }
        });
        // Set editor dependency
        this.filterColumns.forEach(col => {
            const editor: AdFormEditorModel = this.editors.find(edt => edt.name === col.name);
            if (editor) {
                if (col.relatedField.displayed && col.relatedField.displaylogic) {
                    this._utilitiesService.addFieldDependency(editor, this._conditionalService.getListenersField(col.relatedField.displaylogic), 'displayListeners');
                }
                if (VINCOFW_CONFIG.sqlDependReferences.indexOf(col.reference.rtype) >= 0) {
                    this._utilitiesService.addFieldDependency(editor, this._conditionalService.getListenersField(col.reference.refTable.sqlwhere), 'sqlListeners');
                }
            }
        });
        this.notifyChanges(this.filterValues);
        // Listen form changes
        this.autogenForm.valueChanges.pipe(
            debounceTime(this.delay),
            distinctUntilChanged()
        ).subscribe(data => {
            let filterChanged = false;
            Object.keys(this.filterValues).forEach(key => {
                if (this.filterValues[key] !== data[key]) {
                    filterChanged = true;
                    this.filterValues[key] = data[key];
                }
            });
            if (filterChanged) {
                this.notifyChanges(data);
                if (this.tab.gui.filterApply === 'auto') {
                    this.doFilter();
                }
            }
        });
    }

    ngOnDestroy(): void {
        console.log('AutogenFilterComponent: ngOnDestroy()');
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    doFilter(): void {
        this._localstoreService.saveTabInfo(this.tab.idTab, this.filterValues, null, null);
        this.onFilterChanged.emit(this.filterValues);
    }

    doClear(): void {
        // Set form values
        Object.keys(this.filterValues).forEach(key => {
            this.filterValues[key] = '';
            this.autogenForm.get(key).setValue(this.filterValues[key]);
        });
        if (this.tab.gui.filterApply === 'auto') {
            this.doFilter();
        }
    }

    /**
     * Reset filter value to initial or store value
     *
     * @param name Filter name
     * @param valuedefault Default value
     * @param storeFilterValues Store filter values
     * @param referenceType Reference type
     */
    private resetFilterValue(name: string, valuedefault: string, storeFilterValues: any, referenceType: string): void {
        if (valuedefault) {
            const value = this._conditionalService.replaceFixedConditional(valuedefault);
            this.filterValues[name] = referenceType === 'YESNO' ? eval(value) : value;
        } else {
            this.filterValues[name] = isNullOrUndefined(storeFilterValues[name]) ? '' : storeFilterValues[name];
        }
    }

    /**
     * Get item values to filter
     */
    private getItemToFilter(): AdBaseModel {
        const itemToFilter = new AdBaseModel();
        this.fields.forEach(fld => {
            const key = fld.columnName;
            if (this.autogenForm.value[key] instanceof AdReferenceKeyValueModel) {
                itemToFilter[key] = this.autogenForm.value[key].key;
            } else {
                itemToFilter[key] = this._utilitiesService.getFormValue(this.autogenForm.value[key], fld);
            }
        });
        return itemToFilter;
    }

    /**
     * Adjust field visibility
     *
     * @param field Field
     * @param data Form data
     */
    private adjustFieldDisplayLogic(field: AdFieldDtoModel, data: any): void {
        field.runtimeDisplaylogic = this._conditionalService.replaceJSConditional(field.runtimeDisplaylogic, field.relatedColumn, null, data);
        if (this._conditionalService.isCompleteConditional(field.runtimeDisplaylogic)) {
            field.runtimeDisplayed = eval(field.runtimeDisplaylogic);
            field.runtimeDisplaylogic = field.displaylogic;
        } else {
            field.runtimeDisplayed = false;
        }
    }

    /**
     * Filter changes listener
     *
     * @param data Form data
     */
    private notifyChanges(data: any): void {
        this.editors.forEach(edt => {
            if (edt.isModify(data)) {
                // Field Display logic
                const dependDisplayEditors = this.editors.filter(depEdt => depEdt.displayListeners.find(lst => lst === edt.name));
                dependDisplayEditors.forEach(depEdt => {
                    const column = this.filterColumns.find(col => col.name === depEdt.name);
                    this.adjustFieldDisplayLogic(column.relatedField, data);
                });
                // SQL where logic
                const dependSQLEditors = this.editors.filter(depEdt => depEdt.sqlListeners.find(lst => lst === edt.name));
                dependSQLEditors.forEach(depEdt => {
                    const column = this.filterColumns.find(col => col.name === depEdt.name);
                    this._referenceService.loadTabledir(this.fields, column.reference, this.getItemToFilter())
                        .pipe(takeUntil(this._unsubscribeAll))
                        .subscribe( values => {
                            this._hookService.execHook(
                                AdHookNames.AD_LIST_CHANGE_VALUES, {
                                    fieldName: column.name,
                                    values: values
                                }
                            ).then(() => {
                                const value = values.find(v => v.key === this.filterValues[column.name]);
                                if (!value) {
                                    this.filterValues[column.name] = '';
                                    this.autogenForm.get(column.name).setValue(this.filterValues[column.name]);
                                    this.doFilter();
                                }
                            });
                        }
                    );
                });
            }
        });
    }

}
