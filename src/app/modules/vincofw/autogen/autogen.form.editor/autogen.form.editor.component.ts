import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AdTabMainDtoModel} from '../../models/dto/ad.tab.main.dto.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdFieldGroupDtoModel} from '../../models/dto/ad.field.group.dto.model';

@Component({
    selector: 'autogen-form-editor',
    templateUrl: './autogen.form.editor.component.html',
    styleUrls: ['./autogen.form.editor.component.scss']
})
export class AutogenFormEditorComponent implements OnInit {

    ungroupedFields: AdFieldDtoModel[];
    fieldGroups: AdFieldGroupDtoModel[];
    loading: boolean;

    @Input() autogenForm: FormGroup;
    @Input() serverErrorMsg: string = null;
    @Input() currentItem: any = null;
    @Input() showInPopup = false;
    @Input() fields: AdFieldDtoModel[];
    @Input() tab: AdTabMainDtoModel;

    constructor() {
        this.loading = true;
    }

    ngOnInit(): void {
        // Build form groups
        this.ungroupedFields = [];
        this.tab.tab.ungroupedFields.forEach(fld => {
            if (this.fields.find(f => fld.idField  === f.idField)) {
                this.ungroupedFields.push(fld);
            }
        });
        this.fieldGroups = [];
        this.tab.tab.fieldGroups.forEach(fg => {
            const fldGrp = { ...fg };
            fldGrp.fields = [];
            fg.fields.forEach(fld => {
                if (this.fields.find(f => fld.idField  === f.idField)) {
                    fldGrp.fields.push(fld);
                }
            });
            if (fldGrp.fields.length > 0) {
                this.fieldGroups.push(fg);
            }
        });
        this.loading = false;
    }
}
