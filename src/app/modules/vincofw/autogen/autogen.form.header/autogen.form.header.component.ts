import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Subscription} from 'rxjs';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {AutogenComponent} from '../autogen.component';
import {NotificationService} from '../../notification/notification.service';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {EventTabHeaderMode, EventTabHeaderType} from '../../models/event/event.tab.header.model';
import {AdHookNames, HookService} from '../../services/hook.service';

@Component({
    selector: 'autogen-form-header',
    templateUrl: './autogen.form.header.component.html',
    styleUrls: ['./autogen.form.header.component.scss']
})
export class AutogenFormHeaderComponent extends AutogenComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = [];

    @Input() showNav: boolean;
    @Input() showAudit: boolean;
    @Input() showNew = true;
    @Input() showInPopup = false;

    @Output() navFirst: EventEmitter<any> = new EventEmitter();
    @Output() navPrevious: EventEmitter<any> = new EventEmitter();
    @Output() save: EventEmitter<any> = new EventEmitter();
    @Output() cancel: EventEmitter<any> = new EventEmitter();
    @Output() navNext: EventEmitter<any> = new EventEmitter();
    @Output() navLast: EventEmitter<any> = new EventEmitter();
    @Output() new: EventEmitter<any> = new EventEmitter();
    @Output() list: EventEmitter<any> = new EventEmitter();
    @Output() audit: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor(
        _fuseConfigService: FuseConfigService,
        _notificationService: NotificationService,
        _cacheService: AppCacheService,
        _autogenService: AutogenService,
        private _globalEventsService: GlobalEventsService,
        private _hookService: HookService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
        this.subscriptions.push(
            this._globalEventsService.tabHeaderRecordEvent$.subscribe(data => {
                if (data.mode === EventTabHeaderMode.Emit) {
                    switch (data.event) {
                        case EventTabHeaderType.FisrtRecord:
                            this.doNavFirst();
                            break;

                        case EventTabHeaderType.PreviousRecord:
                            this.doNavPrevious();
                            break;

                        case EventTabHeaderType.NextRecord:
                            this.doNavNext();
                            break;

                        case EventTabHeaderType.LastRecord:
                            this.doNavLast();
                            break;

                        case EventTabHeaderType.SaveRecord:
                            this.doSave();
                            break;

                        case EventTabHeaderType.CancelRecord:
                            this.doCancel();
                            break;

                        case EventTabHeaderType.NewRecord:
                            this.doNew();
                            break;

                        case EventTabHeaderType.ListRecords:
                            this.doList();
                            break;
                    }
                }
            })
        );
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscrition => subscrition.unsubscribe());
    }

    ngOnInit(): void {
    }

    relatedTabLoaded(): void {
        if (this.tab.tab.uipattern === 'READONLY' || this.tab.tab.uipattern === 'EDIT' || this.tab.tab.uipattern === 'EDIT_DELETE') {
            this.showNew = false;
        }
    }

    doNavFirst(): void {
        this.navFirst.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.FisrtRecord, this.tab.tab.idTab);
    }

    doNavPrevious(): void {
        this.navPrevious.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.PreviousRecord, this.tab.tab.idTab);
    }

    doSave(): void {
        this.save.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.SaveRecord, this.tab.tab.idTab);
    }

    doCancel(): void {
        this.cancel.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.CancelRecord, this.tab.tab.idTab);
    }

    doNavNext(): void {
        this.navNext.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.NextRecord, this.tab.tab.idTab);
    }

    doNavLast(): void {
        this.navLast.emit();
        this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.LastRecord, this.tab.tab.idTab);
    }

    doNew(): void {
        this._hookService.execHook(
            AdHookNames.AD_CAN_NEW_ITEM, {
                idTab: this.tab.tab.idTab,
                origin: 'FORM'
            }
        ).then(response => {
            if (response.success) {
                this.new.emit();
                this._globalEventsService.notifyTabHeaderRecordEvent(EventTabHeaderType.NewRecord, this.tab.tab.idTab);
            }
        }).catch(() => {
            // Nothing to do
        });
    }

    doList(): void {
        this.list.emit();
    }

    doAudit(): void {
        this.audit.emit();
    }

    doClose(): void {
        this.close.emit();
    }
}
