import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {MatTabChangeEvent} from '@angular/material';
import {MatDialog} from '@angular/material/dialog';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {Subscription} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AutogenEditElementsModel} from '../models/autogen.edit.elements.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdFormEditorModel} from '../../models/ad.form.editor.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {UtilitiesService} from '../../services/utilities.service';
import {ConditionalService} from '../../services/conditional.service';
import {AdReferenceService} from '../../services/ad.reference.service';
import {AdHookNames, HookService} from '../../services/hook.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AutogenAuditComponent} from '../autogen.audit/autogen.audit.component';
import {AutogenBaseFormComponent} from '../autogen.base.form.component';
import {VINCOFW_CONFIG} from '../../vincofw.constants';
import {EventTabHeaderType} from '../../models/event/event.tab.header.model';

@Component({
    selector: 'autogen-form',
    templateUrl: './autogen.form.component.html',
    styleUrls: ['./autogen.form.component.scss']
})
export class AutogenFormComponent extends AutogenBaseFormComponent implements OnInit, OnDestroy {

    protected subscriptions: Subscription[] = [];
    contentFieldStyleHeight: string = null;
    contentFieldStyleOverflow: string = null;

    @Input() selectedItems: AutogenEditElementsModel;
    @Input() showInPopup = false;

    @Output() close: EventEmitter<any> = new EventEmitter();

    constructor(
        _fuseConfigService: FuseConfigService,
        _notificationService: NotificationService,
        _cacheService: AppCacheService,
        _autogenService: AutogenService,
        _conditionalService: ConditionalService,
        _utilitiesService: UtilitiesService,
        _referenceService: AdReferenceService,
        _hookService: HookService,
        _globalEventsService: GlobalEventsService,
        private _dialog: MatDialog,
        private _bottomSheet: MatBottomSheet
    ) {
        super(
            _fuseConfigService, _notificationService, _cacheService, _autogenService, _conditionalService,
            _utilitiesService, _referenceService, _hookService, _globalEventsService
        );
        this.subscriptions.push(
            this._globalEventsService.tabHeaderRecordEvent$.subscribe(data => {
                if (this.tab.idTab === data.idTab && data.event === EventTabHeaderType.ChangeCaption) {
                    this.formCaption = data.caption;
                }
            })
        );
        this.subscriptions.push(
            this._globalEventsService.tabHeaderRecordEvent$.subscribe(data => {
                if (this.tab.idTab === data.idTab && data.event === EventTabHeaderType.ReloadRecord) {
                    const pk = this.tab.tab.fields.find(fld => fld.relatedColumn.primaryKey);
                    if (pk) {
                        const id = this.currentItem[pk.columnName];
                        let indx = this.selectedItems.items.findIndex(item => item[pk.columnName] === id);
                        if (indx < 0) {
                            indx = this.selectedItems.items.length - 1;
                            this.selectedItems.items[indx] = this.currentItem;
                        }
                        this.setCurrentItem(indx);
                    }
                }
            })
        );
    }

    ngOnInit(): void {
        this.onInit();
        this.fieldIdentifier = this._utilitiesService.getIdentifierColumn(this.tab.tab.table.columns, this.tab.tab.fields);
        this.isNew = isNullOrUndefined(this.selectedItems.isNew) ? this.selectedItems.items === null : this.selectedItems.isNew;
        if (this.showInPopup) {
            this.contentFieldStyleHeight = (window.innerHeight * 0.7) + 'px';
            this.contentFieldStyleOverflow = 'auto';
        }
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
        this.subscriptions.forEach(subscrition => subscrition.unsubscribe());
    }

    /**
     * Get field to be edited
     */
    protected getFormFields(): AdFieldDtoModel[] {
        return this.tab.tab.fields;
    }

    /**
     * Called when the form is build and ready to shown
     */
    protected formReady(): void {
        // Set tabs dependency
        this.tab.visibleTabs.forEach(tab => {
            if (tab.displaylogic) {
                const fields = this._conditionalService.getListenersField(tab.displaylogic);
                fields.forEach(fld => {
                    const editor: AdFormEditorModel = this.editors.find(edt => edt.name === fld);
                    if (editor) {
                        this._utilitiesService.addFieldDependency(editor, [tab.idTab], 'tabListeners');
                    }
                });
            }
        });

        if (this.isNew) {
            this.doNew();
        } else {
            this.setCurrentItem(this.selectedItems.index);
        }
    }

    /**
     * Navigate to first element
     */
    doNavFirst(): void {
        this.setCurrentItem(0);
    }

    /**
     * Navigate to previous element
     */
    doNavPrevious(): void {
        this.stepCurrentItem(false);
    }

    /**
     * Save element modifications
     *
     * @param index Element index for navigation or null (save current) or -1 (Add new element)
     */
    doSave(index: number): void {
        if (this.tab.tab.defaultEditor) {
            this.getProcessedItemToSave().then(itemToSave => {
                if (this.isNew) {
                    if (this.tab.tab.table.idTable === VINCOFW_CONFIG.TABLES.AD_FILE || this.tab.tab.table.idTable === VINCOFW_CONFIG.TABLES.AD_IMAGE) {
                        itemToSave.idTable = this.tab.tab.idParentTable;
                        itemToSave.idRow = this.parentItem.id;
                    }
                    this._service.save(itemToSave).subscribe(id => {
                        this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
                        if (!isNullOrUndefined(index)) {
                            if (index === -1) {
                                this.autogenForm.reset();
                                this.doNew();
                            } else {
                                this.setCurrentItem(index);
                            }
                        } else {
                            this.currentItem = { ...itemToSave };
                            this.isNew = false;
                            this.currentItem.id = id;
                            const pk = this.tab.tab.fields.find(fld => fld.relatedColumn.primaryKey);
                            if (pk) {
                                this.currentItem[pk.relatedColumn.name] = id;
                            }
                            if (this.tab.childTabs.length === 0) {
                                this.doList();
                            } else {
                                if (this.tab.tab.reloadAfterSave) {
                                    index = isNullOrUndefined(index) ? this.selectedItems.items.length - 1 : index;
                                    this.selectedItems.items[index] = this.currentItem;
                                    this.setCurrentItem(index);
                                }
                                this.adjustReadOnlyAndDisplayLogic();
                            }
                        }
                    },
                    error => {
                        this.showServerError(error);
                    });
                } else {
                    const pk = this.tab.tab.fields.find(fld => fld.relatedColumn.primaryKey);
                    if (pk) {
                        itemToSave.id = this.currentItem[pk.relatedColumn.name];
                    }
                    this._service.update(itemToSave).subscribe(response => {
                        this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
                        if (response) {
                            this._notificationService.showWarning(response);
                        }
                        if (!isNullOrUndefined(index)) {
                            if (index === -1) {
                                this.autogenForm.reset();
                                this.doNew();
                            } else {
                                this.setCurrentItem(index);
                            }
                        } else {
                            this.currentItem = { ...itemToSave };
                            this.doList();
                        }
                    },
                    error => {
                        this.showServerError(error);
                    });
                }
            }).catch(() => {
                console.log('Form processing error');
            });
        }
    }

    /**
     * Execute custom action
     *
     * @param btn Button action
     */
    doCustomAction(btn: AdFieldDtoModel): void {
        switch (btn.relatedColumn.reference.refButton.btype) {
            case 'PROCESS':
            case 'PROCESS_QUICK':
                this._globalEventsService.notifyProcessOpen(btn.relatedColumn.reference.refButton.idProcess, btn.relatedColumn.reference.refButton.btype, this. getItemToSave());
                break;

            case 'JAVASCRIPT':
                this._globalEventsService.notifyExecuteCode(btn.relatedColumn.reference.refButton.jsCode, 'FORM', [this. getItemToSave()]);
                break;

            default:
                this._notificationService.showWarning('Custom button actions (' + btn.caption + ') invalid type: ' + btn.relatedColumn.reference.refButton.btype);
        }
    }

    /**
     * Navigate to next element
     */
    doNavNext(): void {
        this.stepCurrentItem(true);
    }

    /**
     * Navigate to last element
     */
    doNavLast(): void {
        this.setCurrentItem(this.selectedItems.items.length - 1);
    }

    /**
     * Create a new element
     */
    doNew(): void {
        if (!this.autogenForm.pristine) {
            this.doSave(-1);
        } else {
            this.isNew = true;
            this.usedWhenNew = !isNullOrUndefined(this.tab.childTabs.find(tab => tab.usedWhenNew));
            this.formCaption = this._cacheService.getTranslation('AD_msgCaptionBarNew');
            if (this.showInPopup) {
                this.formCaption += ' - ' + this.tab.name;
            }
            if (this.selectedItems === null || this.selectedItems.items === null) {
                this.selectedItems = {
                    items: [],
                    index: 0,
                    isNew: true
                };
            }
            const item = this.getDefaultItem(this.showInPopup ? this.selectedItems.items[0] : null);
            if (this.showInPopup) {
                this.selectedItems.items[0] = item;
            } else {
                this.selectedItems.items.push(item);
            }
            this._hookService.execHook(
                AdHookNames.AD_AFTER_NEW_ITEM, {
                    tableName: this.tab.tab.table.name,
                    tab: this.tab.tab,
                    item: item
                }
            ).then(response => {
                if (response.success) {
                    this.setCurrentItem(this.selectedItems.items.length - 1, true);
                } else {
                    this._notificationService.showError(this._cacheService.getTranslation('AD_ErrorHookExec'));
                }
            });
        }
    }

    /**
     * Show element list
     */
    doList(): void {
        this.listItems.emit(this.currentItem);
    }

    /**
     * Show element audit
     */
    doAudit(): void {
        this._bottomSheet.open(
            AutogenAuditComponent, {
                data: {
                    item: this.currentItem,
                    service: this._service,
                    table: this.tab.tab.table,
                    fields: this.tab.tab.fields
                }
            });
    }

    /**
     * Close dialog
     */
    doClose(): void {
        this.close.emit();
    }

    /**
     * Process "SelectedTabChange" event
     *
     * @param event Event
     */
    doSelectedTabChange(event: MatTabChangeEvent): void {
        let selectedTab = null;
        if (event.index > 0 || !this.tab.tab.defaultEditor) {
            let displayedIndx = this.tab.tab.defaultEditor ? 0 : -1;
            for (let i = 0; i < this.tab.visibleTabs.length; i++) {
                if (this.tab.visibleTabs[i].displayed) {
                    displayedIndx++;
                }
                if (event.index === displayedIndx) {
                    selectedTab = this.tab.visibleTabs[i];
                    break;
                }
            }
        } else {
            selectedTab = this.tab;
        }
        if (selectedTab) {
            this._globalEventsService.notifySelectedTabChange(this.tab.idTab, selectedTab.idTab);
        }
    }

    /**
     * Move to next/previus element
     *
     * @param forward Move direction
     */
    private stepCurrentItem(forward: boolean): void {
        if (forward && this.selectedItems.items.length === this.selectedItems.index + 1) {
            this._notificationService.showMessage(this._cacheService.getTranslation('AD_msgCaptionBarAlreadyLast'));
            return;
        }
        if (!forward && this.selectedItems.index === 0) {
            this._notificationService.showMessage(this._cacheService.getTranslation('AD_msgCaptionBarAlreadyFirst'));
            return;
        }
        const index = this.selectedItems.index + (forward ? 1 : -1);
        if (!this.autogenForm.pristine) {
            this.doSave(index);
        } else {
            this.setCurrentItem(index);
        }
    }

    /**
     * Set current item
     *
     * @param index Element index
     * @param isNew Is new item
     */
    private setCurrentItem(index: number, isNew: boolean = false): void {
        this.currentItemCompleted = false;
        this.selectedItems.index = index;
        this.currentItem = this.selectedItems.items[this.selectedItems.index];
        if (!isNew) {
            this._service.get(this.currentItem[this.pkField.columnName]).subscribe(data => {
                this.currentItem = data;
                this.formCaption = this.currentItem[this.fieldIdentifier];
                this.loadCurrentItem();
            });
        } else {
            this.loadCurrentItem();
        }
    }

}
