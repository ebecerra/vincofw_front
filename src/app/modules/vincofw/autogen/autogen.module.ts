import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AutogenDatatableComponent} from './autogen.datatable/autogen.datatable.component';
import {AutogenDatatableHeaderComponent} from './autogen.datatable.header/autogen.datatable.header.component';
import {AutogenCustomDatatableComponent} from './datatable/autogen.custom.datatable/autogen.custom.datatable.component';
import {AutogenNgxDatatableComponent} from './datatable/autogen.ngx.datatable/autogen.ngx.datatable.component';
import {AutogenMatDatatableComponent} from './datatable/autogen.mat.datatable/autogen.mat.datatable.component';
import {AutogenFilterComponent} from './autogen.filter/autogen.filter.component';
import {AutogenCrudComponent} from './autogen.crud/autogen.crud.component';
import {FuseSidebarModule} from '../../../../@fuse/components';
import {NavbarModule} from '../../../layout/components/navbar/navbar.module';
import {ExtensionsModule} from '../../../extensions/extensions.module';
import {AutogenFilterYesnoComponent} from './filters/autogen.yesno/autogen.filter.yesno.component';
import {AutogenFilterSearchComponent} from './filters/autogen.filter.search/autogen.filter.search.component';
import {AutogenFormComponent} from './autogen.form/autogen.form.component';
import {AutogenFormHeaderComponent} from './autogen.form.header/autogen.form.header.component';
import {AutogenComponent} from './autogen.component';
import {AutogenTableBaseComponent} from './autogen.table.base.component';
import {VincofwModule} from '../vincofw.module';
import {AutogenFieldsComponent} from './autogen.fields/autogen.fields.component';
import {AutogenProcessComponent} from './autogen.process/autogen.process.component';
import {ProcessDialogComponent} from './process.dialog/process.dialog.component';
import {VincoCustomTabComponent} from './custom.tab/vinco.custom.tab.component';
import {CustomModule} from '../../../extensions/custom/custom.module';
import {AutogenAuditComponent} from './autogen.audit/autogen.audit.component';
import {MaterialModule} from '../../../material.module';
import {AutogenEditorsModule} from './editors/autogen.editors.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {TranslationDialogComponent} from './translation.dialog/translation.dialog.component';
import {FormDesignerComponent} from './form.designer/form.designer.component';
import {FormFieldsComponent} from './form.designer/form.fields/form.fields.component';
import {AutogenSortComponent} from './autogen.sort/autogen.sort.component';
import {AutogenCardComponent} from './autogen.card/autogen.card.component';
import {GridFieldsSortComponent} from './grid.fields/grid.field.sort/grid.fields.sort.component';
import {GridFieldsComponent} from './grid.fields/grid.fields.component';
import {ProcessPlanningComponent} from './process.planning/process.planning.component';
import {QueryExecComponent} from './query.exec/query.exec.component';
import {MenuRoleComponent} from './menu.role/menu.role.component';
import {FormEditorDialogComponent} from './form.editor.dialog/form.editor.dialog.component';
import {AutogenChartComponent} from './autogen.chart/autogen.chart.component';
import {AutogenMultiselectComponent} from './autogen.multiselect/autogen.multiselect.component';
import {AutogenBaseFormComponent} from './autogen.base.form.component';
import {AutogenFormEditorComponent} from './autogen.form.editor/autogen.form.editor.component';
import {AutogenChartPieComponent} from './charts/autogen.chart.pie/autogen.chart.pie.component';
import {AutogenChartLineareaComponent} from './charts/autogen.chart.linearea/autogen.chart.linearea.component';
import {AutogenChartBarComponent} from './charts/autogen.chart.bar/autogen.chart.bar.component';

@NgModule({
    declarations: [
        AutogenAuditComponent,
        AutogenBaseFormComponent,
        AutogenComponent,
        AutogenCardComponent,
        AutogenChartComponent,
        AutogenChartPieComponent,
        AutogenChartBarComponent,
        AutogenChartLineareaComponent,
        AutogenCrudComponent,
        AutogenDatatableComponent,
        AutogenFilterComponent,
        AutogenDatatableHeaderComponent,
        AutogenFieldsComponent,
        AutogenFilterYesnoComponent,
        AutogenFilterSearchComponent,
        AutogenFormComponent,
        AutogenFormEditorComponent,
        AutogenFormHeaderComponent,
        AutogenCustomDatatableComponent,
        AutogenMatDatatableComponent,
        AutogenMultiselectComponent,
        AutogenNgxDatatableComponent,
        AutogenProcessComponent,
        AutogenSortComponent,
        AutogenTableBaseComponent,
        FormDesignerComponent,
        FormEditorDialogComponent,
        FormFieldsComponent,
        GridFieldsSortComponent,
        GridFieldsComponent,
        MenuRoleComponent,
        ProcessDialogComponent,
        ProcessPlanningComponent,
        QueryExecComponent,
        TranslationDialogComponent,
        VincoCustomTabComponent
    ],
    entryComponents: [
        AutogenAuditComponent,
        FormEditorDialogComponent,
        GridFieldsComponent,
        ProcessDialogComponent,
        TranslationDialogComponent
    ],
    imports: [
        NavbarModule,
        FuseSidebarModule,
        NgxDatatableModule,
        NgxChartsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        PipesModule,
        VincofwModule,
        MaterialModule,
        AutogenEditorsModule,
        ExtensionsModule,
        CustomModule,
        RouterModule,
        CKEditorModule,
        DragDropModule
    ],
    providers: [
    ],
    exports: [
        AutogenAuditComponent,
        AutogenComponent,
        AutogenBaseFormComponent,
        AutogenCardComponent,
        AutogenChartComponent,
        AutogenChartPieComponent,
        AutogenChartBarComponent,
        AutogenChartLineareaComponent,
        AutogenCrudComponent,
        AutogenDatatableComponent,
        AutogenFilterComponent,
        AutogenDatatableHeaderComponent,
        AutogenFieldsComponent,
        AutogenFilterYesnoComponent,
        AutogenFilterSearchComponent,
        AutogenFormComponent,
        AutogenFormEditorComponent,
        AutogenFormHeaderComponent,
        AutogenCustomDatatableComponent,
        AutogenMatDatatableComponent,
        AutogenMultiselectComponent,
        AutogenNgxDatatableComponent,
        AutogenProcessComponent,
        AutogenSortComponent,
        AutogenTableBaseComponent,
        ProcessDialogComponent,
        ProcessPlanningComponent,
        QueryExecComponent,
        TranslationDialogComponent,
        VincoCustomTabComponent
    ]
})

export class AutogenModule {
}
