import {Component, OnDestroy, OnInit} from '@angular/core';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdMultiSelectValueModel} from '../../models/ad.multi.select.value.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {ConditionalService} from '../../services/conditional.service';
import {UtilitiesService} from '../../services/utilities.service';
import {AdReferenceService} from '../../services/ad.reference.service';
import {HookService} from '../../services/hook.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AdFieldService} from '../../services/ad.field.service';
import {AutogenBaseFormComponent} from '../autogen.base.form.component';

@Component({
    selector: 'autogen-multiselect',
    templateUrl: './autogen.multiselect.component.html',
    styleUrls: ['./autogen.multiselect.component.scss']
})
export class AutogenMultiselectComponent extends AutogenBaseFormComponent implements OnInit, OnDestroy {

    loading: boolean;
    multiSelectItems: AdMultiSelectValueModel[];
    caption: string;

    constructor(
        _fuseConfigService: FuseConfigService,
        _notificationService: NotificationService,
        _cacheService: AppCacheService,
        _autogenService: AutogenService,
        _conditionalService: ConditionalService,
        _utilitiesService: UtilitiesService,
        _referenceService: AdReferenceService,
        _hookService: HookService,
        _globalEventsService: GlobalEventsService,
        private _fieldService: AdFieldService
    ) {
        super(
            _fuseConfigService, _notificationService, _cacheService, _autogenService, _conditionalService,
            _utilitiesService, _referenceService, _hookService, _globalEventsService
        );
        this.loading = true;
    }

    ngOnInit(): void {
        this.onInit();
    }

    /**
     * Get field to be edited
     */
    protected getFormFields(): AdFieldDtoModel[] {
        return this.tab.tab.fields.filter(fld => fld.displayed && !fld.multiSelect);
    }

    /**
     * Called when the form is build and ready to shown
     */
    protected formReady(): void {
        this.loading = false;
        this.currentItemCompleted = false;
        this.currentItem = this.getDefaultItem(null);
        this.loadCurrentItem();
        const field = this.tab.tab.fields.find(fld => fld.multiSelect);
        if (field) {
            this.caption = field.caption;
            const parentField = this.tab.tab.fields.find(fld => fld.relatedColumn.linkParent);
            this._fieldService.multiselect(field.idField, parentField.idField, this.parentItem[parentField.columnName]).subscribe(response => {
                this.multiSelectItems = response;
            });
        } else {
            console.log('Not found multiselect field');
            this.multiSelectItems = [];
            this.caption = '-';
        }
    }

    doSave(): void {
        this.getProcessedItemToSave().then(itemToSave => {
            const ids = [];
            this.multiSelectItems.forEach(item => {
                if (item.selected) {
                    ids.push(item.id);
                }
            });
            if (ids.length > 0) {
                const service = this._autogenService.bindedService(this.tab.tab.table);
                service.saveMultiple(itemToSave, ids).subscribe(resp => {
                    if (resp.success) {
                        this.listItems.emit(null);
                    } else {
                        this._notificationService.showError(this._cacheService.getTranslation(resp.message));
                    }
                });
            } else {
                this._notificationService.showError(this._cacheService.getTranslation('AD_MultiSelectCheckValues'));
            }
        }).catch(() => {
            console.log('Multi editor form processing error');
        });
    }

    toggleRow(row: AdMultiSelectValueModel): void {
        row.selected = !row.selected;
    }

    doChangeAll(event): void {
        this.multiSelectItems.forEach(item => item.selected = event.checked);
    }
}
