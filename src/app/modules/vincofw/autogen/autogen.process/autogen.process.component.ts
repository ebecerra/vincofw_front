import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {isNullOrUndefined} from 'util';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AdReferenceKeyValueModel} from '../../models/dto/ad.reference.key.value.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdColumnDtoModel} from '../../models/dto/ad.column.dto.model';
import {AdProcessDtoModel} from '../../models/dto/ad.process.dto.model';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {AdProcessParamDtoModel} from '../../models/dto/ad.process.param.dto.model';
import {AdProcessLogModel} from '../../models/ad.process.log.model';
import {AdFormEditorModel} from '../../models/ad.form.editor.model';
import {AdBaseModel} from '../../models/ad.base.model';
import {ControllerResultModel} from '../../models/controller.result.model';
import {AppCacheService} from '../../services/cache.service';
import {AdProcessService} from '../../services/ad.process.service';
import {UtilitiesService} from '../../services/utilities.service';
import {ConditionalService} from '../../services/conditional.service';
import {SocketClientService} from '../../services/socket.client.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AdHookNames, HookService} from '../../services/hook.service';
import {AdReferenceService} from '../../services/ad.reference.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';
import {HelpShowComponent} from '../../components/help.show/help.show.component';

import * as moment_ from 'moment';

const moment = moment_;

@Component({
    selector: 'autogen-process',
    templateUrl: './autogen.process.component.html',
    styleUrls: ['./autogen.process.component.scss']
})
export class AutogenProcessComponent implements OnInit, OnDestroy {

    private generatedFields: AdFieldDtoModel[];

    fuseConfig: any;
    _unsubscribeAll: Subject<any>;
    loadedReferences: AdReferenceDtoModel[];
    visibleParams: AdProcessParamDtoModel[];
    rows: any[];
    autogenForm: FormGroup;
    serverLog: AdProcessLogModel[];
    showLog = true;
    htmlPreview: string;
    editors: AdFormEditorModel[];
    currentItem: any;
    subscriptions: Subscription[] = [];
    autoScroll = true;
    canExecute = true;
    canCancel = false;
    idProcessExec: string = null;
    quickLog: AdProcessLogModel = null;
    quickExecuting = false;

    @Input() process: AdProcessDtoModel;
    @Input() item: AdBaseModel = null;
    @Input() delay = 300;
    @Input() showTitle = true;
    @Input() execMode = 'PROCESS';

    @ViewChild('divViewer', { read: ElementRef }) divViewer: ElementRef;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _utilitiesService: UtilitiesService,
        private _processService: AdProcessService,
        private _conditionalService: ConditionalService,
        private _referenceService: AdReferenceService,
        private _hookService: HookService,
        private _socketClientService: SocketClientService,
        private _globalEventsService: GlobalEventsService,
        private _bottomSheet: MatBottomSheet,
    ) {
        this._unsubscribeAll = new Subject();
        this.serverLog = [];
        this.showLog = true;
        this.htmlPreview = '';
        this.subscriptions.push(
            this._globalEventsService.processLog$.subscribe(data => {
                if (this.process.idProcess === data.idProcess && (this.idProcessExec === data.idProcessExec || data.idProcessExec === null)) {
                    this.processLog(data);
                }
            })
        );
    }

    /**
     * Get log item
     *
     * @param logType Log type
     * @param log Log info
     */
    private static getLog(logType: string, log: string): AdProcessLogModel {
        const result = new AdProcessLogModel();
        result.logType = logType;
        result.info = log;
        result.date = new Date();
        return result;
    }

    ngOnInit(): void {
        console.log('AutogenProcessComponent: ngOnInit(' + this.process.name + ')');
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
        this.visibleParams = this.process.params.filter(param => param.displayed);
        this.generatedFields = this.process.params.map(current => {
            const transformed = new AdFieldDtoModel();
            transformed.columnName = current.name;
            transformed.relatedColumn = new AdColumnDtoModel();
            transformed.relatedColumn.reference = current.reference;
            return transformed;
        });
        this.rows = [];
        this.editors = [];
        const distinct: string[] = [];
        this.process.params.forEach(current => {
            if (isNullOrUndefined(distinct.find(r => r === current.reference.idReference))) {
                distinct.push(current.reference.idReference);
            }
        });
        this._cacheService.getReferenceInfo(distinct).pipe(takeUntil(this._unsubscribeAll)).subscribe(references => {
            this.loadedReferences = references;
            this.process.params.forEach(param => {
                if (param.reference.rtype === 'BUTTON' || param.reference.rtype === 'LIST' || param.reference.rtype === 'TABLE' || param.reference.rtype === 'TABLEDIR') {
                    const ref = this.loadedReferences.find(reference => reference.idReference === param.reference.idReference);
                    if (ref) {
                        param.reference.refButton = ref.refButton;
                        param.reference.refList = ref.refList;
                        param.reference.refTable = ref.refTable;
                    }
                }
            });
            this.initForm();
        });
    }

    ngOnDestroy(): void {
        console.log('AutogenProcessComponent: ngOnDestroy(' + this.process.name + ')');
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    doExecute(action: string): void {
        if (this.process.showConfirm) {
            const dialogRef = this._notificationService.confirmDialog(
                this._cacheService.getTranslation('AD_GlobalConfirm'),
                this.process.confirmMsg,
                'play_circle_outline'
            );
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    this.executeProcess(action);
                }
            });
        } else {
            this.executeProcess(action);
        }
    }

    doCancel(): void {
        if (this.idProcessExec) {
            const dialogRef = this._notificationService.confirmDialog(
                this._cacheService.getTranslation('AD_GlobalConfirm'),
                this._cacheService.getTranslation('AD_ProcessCancelExec'),
                'stop'
            );
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    this._processService.cancelExec(this.process.idProcess, this.idProcessExec).subscribe(response => {
                        const status = response.properties.status;
                        if (response.success) {
                            this.serverLog.push(AutogenProcessComponent.getLog(
                                status === 'CancelFinished' ? 'SUCCESS' : 'WARNING',
                                this._cacheService.getTranslation('AD_Process' + status))
                            );
                        } else {
                            const err = this._cacheService.getTranslation('AD_Process' + status);
                            this.serverLog.push(AutogenProcessComponent.getLog('ERROR', err));
                            this._notificationService.showError(err);
                        }
                    });
                }
            });
        }
    }

    doDelete(): void {
        if (this.showLog) {
            this.serverLog = [];
        } else {
            this.htmlPreview = '';
            this.showLog = true;
        }
    }

    doTop(): void {
        this.divViewer.nativeElement.scrollTo( 0, 0 );
        this.autoScroll = false;
    }

    doBottom(): void {
        this.divViewer.nativeElement.scrollTo( 0, this.divViewer.nativeElement.scrollHeight);
        this.autoScroll = true;
    }

    doToggleShow(): void {
        this.showLog = !this.showLog;
    }

    doComplete(event: ControllerResultModel): void {
        if (event.success) {
            const value = {};
            value[event.properties.field] = event.properties.field + '$' + event.properties.id + '$' + event.properties.name;
            this.autogenForm.patchValue(value);
        } else {
            // TODO: Complete with errors
        }
    }

    doAction(event, param): void {
        if (event === 'help') {
            this._bottomSheet.open(HelpShowComponent, { data: { message: param.description } });
        } else {
            this._notificationService.showWarning('Acción: ' + event + ', Parámetro: ' + param.caption);
        }
    }

    /**
     * Initialize form
     */
    private initForm(): void {
        const cssCol = 12 / this.process.columnsInput;
        let count = 0;
        let row: AdProcessParamDtoModel[] = [];
        const formElements = {};
        this.visibleParams.forEach(param => {
            if (this.process.columnsInput < count + param.span && row.length > 0) {
                this.rows.push(row);
                row = [];
                count = 0;
            }
            param.cssColspan = 'col-sm-' + (cssCol * param.span);
            param.runtimeDisplayed = true;
            row.push(param);
            count += param.span;
            formElements[param.name] = this._utilitiesService.buildFormControl(
                param.reference.rtype, param.mandatory,
                param.valuemin ? Number(param.valuemin) : null,
                param.valuemax ? Number(param.valuemax) : null, null, null);
            // Set editors
            const editor = new AdFormEditorModel(param.name);
            if (param.displaylogic) {
                this._utilitiesService.addFieldDependency(
                    editor, this._conditionalService.getListenersField(param.displaylogic), 'displayListeners'
                );
            }
            if (VINCOFW_CONFIG.sqlDependReferences.indexOf(param.reference.rtype) >= 0) {
                this._utilitiesService.addFieldDependency(
                    editor, this._conditionalService.getListenersField(param.reference.refTable.sqlwhere), 'sqlListeners'
                );
            }
            this.editors.push(editor);
        });
        this.rows.push(row);
        this.autogenForm = new FormGroup(formElements);
        // Set default values
        this.clearParameters();
        // Listening to the changes in the form
        this.autogenForm.valueChanges
            .pipe(
                debounceTime(this.delay),
                distinctUntilChanged()
            ).subscribe(data => {
            this.notifyChanges(data);
        });
        if (this.execMode === 'PROCESS_QUICK') {
            this.quickExecuting = true;
            this.autoScroll = false;
            this.executeProcess(this.process.format);
        }
    }

    /**
     * Process log events
     *
     * @param log Process log
     */
    private processLog(log: AdProcessLogModel): void {
        if (log.eventType.indexOf('FINISH_') === 0) {
            this.canExecute = true;
            this.canCancel = false;
            this.quickExecuting = false;
            const execSuccess = log.eventType === 'FINISH_SUCCESS';
            if (execSuccess) {
                this._notificationService.showSuccess(this._cacheService.getTranslation('AD_ProcessFinished'));
            } else {
                this._notificationService.showError(this._cacheService.getTranslation('AD_ProcessFinishedError'));
            }
            this._socketClientService.disconnect(this.process.idProcess);
            this._globalEventsService.notifyProcessExecuted(this.process.idProcess, execSuccess);
        } else if (log.eventType === 'START_EXEC') {
            this.canCancel = true;
        } else {
            this.serverLog.push(log);
            this.quickLog = log;
        }
        if (this.autoScroll) {
            this._utilitiesService.delay(100).then( () => {
                this.doBottom();
            });
        }
    }

    /**
     * Execute the process
     *
     * @param action Action request
     */
    private executeProcess(action: string): void {
        let formValid = true;
        if (this.autogenForm) {
            this.autogenForm.updateValueAndValidity();
            Object.keys(this.autogenForm.controls).forEach(key => {
                this.autogenForm.controls[key].markAsTouched();
            });
            formValid = this.autogenForm.valid;
        }
        if (formValid) {
            this.serverLog = [];
            this.showLog = true;
            this.autoScroll = !this.quickExecuting;
            this.serverLog.push(AutogenProcessComponent.getLog('SUCCESS', this._cacheService.getTranslation('AD_ProcessStarting')));
            Object.keys(this.currentItem).forEach(key => {
                if (this.autogenForm.get(key) !== null) {
                    this.currentItem[key] = this.autogenForm.value[key];
                }
            });
            this.canExecute = false;
            this.canCancel = false;
            this.idProcessExec = this._utilitiesService.generateUID();
            this._socketClientService.connect(this.process.idProcess).then(() => {
                console.log('AutogenProcessComponent: WebSocket Connected');
                let params = this._utilitiesService.getFilterParams(this.currentItem, this.process.params, true);
                if (action !== 'execute') {
                    params += ',outputType=' + action;
                    if (action === 'html') {
                        this._processService.execHtml(this.process.idProcess, this.idProcessExec, params).subscribe(data => {
                            this.showLog = false;
                            this.htmlPreview = data;
                        },
                        error => {
                            this.canExecute = true;
                            this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationServerError'));
                        });
                    } else {
                        this._processService.exec(this.process.idProcess, this.idProcessExec, params).subscribe(data => {
                            this._utilitiesService.downloadFile(data.body, data.fileName);
                        },
                        error => {
                            this.canExecute = true;
                            this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationServerError'));
                        });
                    }
                } else {
                    this._processService.exec(this.process.idProcess, this.idProcessExec, params).subscribe(data => {
                        if (data.body.type === 'application/json') {
                            this._utilitiesService.downloadJson(data).then(response => {
                                if (!response.success) {
                                    this._notificationService.showError(this._cacheService.getTranslation(response.message));
                                }
                            });
                        } else {
                            const fileName = this.process.name + '_' + moment().format('YYYYMMDD_HHMISS');
                            this._utilitiesService.downloadFile(data.body, fileName);
                        }
                        this.clearParameters();
                    },
                    error => {
                        this.canExecute = true;
                        this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationServerError'));
                    });
                }
            })
            .catch(error => {
                this.canExecute = true;
                this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationServerError'));
            });
        }
    }

    /**
     * Clear parameters form (load default values)
     */
    private clearParameters(): void {
        // Set default values
        this.currentItem = {};
        const globalParameters = {};
        this._hookService.execHook(AdHookNames.AD_GET_GLOBAL_PARAMETERS, { item: globalParameters }).then(response => {
            if (response.success) {
                this.process.params.forEach(param => {
                    let value = null;
                    if (param.valuedefault) {
                        const defValue = this._conditionalService.replaceJSConditional(param.valuedefault, null, null, { ...this.item, ...globalParameters });
                        // tslint:disable-next-line:no-eval
                        value = param.reference.rtype === 'YESNO' ? eval(defValue) : defValue;
                    }
                    param.value = value;
                    this.currentItem[param.name] = value;
                });
                // Create reactive form and initialize default values
                Object.keys(this.autogenForm.controls).forEach(key => {
                    const param = this.process.params.find(p => p.name === key);
                    param.runtimeDisplaylogic = param.displaylogic;
                    this._utilitiesService.setFormValue(this.autogenForm, key, param.reference, false, this.currentItem, null);
                });
                this.autogenForm.markAsUntouched();
                this.autogenForm.markAsPristine();
            }
        });
    }

    /**
     * Form changes listener
     *
     * @param data Form data
     */
    private notifyChanges(data: any): void {
        this.editors.forEach(edt => {
            if (edt.isModify(data)) {
                // Field Display logic
                const dependDisplayEditors = this.editors.filter(depEdt => depEdt.displayListeners.find(lst => lst === edt.name));
                dependDisplayEditors.forEach(depEdt => {
                    const param = this.process.params.find(prm => prm.name === depEdt.name);
                    param.runtimeDisplaylogic = this._conditionalService.replaceJSConditional(param.runtimeDisplaylogic, null, null, data);
                    if (this._conditionalService.isCompleteConditional(param.runtimeDisplaylogic)) {
                        // tslint:disable-next-line:no-eval
                        param.runtimeDisplayed = eval(param.runtimeDisplaylogic);
                        param.runtimeDisplaylogic = param.displaylogic;
                    } else {
                        param.runtimeDisplayed = false;
                    }
                });
                // SQL where logic
                const dependSQLEditors = this.editors.filter(depEdt => depEdt.sqlListeners.find(lst => lst === edt.name));
                dependSQLEditors.forEach(depEdt => {
                    const param = this.process.params.find(prm => prm.name === depEdt.name);
                    if (param.runtimeDisplayed) {
                        if (param.reference.rtype === 'TABLEDIR') {
                            this._referenceService.loadTabledir(this.generatedFields, param.reference, this.getFormModel())
                                .pipe(takeUntil(this._unsubscribeAll))
                                .subscribe( values => {
                                    this._hookService.execHook(
                                        AdHookNames.AD_LIST_CHANGE_VALUES, {
                                            fieldName: param.name,
                                            values: values
                                        }
                                    ).then();
                                });
                        } else if (param.reference.rtype === 'TABLE') {
                            this.currentItem = this.getFormModel();
                            param.reference.refTable.runtimeSqlwhere = null;
                        }
                    }
                });
            }
        });
    }

    /**
     * Get form values
     */
    private getFormModel(): AdBaseModel {
        const itemToSave = new AdBaseModel();
        this.generatedFields.forEach(field => {
            const key = field.columnName;
            if (this.autogenForm.get(key) !== null) {
                if (this.autogenForm.value[key] instanceof AdReferenceKeyValueModel) {
                    itemToSave[key] = this.autogenForm.value[key].key;
                } else {
                    itemToSave[key] = this._utilitiesService.getFormValue(this.autogenForm.value[key], field);
                }
            } else {
                itemToSave[key] = this._utilitiesService.getFormValue(this.currentItem[key], field);
            }
        });
        return itemToSave;
    }
}
