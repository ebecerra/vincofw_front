import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {PageEvent} from '@angular/material/paginator';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AutogenComponent} from '../autogen.component';
import {PagedContentModel} from '../../models/ad.base.model';
import {Page} from '../../models/page';
import {AutogenSelectValueModel} from '../models/autogen.select.value.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';

@Component({
    selector: 'autogen-sort',
    templateUrl: './autogen.sort.component.html',
    styleUrls: ['./autogen.sort.component.scss']
})
export class AutogenSortComponent extends AutogenComponent implements OnInit, OnDestroy {

    private _content: PagedContentModel<any>;
    private _page = new Page();

    cssCol: string;
    autogenForm: FormGroup;
    sortItems:  AutogenSelectValueModel[] = [
        { value: '1', caption: '1' },
        { value: '5', caption: '5' },
        { value: '10', caption: '10' },
        { value: '50', caption: '50' },
        { value: '100', caption: '100' },
        { value: '500', caption: '500' },
        { value: '1000', caption: '1000' }
    ];
    fields: AdFieldDtoModel[] = null;

    @Input()
    set content(content: PagedContentModel<any>) {
        this._content = content;
        // Assign the data to the data source for the table to render
        this.page.pageNumber = this._content.number;
        this.page.totalElements = this._content.totalElements;
    }

    get content(): PagedContentModel<any> {
        return this._content;
    }

    @Input()
    set page(page: Page) {
        this._page = page;
        this.pageChanged.emit(page);
    }

    get page(): Page {
        return this._page;
    }

    @Input() sortStart: number;
    @Input() sortIncrement: number;
    @Input() sortColumnName: string;

    @Output() pageChanged = new EventEmitter<Page>();
    @Output() listItems = new EventEmitter();

    constructor(
        protected _fuseConfigService: FuseConfigService,
        protected _notificationService: NotificationService,
        protected _cacheService: AppCacheService,
        protected _autogenService: AutogenService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
        this.page.pageNumber = 0;
        this.page.size = 10;
    }

    ngOnInit(): void {
        this.onInit();
        this.autogenForm = new FormGroup({
            sortStart: new FormControl('', Validators.required),
            sortIncrement: new FormControl('', Validators.required)
        });
        this.autogenForm.get('sortStart').setValue(this.sortStart);
        this.autogenForm.get('sortIncrement').setValue('' + this.sortIncrement);
    }

    protected relatedTabLoaded(): void {
        let cols = Math.trunc(12 / this.tab.tab.sortColumns);
        if (cols === 0) {
            cols = 1;
        }
        this.cssCol = 'col-sm-' + cols;
        this.fields = this.tab.tab.fields.filter(fld => fld.displayed && fld.relatedColumn.reference.rtype !== 'BUTTON');
    }

    drop(event: CdkDragDrop<string[]>): void {
        console.log(`previousIndex: ${event.previousIndex}, currentIndex: ${event.currentIndex}`);
        const previousIndex = event.previousIndex;
        let currentIndex = event.currentIndex;
        if (previousIndex === currentIndex) {
            if (currentIndex > 0) {
                currentIndex--;
            } else if (currentIndex + 1 < this._content.content.length) {
                currentIndex++;
            }
        }
        const moveItem = this._content.content.splice(previousIndex, 1);
        this._content.content.splice(currentIndex, 0, moveItem[0]);
    }

    changePageInfo(pageEvent: PageEvent): void {
        this.page.pageNumber = pageEvent.pageIndex;
        this.pageChanged.emit(this.page);
    }

    doSave(): void {
        this.autogenForm.updateValueAndValidity();
        if (this.autogenForm.valid) {
            const sortParam = {
                field: this.sortColumnName,
                start: this.autogenForm.value['sortStart'],
                step: this.autogenForm.value['sortIncrement'],
                ids: []
            };
            this._content.content.forEach(item => {
                sortParam.ids.push(item.id);
            });
            const service = this._autogenService.bindedService(this.tab.tab.table);
            service.sort(sortParam).subscribe(resp => {
                if (resp.success) {
                    this.listItems.emit();
                } else {
                    this._notificationService.showError(this._cacheService.getTranslation(resp.message));
                }
            });
        }
    }
}
