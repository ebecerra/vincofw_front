import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';
import {isNullOrUndefined} from 'util';
import {AdTabMainDtoModel} from '../models/dto/ad.tab.main.dto.model';
import {AdTableActionDtoModel} from '../models/dto/ad.table.action.dto.model';
import {Page} from '../models/page';
import {PagedContentModel, SortData} from '../models/ad.base.model';
import {AdColumnDtoModel} from '../models/dto/ad.column.dto.model';
import {AppCacheService} from '../services/cache.service';
import {AdMenuService} from '../services/ad.menu.service';
import {GlobalEventsService} from '../services/global.events.service';
import {UtilitiesService} from '../services/utilities.service';
import {ConditionalService} from '../services/conditional.service';
import {NotificationService} from '../notification/notification.service';

@Component({
    template: ``
})
export class AutogenTableBaseComponent {

    _page = new Page();
    _content: PagedContentModel<any>;
    actions: AdTableActionDtoModel[];
    visibleColumns: AdColumnDtoModel[];
    tab: AdTabMainDtoModel;

    @Input()
    set page(page: Page) {
        this._page = page;
        this.pageChanged.emit(page);
    }

    get page(): Page {
        return this._page;
    }

    @Input()
    set content(content: PagedContentModel<any>) {
        this._content = content;
        // Assign the data to the data source for the table to render
        this.page.pageNumber = this._content.number;
        this.page.totalElements = this._content.totalElements;
        if (this.tab) {
            this.setPageContent();
        }
    }

    get content(): PagedContentModel<any> {
        return this._content;
    }

    @Input()
    set relatedTab(tab: AdTabMainDtoModel) {
        if (isNullOrUndefined(this.tab) || this.tab.tab.table.idTable !== tab.tab.table.idTable) {
            this.tab = tab;
            this.actions = this.tab.tab.table.actions;
            this.refreshVisibleColumns();
            this.setPageContent();
            this.relatedTabLoaded();
        }
    }

    @Output() pageChanged = new EventEmitter<Page>();
    @Output() sortChanged = new EventEmitter<SortData[]>();
    @Output() selectedElementsChanged: EventEmitter<any[]> = new EventEmitter();
    @Output() editSelected: EventEmitter<any[]> = new EventEmitter();

    constructor(
        protected _router: Router,
        protected _menuService: AdMenuService,
        protected _notificationService: NotificationService,
        protected _globalEventsService: GlobalEventsService,
        protected _utilitiesService: UtilitiesService,
        protected _conditionalService: ConditionalService,
        protected _cacheService: AppCacheService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 20;
        this.visibleColumns = [];
        this._content = new PagedContentModel<any>();
    }

    /**
     * Execute custom actions
     *
     * @param action Table action
     * @param item Row item
     */
    doCustomAction(action: AdTableActionDtoModel, item: any): void {
        switch (action.atype) {
            case 'PROCESS':
            case 'PROCESS_QUICK':
                this._globalEventsService.notifyProcessOpen(action.idProcess, action.atype, item);
                break;

            case 'LINK':
                if (action.url === '@currentFile@') {
                    window.open(item.fileUrl, action.target);
                } else {
                    window.open(action.url, action.target);
                }
                break;

            case 'WINDOW':
                const menu = this._menuService.getMenuByWindow(action.idWindow);
                if (menu) {
                    this._router.navigate(['tab', menu.idMenu]);
                } else {
                    this._notificationService.showError(this._cacheService.getTranslation('AD_ErrMenuNotAvailable'));
                }
                break;

            default:
                this._notificationService.showWarning('Action type: ' + action.atype + ' not implemented');
        }
    }

    /**
     * Set page content information
     */
    protected setPageContent(): void {
        if (this.tab.tab.table.actions && this.tab.tab.table.actions.length > 0) {
            this._content.content.forEach(row => {
                row.$action$ = [];
                this.tab.tab.table.actions.forEach(action => {
                    let displayed = true;
                    if (action.displaylogic) {
                        const runtimeDisplaylogic = this._conditionalService.replaceJSConditional(action.displaylogic, null, null, row);
                        if (this._conditionalService.isCompleteConditional(runtimeDisplaylogic)) {
                            try {
                                // tslint:disable-next-line:no-eval
                                displayed = eval(runtimeDisplaylogic);
                            } catch (e) {
                                console.error('Invalid display logic: ' + action.displaylogic);
                                console.error('Eval expression: ' + runtimeDisplaylogic);
                                displayed = false;
                            }
                        }
                    }
                    row.$action$[action.idTableAction] = displayed;
                });
            });
        }
    }

    /**
     * Updates the visible columns
     */
    protected refreshVisibleColumns(): void {
        this.visibleColumns = this._utilitiesService.getGridColumns(this.tab.tab.table.columns, this.tab.tab.fields, this.tab.tab.table.actions);
    }

    /**
     * This function is called when related tab was loaded
     */
    protected relatedTabLoaded(): void {

    }
}
