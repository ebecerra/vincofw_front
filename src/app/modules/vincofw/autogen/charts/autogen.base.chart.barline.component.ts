import {Input} from '@angular/core';
import {AutogenBaseChartComponent} from './autogen.base.chart.component';

export abstract class AutogenBaseChartBarlineComponent extends AutogenBaseChartComponent {

    xAxisLabel = '';
    yAxisLabel = '';
    values: any[] = [];

    @Input() showXAxis = true;
    @Input() showYAxis = true;
    @Input() showXAxisLabel = true;
    @Input() showYAxisLabel = true;
    @Input() showGridLines = false;
    @Input() roundDomains = true;

    abstract useUniqueSerie(): boolean;
    abstract setValues(): void;

    processData(): void {
        this.values = [];
        this.xAxisLabel = this.data.titleX;
        this.yAxisLabel = this.data.titleY;
        if (this.data.series) {
            if (this.useUniqueSerie()) {
                this.data.series[0].values.forEach(val => {
                    this.values.push({
                        name: val.fullX,
                        value: val.value,
                        extra: {
                            code: val.id,
                            name: val.x
                        }
                    });
                });
            } else {
                this.setValues();
            }
        }
    }

}
