import {EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Subscription} from 'rxjs';
import {VINCOFW_CHARTS} from './autogen.chart.constants';
import {AdChartResultModel} from '../../models/ad.chart.result.model';
import {EventChartSelectModel, EventChartTypeModel} from '../../models/event/event.chart.model';
import {AppCacheService} from '../../services/cache.service';
import {GlobalEventsService} from '../../services/global.events.service';

export abstract class AutogenBaseChartComponent implements OnDestroy {

    protected _data: AdChartResultModel;
    protected subscriptions: Subscription[] = [];

    colorSet: any;
    legend: string;

    @Input() idChart: string;
    @Input() disableTooltip = false;
    @Input() gradient = true;
    @Input() colorScheme = 'vivid';
    @Input() showLegend = true;
    @Input() legendPosition = 'below';
    @Input() animations = true;

    @Input()
    set data(item: AdChartResultModel) {
        this._data = item;
        if (this._data && this.colorSet) {
            this.processData();
        }
    }

    get data(): AdChartResultModel {
        return this._data;
    }

    @Output() onSelect: EventEmitter<EventChartSelectModel> = new EventEmitter<EventChartSelectModel>();

    constructor(
        protected _cacheService: AppCacheService,
        protected _globalEventsService: GlobalEventsService
    ) {
        this.legend = this._cacheService.getTranslation('AD_ChartLegend');
        this.subscriptions.push(
            this._globalEventsService.chartChangeType$.subscribe(data => {
                if (data.idChart === this.idChart) {
                    this.changeChartType(data);
                }
            })
        );
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscrition => subscrition.unsubscribe());
    }

    abstract processData(): void;

    changeChartType(data: EventChartTypeModel): void {

    }

    getSelectedId(data: any): EventChartSelectModel {
        if (data.extra) {
            return {
                idChart: this._data.idChart,
                code: data.extra.code,
                name: this._data.titleX + ': ' + data.extra.name
            };
        }
        return null;
    }

    setColors(): void {
        this.colorSet = VINCOFW_CHARTS.charColorSets.find(s => s.name === this.colorScheme);
        if (isNullOrUndefined(this.colorSet)) {
            this.colorSet = VINCOFW_CHARTS.charColorSets.find(s => s.name === 'vivid');
        }
        if (this._data) {
            this.processData();
        }
    }

    select(data): void {
        console.log('Chart Item clicked', JSON.parse(JSON.stringify(data)));
        this.onSelect.emit(this.getSelectedId(data));
    }

}
