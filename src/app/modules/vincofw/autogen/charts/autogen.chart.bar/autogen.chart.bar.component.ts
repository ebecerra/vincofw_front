import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {EventChartSelectModel} from '../../../models/event/event.chart.model';
import {AppCacheService} from '../../../services/cache.service';
import {GlobalEventsService} from '../../../services/global.events.service';
import {AutogenBaseChartBarlineComponent} from '../autogen.base.chart.barline.component';

@Component({
    selector: 'autogen-chart-bar',
    templateUrl: './autogen.chart.bar.component.html',
    styleUrls: ['./autogen.chart.bar.component.scss']
})
export class AutogenChartBarComponent extends AutogenBaseChartBarlineComponent implements OnInit, OnDestroy {

    @Input() barMode: string;
    @Input() showDataLabel = true;
    @Input() roundBarEdges = true;
    @Input() hideBarZero = true;

    /**
     * Constructor
     */
    constructor(
        _cacheService: AppCacheService,
        _globalEventsService: GlobalEventsService
    ) {
        super(_cacheService, _globalEventsService);
    }

    ngOnInit(): void {
        this.setColors();
    }

    ngOnDestroy(): void {
    }

    useUniqueSerie(): boolean {
        return this.barMode === 'BAR_CHARTS_VERT' || this.barMode === 'BAR_CHARTS_HORIZ';
    }

    setValues(): void {
        this.data.series.forEach((serie, index) => {
            if (serie.color) {
                this.colorSet.domain[index] = serie.color;
            }
            serie.values.forEach(value => {
                let valSerie = this.values.find(v => v.id === value.id);
                if (!valSerie) {
                    valSerie = {
                        id: value.id,
                        name: this.data.dateMode === 'DAY' ? value.x : value.fullX,
                        series: []
                    };
                    this.values.push(valSerie);
                }

                valSerie.series.push({
                    name: serie.legend,
                    value: value.value,
                    extra: {
                        code: value.id,
                        name: value.x
                    }
                });
            });
        });
    }

    getSelectedId(data: any): EventChartSelectModel {
        if (data.extra) {
            return {
                idChart: this._data.idChart,
                code: data.extra.code,
                name: (this._data.ctype === 'BAR_CHARTS_HORIZ' ? this._data.titleY : this._data.titleX) + ': ' + data.extra.name
            };
        }
        return null;
    }
}
