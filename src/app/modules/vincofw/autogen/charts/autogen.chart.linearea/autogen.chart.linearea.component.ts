import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import * as shape from 'd3-shape';
import {EventChartSelectModel, EventChartTypeModel} from '../../../models/event/event.chart.model';
import {AppCacheService} from '../../../services/cache.service';
import {GlobalEventsService} from '../../../services/global.events.service';
import {AutogenBaseChartBarlineComponent} from '../autogen.base.chart.barline.component';

@Component({
    selector: 'autogen-chart-linearea',
    templateUrl: './autogen.chart.linearea.component.html',
    styleUrls: ['./autogen.chart.linearea.component.scss']
})
export class AutogenChartLineareaComponent extends AutogenBaseChartBarlineComponent implements OnInit, OnDestroy {

    curves = {
        Basis: shape.curveBasis,
        Basis_Closed: shape.curveBasisClosed,
        Bundle: shape.curveBundle.beta(1),
        Cardinal: shape.curveCardinal,
        Cardinal_Closed: shape.curveCardinalClosed,
        Catmull_Rom: shape.curveCatmullRom,
        Catmull_Rom_Closed: shape.curveCatmullRomClosed,
        Linear: shape.curveLinear,
        Linear_Closed: shape.curveLinearClosed,
        Monotone_X: shape.curveMonotoneX,
        Monotone_Y: shape.curveMonotoneY,
        Natural: shape.curveNatural,
        Step: shape.curveStep,
        Step_After: shape.curveStepAfter,
        Step_Before: shape.curveStepBefore,
        default: shape.curveLinear
    };

    yScaleMin: number;
    yScaleMax: number;
    noData: boolean;
    curve: any = this.curves.default;

    @Input() areaMode: string;
    @Input() autoScale: boolean;
    @Input() timeLine: boolean;

    @Input()
    set lineInterpolation(value: string) {
        if (this.curves[value]) {
            this.curve  = this.curves[value];
        }
    }

    /**
     * Constructor
     */
    constructor(
        _cacheService: AppCacheService,
        _globalEventsService: GlobalEventsService
    ) {
        super(_cacheService, _globalEventsService);
    }

    ngOnInit(): void {
        this.setColors();
    }

    ngOnDestroy(): void {
    }

    useUniqueSerie(): boolean {
        return false;
    }

    setValues(): void {
        this.noData = true;
        this.data.series.forEach((serie, index) => {
            if (serie.color) {
                this.colorSet.domain[index] = serie.color;
            }
            const valSerie = {
                name: serie.legend,
                series: []
            };
            serie.values.forEach(val => {
                const value = parseFloat(val.value);
                valSerie.series.push({
                    name: this.data.dateMode === 'DAY' ? val.x : val.fullX,
                    value: value,
                    extra: {
                        code: val.id,
                        name: val.fullX
                    }
                });
                if (value !== 0) {
                    this.noData = false;
                }
            });
            this.values.push(valSerie);
        });
        this.resetScale();
    }

    changeChartType(data: EventChartTypeModel): void {
        if (data.previousChartType !== this.areaMode) {
            this.resetScale();
        }
    }

    private resetScale(): void {
        if (!this.noData) {
            this.yScaleMin = Number.MAX_SAFE_INTEGER;
            this.yScaleMax = Number.MIN_SAFE_INTEGER;
            if (this.areaMode === 'LINEAREA_AREA_STACKED') {
                const valuesLen = this.values[0].series.length;
                this.yScaleMin = 0;
                for (let i = 0; i < valuesLen; i++) {
                    let sum = 0;
                    this.values.forEach(value => {
                        sum += value.series[i].value;
                    });
                    if (sum > this.yScaleMax) {
                        this.yScaleMax = sum;
                    }
                }
            } else {
                this.values.forEach(value => {
                    value.series.forEach(serie => {
                        if (serie.value > this.yScaleMax) {
                            this.yScaleMax = serie.value;
                        }
                        if (serie.value < this.yScaleMin) {
                            this.yScaleMin = serie.value;
                        }
                    });
                });
            }
            if (this.areaMode !== 'LINEAREA_POLAR' && this.areaMode !== 'LINEAREA_AREA_NORMALIZED') {
                const percent = this.yScaleMax * 0.05;
                this.yScaleMax = this.yScaleMax + percent;
                this.yScaleMin = this.yScaleMin - percent;
            }
        }
        if (this.autoScale) {
            this.yScaleMin = null;
            this.yScaleMax = null;
        }
    }

    getSelectedId(data: any): EventChartSelectModel {
        const serie = this.values.find(v => v.name === data.series);
        if (serie) {
            const item = serie.series.find(v => v.name === data.name);
            if (item) {
                return {
                    idChart: this._data.idChart,
                    code: item.extra.code,
                    name: this._data.titleX + ': ' + item.extra.name
                };
            }
        }
        return null;
    }

}
