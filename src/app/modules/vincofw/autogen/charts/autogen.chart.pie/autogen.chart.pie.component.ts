import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AutogenBaseChartComponent} from '../autogen.base.chart.component';
import {AppCacheService} from '../../../services/cache.service';
import {GlobalEventsService} from '../../../services/global.events.service';

@Component({
    selector: 'autogen-chart-pie',
    templateUrl: './autogen.chart.pie.component.html',
    styleUrls: ['./autogen.chart.pie.component.scss']
})
export class AutogenChartPieComponent extends AutogenBaseChartComponent implements OnInit, OnDestroy {

    @Input() isDoughnut = false;
    @Input() explodeSlices = false;
    @Input() showLabels = true;
    @Input() pieMode = 'PIE';
    @Input() arcWidth = 0.45;

    values: any[] = [];

    /**
     * Constructor
     */
    constructor(
        _cacheService: AppCacheService,
        _globalEventsService: GlobalEventsService
    ) {
        super(_cacheService, _globalEventsService);
    }

    ngOnInit(): void {
        this.setColors();
    }

    ngOnDestroy(): void {
    }

    processData(): void {
        this.values = [];
        if (this.data.series && this.data.series.length > 0) {
            this.data.series[0].values.forEach(value => {
                this.values.push({
                    name: value.fullX,
                    value: value.value,
                    extra: {
                        code: value.id,
                        name: value.x
                    }
                });
            });
        }
    }

}
