import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AutogenComponent} from '../autogen.component';
import {FuseConfigService} from '../../../../../@fuse/services/config.service';
import {NotificationService} from '../../notification/notification.service';
import {AdTabMainDtoModel} from '../../models/dto/ad.tab.main.dto.model';
import {SortOrder} from '../../models/ad.base.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {UtilitiesService} from '../../services/utilities.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {AutogenUsedinchildModel} from '../../models/autogen.usedinchild.model';

@Component({
  selector: 'vinco-custom-tab',
  templateUrl: './vinco.custom.tab.component.html',
  styleUrls: ['./vinco.custom.tab.component.scss']
})
export class VincoCustomTabComponent extends AutogenComponent implements OnInit, OnDestroy, AfterViewInit {

    tabActive: boolean;
    parentTab: AdTabMainDtoModel;
    errors: string = null;
    fwErrors: string = null;
    fwTab = false;
    customModuleName = '';
    customTabName = '';
    _parentItem: any;

    // Process execution log
    fwLogData: any[];

    @Input() usedInChild: AutogenUsedinchildModel[];
    @Input() loadOnInit: boolean;
    @Input() showInPopup = false;
    @Input()
    set parentItem(item: any) {
        this._parentItem = item;
        if (this.initialize) {
            this.doLoadContent();
        }
    }

    get parentItem(): any {
        return this._parentItem;
    }

    constructor(
        protected _fuseConfigService: FuseConfigService,
        protected _notificationService: NotificationService,
        protected _cacheService: AppCacheService,
        protected _autogenService: AutogenService,
        private _globalEventsService: GlobalEventsService,
        private _utilitiesService: UtilitiesService
    ) {
        super(_fuseConfigService, _notificationService, _cacheService, _autogenService);
    }

    ngOnInit(): void {
        console.log('VincoCustomTabComponent: ngOnInit');
        this.tabActive = this.loadOnInit;
        const window = this._cacheService.getWindowFromCache(this.tab.idWindow);
        this.parentTab = this._cacheService.getParentTab(window.mainTab, this.tab.idTab);
        const command = this.tab.tab.command.split(':');
        if (command.length === 2) {
            this.customModuleName = command[0].trim();
            this.customTabName = command[1].trim();
            this.fwTab = this.customModuleName === 'vinco_core';
        } else {
            this.errors = this._cacheService.getTranslation('AD_CustomTabError', [this.tab.tab.command]);
        }
        this.onInit();
    }

    ngAfterViewInit(): void {
        if (this.fwTab) {
            this.doLoadContent();
        }
    }

    /**
     * Loads the table content
     */
    private doLoadContent(): void {
        if (this.fwTab) {
            if (this.customTabName === 'process.execution.log') {
                this.loadProcessExecutionLog();
            } else if (this.customTabName === 'designer') {
                this.loadDesigner();
            } else if (this.customTabName === 'process.planning') {
                this.loadProcessPlanning();
            } else if (this.customTabName === 'menu.role') {
                this.loadMenuRole();
            } else if (this.customTabName === 'sql.execute') {
                this.loadQueryExec();
            } else {
                this.fwErrors = `Sin implementar: ${this.tab.tab.command}`;
            }
        }
    }

    /**
     * Load process execution log
     */
    private loadProcessExecutionLog(): void {
        this._utilitiesService.loadTableContent(
            UtilitiesService.TABLES.AdProcessLog,
            { q: `idProcessExec=${this.parentItem.idProcessExec}`},
            [{ property: 'created', direction: SortOrder.ASC }]
        ).then(data => {
            this.fwLogData = data;
        });
    }

    /**
     * Load form designer
     */
    private loadDesigner(): void {
        // No task to do
    }

    /**
     * Load process planning
     */
    loadProcessPlanning(): void {
        // No task to do
    }


    /**
     * Load query executor
     */
    loadQueryExec(): void {
        // No task to do
    }

    /**
     * Load menus for role
     */
    loadMenuRole(): void {
        // No task to do
    }

}
