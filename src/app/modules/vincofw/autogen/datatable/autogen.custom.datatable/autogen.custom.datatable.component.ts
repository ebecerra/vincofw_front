import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {SortData} from '../../../models/ad.base.model';
import {AdColumnDtoModel} from '../../../models/dto/ad.column.dto.model';
import {SortOrder} from '../../../models/sort-order.enum';
import {UtilitiesService} from '../../../services/utilities.service';
import {LocalstoreService} from '../../../services/localstore.service';
import {AdMenuService} from '../../../services/ad.menu.service';
import {GlobalEventsService} from '../../../services/global.events.service';
import {AppCacheService} from '../../../services/cache.service';
import {ConditionalService} from '../../../services/conditional.service';
import {AutogenTableBaseComponent} from '../../autogen.table.base.component';
import {NotificationService} from '../../../notification/notification.service';

@Component({
    selector: 'autogen-custom-datatable',
    templateUrl: './autogen.custom.datatable.component.html',
    styleUrls: ['./autogen.custom.datatable.component.scss']
})
export class AutogenCustomDatatableComponent extends AutogenTableBaseComponent {

    displayedColumns: string[];
    dataSource: MatTableDataSource<any>;
    selection = new SelectionModel<any>(true, []);
    rows = [];
    minWidth = '60rem';
    gridTemplateColumns = '1fr';

    @ViewChild(MatSort) sort: MatSort;

    /**
     * Constructor
     */
    constructor(
        _router: Router,
        _menuService: AdMenuService,
        _notificationService: NotificationService,
        _globalEventsService: GlobalEventsService,
        _utilitiesService: UtilitiesService,
        _conditionalService: ConditionalService,
        _cacheService: AppCacheService,
        private _localstoreService: LocalstoreService,
    ) {
        super(_router, _menuService, _notificationService, _globalEventsService, _utilitiesService, _conditionalService, _cacheService);
    }

    /**
     * Set page content information
     */
    protected setPageContent(): void {
        super.setPageContent();
        this.selection.clear();
        this.rows = this._content.content;
        this.dataSource = new MatTableDataSource(this._content.content);
    }

    /**
     * Updates the visible columns
     */
    protected refreshVisibleColumns(): void {
        super.refreshVisibleColumns();
        this.setSortColumn(this._localstoreService.loadSort(this.tab.tab.idTab));
        this.displayedColumns = ['select'].concat(this.visibleColumns.map(column => column.name));
    }

    /**
     * This function is called when related tab was loaded
     */
    protected relatedTabLoaded(): void {
        let minWidth = 3;
        let gridTemplateColumns = '3rem';
        this.visibleColumns.forEach(column => {
            gridTemplateColumns += ' ';
            if (column.relatedField.guiCustomWidth) {
                gridTemplateColumns += column.relatedField.guiCustomWidth;
                minWidth += column.relatedField.guiCustomWidth.indexOf('fr') > 0 ? 20 : parseInt(column.relatedField.guiCustomWidth, 10);
            } else {
                switch (column.reference.rtype) {
                    case 'YESNO':
                    case 'DATE':
                    case 'TIME':
                    case 'INTEGER':
                    case 'NUMBER':
                    case 'PASSWORD':
                        gridTemplateColumns += '10rem';
                        minWidth += 10;
                        break;

                    case 'IMAGEVIEW':
                    case 'IMAGE_ENCODE':
                        gridTemplateColumns += '10rem';
                        minWidth += 10;
                        break;

                    case 'TIMESTAMP':
                    case 'LIST':
                        gridTemplateColumns += '20rem';
                        minWidth += 20;
                        break;

                    case 'ACTIONS':
                        let w = this.actions.length * 3;
                        if (w < 9) {
                            w = 9;
                        }
                        gridTemplateColumns += w + 'rem';
                        minWidth += w;
                        break;

                    default:
                        gridTemplateColumns += '1fr';
                        minWidth += 20;
                }
            }
        });
        this.minWidth = minWidth + 'rem';
        this.gridTemplateColumns = gridTemplateColumns;
    }

    /**
     * Handles the pagination change logic
     * @param pageEvent Pagination information
     */
    changePageInfo(pageEvent: PageEvent): void {
        this.page.pageNumber = pageEvent.pageIndex;
        this.pageChanged.emit(this.page);
    }

    /**
     * Handles the sort event
     *
     * @param column Sort information
     */
    onSort(column: AdColumnDtoModel): void {
        const sort = new SortData();
        sort.property = column.name;
        sort.direction = column.sort ? (column.sort.direction === SortOrder.ASC ? SortOrder.DESC : SortOrder.ASC) : SortOrder.ASC;
        this.setSortColumn(sort);
        this.sortChanged.emit([sort]);
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle(): void {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
        this.selectedElementsChanged.emit(this.selection.selected);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggleRow(row: any): void {
        this.selection.toggle(row);
        this.selectedElementsChanged.emit(this.selection.selected);
    }

    onDlbClick(row: any): void {
        this.selection.clear();
        this.selection.select(row);
        this.editSelected.emit(this.selection.selected);
    }

    private setSortColumn(sort: SortData): void {
        this.visibleColumns.forEach(col => {
            col.sortable = col.relatedField.sortable;
            col.sort = sort && sort.property === col.name ? sort : null;
        });
    }

}
