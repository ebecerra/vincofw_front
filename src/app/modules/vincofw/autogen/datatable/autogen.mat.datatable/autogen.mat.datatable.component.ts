import {Component, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {Router} from '@angular/router';
import {MatSort, MatTableDataSource, PageEvent} from '@angular/material';
import {SortData} from '../../../models/ad.base.model';
import {SortOrder} from '../../../models/sort-order.enum';
import {UtilitiesService} from '../../../services/utilities.service';
import {AdMenuService} from '../../../services/ad.menu.service';
import {GlobalEventsService} from '../../../services/global.events.service';
import {AppCacheService} from '../../../services/cache.service';
import {ConditionalService} from '../../../services/conditional.service';
import {AutogenTableBaseComponent} from '../../autogen.table.base.component';
import {NotificationService} from '../../../notification/notification.service';

@Component({
    selector: 'autogen-mat-datatable',
    templateUrl: './autogen.mat.datatable.component.html',
    styleUrls: ['./autogen.mat.datatable.component.scss']
})
export class AutogenMatDatatableComponent extends AutogenTableBaseComponent {

    displayedColumns: string[];
    dataSource: MatTableDataSource<any>;
    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;

    rows = [];

    /**
     * Constructor
     */
    constructor(
        _router: Router,
        _menuService: AdMenuService,
        _notificationService: NotificationService,
        _globalEventsService: GlobalEventsService,
        _utilitiesService: UtilitiesService,
        _conditionalService: ConditionalService,
        _cacheService: AppCacheService
    ) {
        super(_router, _menuService, _notificationService, _globalEventsService, _utilitiesService, _conditionalService, _cacheService);
    }

    /**
     * Set page content information
     */
    protected setPageContent(): void {
        super.setPageContent();
        this.selection.clear();
        this.rows = this._content.content;
        this.dataSource = new MatTableDataSource(this._content.content);
    }

    /**
     * Updates the visible columns
     */
    protected refreshVisibleColumns(): void {
        super.refreshVisibleColumns();
        this.displayedColumns = ['select'].concat(this.visibleColumns.map(column => column.name));
    }

    /**
     * Handles the pagination change logic
     * @param pageEvent Pagination information
     */
    changePageInfo(pageEvent: PageEvent): void {
        this.page.pageNumber = pageEvent.pageIndex;
        this.pageChanged.emit(this.page);
    }

    /**
     * Handles the sort event
     * @param event Sort information
     */
    onSort(event): void {
        const sort = new SortData();
        sort.direction = 'asc' === (event as any).direction ? SortOrder.ASC : SortOrder.DESC;
        sort.property = (event as any).active;
        this.sortChanged.emit([sort]);
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle(): void {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
        this.selectedElementsChanged.emit(this.selection.selected);
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: any): string {
        if (!row) {
            return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
        }
        return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    toggleRow(row: any): void {
        this.selection.toggle(row);
        this.selectedElementsChanged.emit(this.selection.selected);
    }

    onDlbClick(row: any): void {
        this.selection.clear();
        this.selection.select(row);
        this.editSelected.emit(this.selection.selected);
    }
}
