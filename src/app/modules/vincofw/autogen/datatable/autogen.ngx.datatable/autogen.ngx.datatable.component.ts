import {AfterViewChecked, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {SortData} from '../../../models/ad.base.model';
import {SortOrder} from '../../../models/sort-order.enum';
import {UtilitiesService} from '../../../services/utilities.service';
import {GlobalEventsService} from '../../../services/global.events.service';
import {AdMenuService} from '../../../services/ad.menu.service';
import {AppCacheService} from '../../../services/cache.service';
import {ConditionalService} from '../../../services/conditional.service';
import {NotificationService} from '../../../notification/notification.service';
import {AutogenTableBaseComponent} from '../../autogen.table.base.component';

@Component({
    selector: 'autogen-ngx-datatable',
    templateUrl: './autogen.ngx.datatable.component.html',
    styleUrls: ['./autogen.ngx.datatable.component.scss']
})
export class AutogenNgxDatatableComponent extends AutogenTableBaseComponent implements AfterViewChecked {
    fuseConfig: any;
    selected: any[] = [];

    // Private
    private currentComponentWidth;

    @ViewChild('tableWrapper') tableWrapper;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    /**
     * Constructor
     */
    constructor(
        _router: Router,
        _menuService: AdMenuService,
        _notificationService: NotificationService,
        _globalEventsService: GlobalEventsService,
        _utilitiesService: UtilitiesService,
        _conditionalService: ConditionalService,
        _cacheService: AppCacheService,
        private _changeDetectorRef: ChangeDetectorRef,
    ) {
        super(_router, _menuService, _notificationService, _globalEventsService, _utilitiesService, _conditionalService, _cacheService);
    }

    /**
     * Set page content information
     */
    protected setPageContent(): void {
        super.setPageContent();
        this.selected = [];
    }

    /**
     * Populate the table with new data based on the page number
     *
     * @param pageInfo The page to select
     */
    setPage(pageInfo): void {
        this.page.pageNumber = pageInfo.offset;
        this.pageChanged.emit(this.page);
    }

    /**
     * Handles the sort event
     *
     * @param event Sort information
     */
    onSort(event): void {
        this.sortChanged.emit((event.sorts as []).map(current => {
            const sort = new SortData();
            sort.direction = 'asc' === (current as any).dir ? SortOrder.ASC : SortOrder.DESC;
            sort.property = (current as any).prop;
            return sort;
        }));
    }

    onSelect({selected}): void {
        this.selected = [];
        this.selected.push(...selected);
        this.selectedElementsChanged.emit(this.selected);
    }

    onActivate(event): void {
        if (event.type === 'dblclick') {
            this.selected = [event.row];
            this.editSelected.emit(this.selected);
        }
    }

    ngAfterViewChecked(): void {
        // Check if the table size has changed,
        this.currentComponentWidth = this.tableWrapper.nativeElement.clientWidth;
        this.table.recalculate();
        this._changeDetectorRef.detectChanges();
    }
}
