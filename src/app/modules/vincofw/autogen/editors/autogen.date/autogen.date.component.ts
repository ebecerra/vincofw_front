import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

import * as moment_ from 'moment';

const moment = moment_;

@Component({
    selector: 'autogen-date',
    templateUrl: './autogen.date.component.html',
    styleUrls: ['./autogen.date.component.scss']
})
export class AutogenDateComponent implements OnInit, OnDestroy {

    contextName: AutogenTemplateContextModel;
    contextFromName: AutogenTemplateContextModel;
    contextToName: AutogenTemplateContextModel;
    id: number;

    @Input() filterRange = false;
    @Input() time = false;
    @Input() readOnly = false;
    @Input() appearance = '';
    @Input() label = '';
    @Input() name = '_date_';
    @Input() description: string = null;
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() min: string = null;
    @Input() max: string = null;

    @Output() action: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor() {
        this.id = new Date().getTime();
    }

    ngOnInit(): void {
        if (this.readOnly) {
            const formValue = this.autogenForm.getRawValue()[this.name];
            if (formValue && typeof (formValue) === 'object') {
                const value = {};
                value[this.name] = moment(formValue).format('DD/MM/YYYY');
                this.autogenForm.patchValue(value);
            }
        } else if (this.filterRange) {
            this.contextFromName = {
                templContext: {
                    label: this.label,
                    name: '$from$' + this.name,
                    description: this.description,
                    hint: this.hint
                }
            };
            this.contextToName = {
                templContext: {
                    label: null,
                    name: '$to$' + this.name,
                    description: null,
                    hint: null
                }
            };
        } else {
            this.contextName = {
                templContext: {
                    label: this.label,
                    name: this.name,
                    description: this.description,
                    hint: this.hint
                }
            };
        }
    }

    ngOnDestroy(): void {
    }

    onHelp(): void {
        this.action.emit('help');
    }
}
