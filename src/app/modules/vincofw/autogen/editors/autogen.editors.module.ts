import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {AutogenDateComponent} from './autogen.date/autogen.date.component';
import {AutogenListComponent} from './autogen.list/autogen.list.component';
import {AutogenTableComponent} from './autogen.table/autogen.table.component';
import {AutogenTabledirComponent} from './autogen.tabledir/autogen.tabledir.component';
import {AutogenTextboxComponent} from './autogen.textbox/autogen.textbox.component';
import {AutogenYesnoComponent} from './autogen.yesno/autogen.yesno.component';
import {MaterialModule} from '../../../../material.module';
import {PipesModule} from '../../../../pipes/pipes.module';
import {AutogenHtmlComponent} from './autogen.html/autogen.html.component';
import {AutogenFileComponent} from './autogen.file/autogen.file.component';
import {AutogenImageencodeComponent} from './autogen.imageencode/autogen.imageencode.component';

@NgModule({
    declarations: [
        AutogenDateComponent,
        AutogenFileComponent,
        AutogenHtmlComponent,
        AutogenImageencodeComponent,
        AutogenListComponent,
        AutogenTableComponent,
        AutogenTabledirComponent,
        AutogenTextboxComponent,
        AutogenYesnoComponent
    ],
    entryComponents: [
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        PipesModule,
        CKEditorModule
    ],
    providers: [
    ],
    exports: [
        AutogenDateComponent,
        AutogenFileComponent,
        AutogenHtmlComponent,
        AutogenImageencodeComponent,
        AutogenListComponent,
        AutogenTableComponent,
        AutogenTabledirComponent,
        AutogenTextboxComponent,
        AutogenYesnoComponent
    ]
})

export class AutogenEditorsModule {
}
