import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {HttpClient, HttpErrorResponse, HttpEventType, HttpRequest} from '@angular/common/http';
import {of, Subscription} from 'rxjs';
import {catchError, last, map, tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {AppCoreSettings} from '../../../configs/app.core.settings';
import {ControllerResultModel} from '../../../models/controller.result.model';
import {AdTableDtoModel} from '../../../models/dto/ad.table.dto.model';
import {AppCacheService} from '../../../services/cache.service';
import {AutogenService} from '../../../services/autogen.service';
import {NotificationService} from '../../../notification/notification.service';

export class FileUploadModel {
    data: File;
    state: string;
    inProgress: boolean;
    progress: number;
    canRetry: boolean;
    canCancel: boolean;
    sub?: Subscription;
}

@Component({
    selector: 'autogen-file',
    templateUrl: './autogen.file.component.html',
    styleUrls: ['./autogen.file.component.scss']
})
export class AutogenFileComponent implements OnInit, OnDestroy {

    file: FileUploadModel = null;
    _fileName: string;
    _fName: string;
    _uploadError = false;

    @Input() readOnly = false;
    @Input() appearance = '';
    @Input() label = '';
    @Input() name = '_date_';
    @Input() description: string = null;
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() min: string = null;
    @Input() max: string = null;
    @Input() accept = 'image/*';
    @Input() table: AdTableDtoModel;
    @Input() rowId: string;

    @Input()
    set fileName(fn: string) {
        this._fileName = fn;
        if (!isNullOrUndefined(fn) && fn.split('$').length === 3) {
            this.file = null;
            this._fName = fn.split('$')[2];
        } else {
            this._fName = fn;
        }
    }

    @Output() action = new EventEmitter<any>();
    @Output() complete = new EventEmitter<ControllerResultModel>();
    @Output() delete = new EventEmitter<void>();

    /**
     * Constructor
     */
    constructor(
        private _http: HttpClient,
        private _settings: AppCoreSettings,
        private _autogenService: AutogenService,
        private _cacheService: AppCacheService,
        private _notificationService: NotificationService
    ) {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    onHelp(): void {
        this.action.emit('help');
    }

    doClick(): void {
        const fileUpload = document.getElementById(this.name) as HTMLInputElement;
        fileUpload.onchange = () => {
            for (let index = 0; index < fileUpload.files.length; index++) {
                this.file = {
                    data: fileUpload.files[index],
                    state: 'in',
                    inProgress: false,
                    progress: 0,
                    canRetry: false,
                    canCancel: true
                };
            }
            this.uploadFile(this.file);
        };
        fileUpload.click();
    }

    cancelFile(file: FileUploadModel): void {
        file.sub.unsubscribe();
    }

    retryFile(file: FileUploadModel): void {
        this.uploadFile(file);
        file.canRetry = false;
    }

    deleteFile(): void {
        const dialogRef = this._notificationService.questionDialog(
            this._cacheService.getTranslation('AD_GlobalConfirm'),
            this._cacheService.getTranslation('AD_msgDeleteFile'),
            'warning'
        );
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'YES') {
                const service = this._autogenService.bindedService(this.table);
                service.deleteImage(this.rowId, this.name, this._fileName).subscribe(() => {
                    this.fileName = null;
                    this.delete.emit();
                });
            }
        });
    }
    
    private uploadFile(file: FileUploadModel): void {
        const fd = new FormData();
        fd.append('fileUpload', file.data);
        fd.append('field', this.name);
        fd.append('table', typeof this.table === 'string' ? this.table : this.table.name);

        const url = this._settings.getRestHostCore() + 'ad_file/' + this._settings.idClient  + '/upload';
        const req = new HttpRequest('POST', url, fd, {
            reportProgress: true
        });

        this._uploadError = false;
        file.inProgress = true;
        file.sub = this._http.request(req).pipe(
            map(event => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        file.progress = Math.round(event.loaded * 100 / event.total);
                        break;
                    case HttpEventType.Response:
                        file.inProgress = false;
                        return event;
                }
            }),
            tap(message => { }),
            last(),
            catchError((error: HttpErrorResponse) => {
                file.inProgress = false;
                file.canRetry = true;
                return of(`${file.data.name} upload failed.`);
            })
        ).subscribe(
            (event: any) => {
                if (typeof (event) === 'object') {
                    if (event.body.success) {
                        this.complete.emit(event.body);
                    } else {
                        this._uploadError = true;
                    }
                }
            }
        );
    }

}
