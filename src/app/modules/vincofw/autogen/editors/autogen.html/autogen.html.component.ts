import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Hook} from '../../../models/hook';
import {AdHookNames, HookService} from '../../../services/hook.service';

class AdFormButtonSaveHtmlHook extends Hook {

    constructor(private htmlComponent: AutogenHtmlComponent) {
        super('AdFormButtonSaveHtmlHook', AdHookNames.AD_FORM_BUTTON_SAVE);
    }

    handler(args): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.htmlComponent.doSave()) {
                resolve(null);
            } else {
                reject();
            }
        });
    }
}

@Component({
    selector: 'autogen-html',
    templateUrl: './autogen.html.component.html',
    styleUrls: ['./autogen.html.component.scss']
})
export class AutogenHtmlComponent implements OnInit, OnDestroy {

    public ckEditor = ClassicEditor;
    content = '';
    mandatoryError = null;

    @Input() readOnly = false;
    @Input() appearance = '';
    @Input() label = '';
    @Input() name = '_html_';
    @Input() description: string = null;
    @Input() placeholder = '';
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() translatable = false;
    @Input() mandatory = false;

    @Output() action: EventEmitter<any> = new EventEmitter();

    constructor(private _hookervice: HookService) { }

    ngOnInit(): void {
        this.content = this.autogenForm.value[this.name] ? this.autogenForm.value[this.name] : '';
        this._hookervice.registerHook(new AdFormButtonSaveHtmlHook(this));
    }

    ngOnDestroy(): void {
        this._hookervice.unregisterHook('AdFormButtonSaveHtmlHook');
    }

    onTranslate(): void {
        this.action.emit('translate');
    }

    onHelp(): void {
        this.action.emit('help');
    }

    doSave(): boolean {
        this.content = this.content.trim();
        if (this.mandatory && this.content.length === 0) {
            this.mandatoryError = true;
            return false;
        }
        this.mandatoryError = false;
        const value = {};
        value[this.name] = this.content.length === 0 ? null : this.content;
        this.autogenForm.patchValue(value);
        return true;
    }
}
