import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {isNullOrUndefined} from 'util';

@Component({
    selector: 'autogen-imageencode',
    templateUrl: './autogen.imageencode.component.html',
    styleUrls: ['./autogen.imageencode.component.scss']
})
export class AutogenImageencodeComponent implements OnInit, OnDestroy {

    private _value: string;
    hasImage = false;
    error: string;

    @Input() label = '';
    @Input() appearance = '';
    @Input() readOnly = false;
    @Input() autogenForm: FormGroup;
    @Input() name = '_imageencode_';
    @Input() hint: string = null;
    @Input() accept = 'image/*';
    @Input() description: string = null;

    @Input()
    set value(v: string) {
        this._value = v;
        this.hasImage = !isNullOrUndefined(v);
    }

    get value(): string {
        return this._value;
    }

    @Output() action = new EventEmitter<any>();

    /**
     * Constructor
     */
    constructor() {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    onHelp(): void {
        this.action.emit('help');
    }

    doClick(): void {
        const fileUpload = document.getElementById(this.name) as HTMLInputElement;
        fileUpload.onchange = () => {
            for (let index = 0; index < fileUpload.files.length; index++) {
                const file = fileUpload.files[index];
                if (!file.type) {
                    this.error = 'AD_msgErrorImageFileType';
                    return;
                }
                if (!file.type.match('image.*')) {
                    this.error = 'AD_msgErrorImageFileMIME';
                    return;
                }
                const reader = new FileReader();
                reader.addEventListener('load', event => {
                    const imageEncode = (event.target as FileReader).result as string;
                    this.autogenForm.get(this.name).setValue(imageEncode);
                    this.value = imageEncode;
                });
                reader.readAsDataURL(file);
            }
        };
        fileUpload.click();
    }

    doDelete(): void {
        this.value = null;
        this.autogenForm.get(this.name).setValue(null);
    }
}
