import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {AppCacheService} from '../../../services/cache.service';
import {AdReferenceDtoModel} from '../../../models/dto/ad.reference.dto.model';
import {AutogenSelectValueModel} from '../../models/autogen.select.value.model';
import {AutogenSelectGroupModel} from '../../models/autogen.select.group.model';

@Component({
    selector: 'autogen-list',
    templateUrl: './autogen.list.component.html',
    styleUrls: ['./autogen.list.component.scss']
})
export class AutogenListComponent implements OnInit, OnDestroy {

    private _readOnly: boolean;
    private _reference: AdReferenceDtoModel;

    @Input() allowBlank = false;
    @Input() appearance = '';
    @Input() label = '';
    @Input() description: string = null;
    @Input() readOnly = false;
    @Input() name = '_list_';
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() multiple: boolean = null;
    @Input() grouped = false;
    @Input() items: AutogenSelectValueModel[] = null;
    @Input() groupedValues: AutogenSelectGroupModel[] = null;

    @Input()
    set reference(reference: AdReferenceDtoModel) {
        this._reference = reference;
        if (this._reference) {
            this.grouped = this._reference.grouped;
            if (this.multiple === null) {
                this.multiple = this._reference.multiple;
            }
            if (this._reference.refList) {
                this.setValues();
            } else {
                this._cacheService.getReferenceInfo([this._reference.idReference]).subscribe(references => {
                    this._reference.refList = references[0].refList;
                    this.setValues();
                });
            }
        }
    }

    get reference(): AdReferenceDtoModel {
        return this._reference;
    }

    @Output() action: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor(
        private _cacheService: AppCacheService
    ) {
        this._readOnly = false;
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    onHelp(): void {
        this.action.emit('help');
    }

    private setValues(): void {
        if (this.grouped) {
            this.groupedValues = [];
            // Insert groups
            this._reference.refList.filter(val => isNullOrUndefined(val.groupValue) || val.groupValue === '').forEach(g => {
                this.groupedValues.push({
                    id: g.value,
                    name: g.name,
                    values: []
                });
            });
            this._reference.refList.forEach(val => {
                if (val.groupValue) {
                    const group = this.groupedValues.find(g => g.id === val.groupValue);
                    if (group) {
                        group.values.push({
                            value: val.value,
                            caption: val.name
                        });
                    }
                }
            });
        } else {
            this.items = [];
            this._reference.refList.forEach(val => {
                this.items.push({
                    value: val.value,
                    caption: val.name
                });
            });
        }
    }
}
