import {HttpParams} from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import {Observable} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {SortData, SortOrder} from '../../../models/ad.base.model';
import {AdReferenceDtoModel} from '../../../models/dto/ad.reference.dto.model';
import {AdReferenceKeyValueModel} from '../../../models/dto/ad.reference.key.value.model';
import {AdMenuModel} from '../../../models/ad.menu.model';
import {AppCacheService} from '../../../services/cache.service';
import {AutogenService} from '../../../services/autogen.service';
import {AdHookNames, HookService} from '../../../services/hook.service';
import {ConditionalService} from '../../../services/conditional.service';
import {AdMenuService} from '../../../services/ad.menu.service';

@Component({
    selector: 'autogen-table',
    templateUrl: './autogen.table.component.html',
    styleUrls: ['./autogen.table.component.scss']
})
export class AutogenTableComponent implements OnInit, OnDestroy, AfterViewInit {
    private _reference: AdReferenceDtoModel = null;
    private _readOnly: boolean = null;
    private _name = '_table_';
    private sort: SortData;
    private tableService: AutogenService;
    private tableName = '';
    private keyName: string;
    private displayName: string;

    tableValues: AdReferenceKeyValueModel[];
    linkToMenu: AdMenuModel = null;
    isLoading = true;
    readOnlyName: string = null;
    id: number;

    @ViewChild('txtInput', { read: ElementRef }) txtInput: ElementRef;
    @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;

    @Input() parentTableId: string;
    @Input() limit = 20;
    @Input() delay = 300;
    @Input() allowBlank = false;
    @Input() appearance = '';
    @Input() placeholder = '';
    @Input() label = '';
    @Input() description: string = null;
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() item: any = null;
    @Input() editMode = false;

    @Input()
    set name(value: string) {
        this._name = value;
        this.readOnlyName = '$readOnly$' + this._name;
        this.createReadOnlyField();
    }

    get name(): string {
        return this._name;
    }

    @Input()
    set readOnly(readOnly: boolean) {
        this._readOnly = readOnly || false;
        this.createReadOnlyField();
        if (this._reference) {
            this.loadValues();
        }
    }

    get readOnly(): boolean {
        return this._readOnly;
    }

    @Input()
    set reference(reference: AdReferenceDtoModel) {
        if (!isNullOrUndefined(reference)){
            this._cacheService.getReferenceInfo([reference.idReference]).subscribe( values => {
                if (values.length > 0) {
                    this._reference = values[0];
                    this.linkToMenu = this.parentTableId !== this._reference.refTable.idTable ? this._menuService.getMenuByTable(this._reference.refTable.idTable) : null;
                    this.keyName = this._reference.refTable.keyName;
                    this.displayName = this._reference.refTable.displayName;
                    this._reference.refTable.runtimeSqlwhere = null;
                    if (this._readOnly !== null) {
                        this.loadValues();
                    }
                }
            });
        }
    }

    get reference(): AdReferenceDtoModel {
        return this._reference;
    }

    @Output() action: EventEmitter<any> = new EventEmitter();
    @Output() openWindow: EventEmitter<AdMenuModel> = new EventEmitter<AdMenuModel>();

    /**
     * Constructor
     */
    constructor(
        private _autogenService: AutogenService,
        private _cacheService: AppCacheService,
        private _conditionalService: ConditionalService,
        private _hookService: HookService,
        private _menuService: AdMenuService
    ) {
        this.tableValues = [];
        this.tableService = null;
        this.sort = null;
        this.id = new Date().getTime();
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    ngAfterViewInit(): void {
        const eventStream = Observable.fromEvent(this.txtInput.nativeElement, 'keyup')
            .map((result) => result['target'].value)
            .debounceTime(this.delay)
            .distinctUntilChanged();
        eventStream.subscribe(input => this.loadTableValues(input));
    }

    onHelp(): void {
        this.action.emit('help');
    }

    displayFn(value?: AdReferenceKeyValueModel): string | undefined {
        return value ? value.value : undefined;
    }

    clear(): void {
        this.txtInput.nativeElement.value = '';
        const value = {};
        value[this.name] = '';
        this.autogenForm.patchValue(value);
        this.loadTableValues('');
    }

    onOpenLinkWindow(): void {
        this.openWindow.emit(this.linkToMenu);
        setTimeout(() => {
            this.autocomplete.closePanel();
        }, 100);
    }

    private createReadOnlyField(): void {
        if (this._readOnly && this.readOnlyName !== null) {
            if (isNullOrUndefined(this.autogenForm.controls[this.readOnlyName])) {
                this.autogenForm.addControl(this.readOnlyName, new FormControl());
            }
        }
    }

    private loadValues(): void {
        this._cacheService.loadTableInfo(this._reference.refTable.idTable).subscribe(table => {
            if (!isNullOrUndefined(table)) {
                this.tableName = table.name;
                this.sort = new SortData();
                this.sort.property = this.reference.refTable.sqlorderby ? this.reference.refTable.sqlorderby : this.displayName;
                this.sort.direction = SortOrder.ASC;
                this.tableService = this._autogenService.bindedService(table);
                if (this._readOnly !== true) {
                    this.loadTableValues('');
                } else {
                    const value = {};
                    value[this.readOnlyName] = this.autogenForm.value[this.name].value;
                    this.autogenForm.patchValue(value);
                }
            }
        });
    }

    private loadTableValues(search: string): void {
        if (!isNullOrUndefined(this.tableService)) {
            this.isLoading = true;
            let q: string = search ? 'search=%' + search + '%' : '';
            if (this._reference.refTable.sqlwhere) {
                if (!this._reference.refTable.runtimeSqlwhere) {
                    const runtimeSqlwhere = this._conditionalService.replaceJSConditional(this._reference.refTable.sqlwhere, null, null, this.item);
                    if (this._conditionalService.isCompleteConditional(runtimeSqlwhere)) {
                        this._reference.refTable.runtimeSqlwhere = runtimeSqlwhere;
                    } else {
                        console.error('AutogenTableComponent: Reference table sqlWhere incomplete: ' + runtimeSqlwhere);
                        this.tableValues = [];
                        this.isLoading = false;
                        return;
                    }
                }
                q += this._reference.refTable.runtimeSqlwhere ? ',$extended$={' + this._reference.refTable.runtimeSqlwhere + '}' : '';
            }
            this._hookService.execHook(
                AdHookNames.AD_QUERY_TABLE, {
                    tableName: this.tableName,
                    constraints: q
                }
            ).then(response => {
                if (response.success) {
                    response.results.forEach(r => q += ',' + r);
                    let parameters = new HttpParams();
                    parameters = parameters.set('q', q);
                    this.tableService.list(parameters, [this.sort], this.limit, 1).subscribe(result => {
                        this.tableValues = [];
                        result.content.forEach(item => {
                            const option = new AdReferenceKeyValueModel();
                            option.key = item[this.keyName];
                            option.value = item[this.displayName];
                            this.tableValues.push(option);
                        });
                        this.isLoading = false;
                    });
                }
            });
        }
    }
}
