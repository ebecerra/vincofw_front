import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {AdReferenceDtoModel} from '../../../models/dto/ad.reference.dto.model';
import {AdReferenceKeyValueModel} from '../../../models/dto/ad.reference.key.value.model';
import {Hook} from '../../../models/hook';
import {AdMenuModel} from '../../../models/ad.menu.model';
import {AutogenSelectGroupModel} from '../../models/autogen.select.group.model';
import {AppCacheService} from '../../../services/cache.service';
import {AdHookNames, HookService} from '../../../services/hook.service';
import {AdMenuService} from '../../../services/ad.menu.service';

class TabledirAdListChangeValuesHook extends Hook {

    constructor(private linkedComponent: AutogenTabledirComponent) {
        super('TabledirAdListChangeValues_' + linkedComponent.name, AdHookNames.AD_LIST_CHANGE_VALUES);
    }

    handler(args): Promise<any> {
        return new Promise((resolve, reject) => {
            if (args.fieldName === this.linkedComponent.name) {
                this.linkedComponent.setRefValues(args.values);
            }
            resolve({ success: true, hook: this.identifier() });
        });
    }
}

@Component({
    selector: 'autogen-tabledir',
    templateUrl: './autogen.tabledir.component.html',
    styleUrls: ['./autogen.tabledir.component.scss']
})
export class AutogenTabledirComponent implements OnInit, OnDestroy {
    /**
     * Defines if the component is shown in read only mode
     */
    private _reference: AdReferenceDtoModel;
    private _values: any[];
    private referenceValues: AdReferenceKeyValueModel[];
    private hook: TabledirAdListChangeValuesHook;
    private grouped = false;
    private groupedValues: AutogenSelectGroupModel[] = null;

    linkToMenu: AdMenuModel = null;

    @ViewChild('listSelect') listSelect;

    @Input() parentTableId: string;
    @Input() allowBlank = false;
    @Input() appearance = '';
    @Input() readOnly = false;
    @Input() description: string = null;
    @Input() label = '';
    @Input() name = '_tabledir_';
    @Input() autogenForm: FormGroup;
    @Input() hint: string = null;
    @Input() multiple: boolean = null;
    @Input() item: any;
    @Input() editMode = false;

    @Input()
    set values(items: any[]) {
        this._values = items;
        if (this._reference) {
            this.loadValues();
        }
    }

    @Input()
    set reference(reference: AdReferenceDtoModel) {
        this._reference = reference;
        this._cacheService.getReferenceInfo([this._reference.idReference]).subscribe(references => {
            this._reference.refTable = references[0].refTable;
            this.grouped = this._reference.grouped;
            if (this.multiple === null) {
                this.multiple = this._reference.multiple;
            }
            this.linkToMenu = this.parentTableId !== this._reference.refTable.idTable ? this._menuService.getMenuByTable(this._reference.refTable.idTable) : null;
            if (isNullOrUndefined(this._reference.autoLoadValues) || this._reference.autoLoadValues) {
                this.loadReferenceValues();
            } else if (this._values) {
                this.loadValues();
            }
        });
    }

    get reference(): AdReferenceDtoModel {
        return this._reference;
    }

    @Output() action: EventEmitter<any> = new EventEmitter();
    @Output() openWindow: EventEmitter<AdMenuModel> = new EventEmitter<AdMenuModel>();

    /**
     * Constructor
     */
    constructor(
        private _cacheService: AppCacheService,
        private _hookService: HookService,
        private _menuService: AdMenuService
    ) {
        this.referenceValues = [];
    }

    ngOnInit(): void {
        this.hook = new TabledirAdListChangeValuesHook(this);
        this._hookService.registerHook(this.hook);
    }

    ngOnDestroy(): void {
        this._hookService.unregisterHook(this.hook.id);
    }

    public setRefValues(values: AdReferenceKeyValueModel[]): void {
        if (this.grouped) {
            this.groupedValues = [];
            values.forEach(val => {
                let group = this.groupedValues.find(g => g.id === val.groupedKey);
                if (isNullOrUndefined(group)) {
                    group = {
                        id: val.groupedKey,
                        name: val.groupedValue,
                        values: []
                    };
                    this.groupedValues.push(group);
                }
                group.values.push({
                    value: val.key,
                    caption: val.value
                });
            });
        } else {
            this.referenceValues = values;
        }
    }

    onHelp(): void {
        this.action.emit('help');
    }

    onOpenLinkWindow(): void {
        this.openWindow.emit(this.linkToMenu);
        setTimeout(() => {
            this.listSelect.close();
        }, 100);
    }

    private loadReferenceValues(): void {
        if (!this.readOnly && !isNullOrUndefined(this._reference)) {
            this._cacheService.getReferenceValues(this._reference, this.item).subscribe( values => {
                this.setRefValues(values);
            });
        }
    }

    private loadValues(): void {
        this.referenceValues = [];
        this._values.forEach(item => {
            this.referenceValues.push({
                key: item[this._reference.refTable.keyName],
                value: item[this._reference.refTable.displayName]
            });
        });
    }
}
