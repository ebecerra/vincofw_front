import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'autogen-textbox',
    templateUrl: './autogen.textbox.component.html',
    styleUrls: ['./autogen.textbox.component.scss']
})
export class AutogenTextboxComponent implements OnInit, OnDestroy {

    contextName: AutogenTemplateContextModel;
    contextFromName: AutogenTemplateContextModel;
    contextToName: AutogenTemplateContextModel;

    @Input() filterRange = false;
    @Input() label = '';
    @Input() description: string = null;
    @Input() name = '_text_';
    @Input() appearance = '';
    @Input() readOnly = false;
    @Input() placeholder = '';
    @Input() editorType = 'text';
    @Input() minValue: number = null;
    @Input() maxValue: number = null;
    @Input() autogenForm: FormGroup;
    @Input() translatable = false;
    @Input() hint: string = null;
    @Input() rows = 5;
    @Input() autocomplete = 'disabled'; // put on if you want to enable it

    @Output()
    public action: EventEmitter<any> = new EventEmitter();

    /**
     * Constructor
     */
    constructor() {

    }

    ngOnInit(): void {
        if (this.filterRange) {
            this.contextFromName = {
                templContext: {
                    label: this.label,
                    name: '$from$' + this.name,
                    description: this.description,
                    hint: this.hint
                }
            };
            this.contextToName = {
                templContext: {
                    label: null,
                    name: '$to$' + this.name,
                    description: null,
                    hint: null
                }
            };
        } else {
            this.contextName = {
                templContext: {
                    label: this.label,
                    name: this.name,
                    description: this.description,
                    hint: this.hint
                }
            };
        }
    }

    ngOnDestroy(): void {
    }

    onTranslate(): void {
        this.action.emit('translate');
    }

    onHelp(): void {
        this.action.emit('help');
    }
}
