import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'autogen-yesno',
    templateUrl: './autogen.yesno.component.html',
    styleUrls: ['./autogen.yesno.component.scss']
})
export class AutogenYesnoComponent implements OnInit, OnDestroy {

    @Input() label = '';
    @Input() appearance = '';
    @Input() readOnly = false;
    @Input() autogenForm: FormGroup;
    @Input() name = '_check_';
    @Input() value: boolean;

    /**
     * Constructor
     */
    constructor() {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }
}
