import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import 'rxjs-compat/add/observable/fromEvent';
import 'rxjs-compat/add/operator/debounceTime';
import 'rxjs-compat/add/operator/distinctUntilChanged';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'autogen-filter-search',
    templateUrl: './autogen.filter.search.component.html',
    styleUrls: ['./autogen.filter.search.component.scss']
})
export class AutogenFilterSearchComponent implements OnInit, OnDestroy {

    @Input() delay = 300;
    @Input() autogenForm: FormGroup;

    /**
     * Constructor
     */
    constructor() {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

}
