import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import 'rxjs-compat/add/observable/fromEvent';
import 'rxjs-compat/add/operator/debounceTime';
import 'rxjs-compat/add/operator/distinctUntilChanged';

@Component({
    selector: 'autogen-filter-yesno',
    templateUrl: './autogen.filter.yesno.component.html',
    styleUrls: ['./autogen.filter.yesno.component.scss']
})
export class AutogenFilterYesnoComponent implements OnInit, OnDestroy {
    // Private
    private _value: any;
    private _unsubscribeAll: Subject<any>;

    @Input() autogenForm: FormGroup;
    @Input() name = '_check_';
    @Input() label = '';

    @Input()
    set value(value: any) {
        this._value = value;
        this.valueChange.emit(value);
    }

    get value(): any {
        return this._value;
    }

    @Output()
    valueChange = new EventEmitter<any>();

    /**
     * Constructor
     */
    constructor() {
        this.valueChange = new EventEmitter<any>();
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
