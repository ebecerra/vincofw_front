import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdFieldGroupDesignerModel, AdFieldGroupViewModel} from '../../models/dto/ad.field.group.dto.model';
import {SortOrder} from '../../models/ad.base.model';
import {AdTabMainDtoModel} from '../../models/dto/ad.tab.main.dto.model';
import {AdDesignerInfoModel} from '../../models/ad.designer.info.model';
import {UtilitiesService} from '../../services/utilities.service';
import {AppCacheService} from '../../services/cache.service';
import {AdTabService} from '../../services/ad.tab.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {NotificationService} from '../../notification/notification.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';
import {debounceTime, distinctUntilChanged} from "rxjs/operators";

@Component({
    selector: 'form-designer',
    templateUrl: './form.designer.component.html',
    styleUrls: ['./form.designer.component.scss']
})
export class FormDesignerComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = [];
    private patternGUICustomWidth = new RegExp('^-?\\d{1,3}(\\.\\d+)?(fr|rem|em)');

    fields: AdFieldDtoModel[] = null;
    designerForm: FormGroup;
    groups: AdFieldGroupViewModel[] = null;
    selectedField: AdFieldDtoModel;
    ungroupedFields: AdFieldDtoModel[];
    ungroupedGridColumns: string;
    fieldGroups: AdFieldGroupDesignerModel[];
    showFormFields = false;
    designerModify = false;

    @Input() parentItem: any;
    @Input() parentTab: AdTabMainDtoModel;
    @Input() delay = 300;

    constructor(
        private _notificationService: NotificationService,
        private _globalEventsService: GlobalEventsService,
        private _cacheService: AppCacheService,
        private _utilitiesService: UtilitiesService,
        private _tabService: AdTabService
    ) {
        this.selectedField = null;
        this.designerForm = new FormGroup({
            gridColumns: new FormControl('', Validators.required),
            field: new FormControl('', Validators.required),
            caption: new FormControl('', Validators.required),
            group: new FormControl(),
            span: new FormControl('', Validators.required),
            seqno: new FormControl(),
            gridSeqno: new FormControl(),
            guiCustomWidth: new FormControl(),
            displayed: new FormControl(),
            showingrid: new FormControl(),
            startnewrow: new FormControl(),
            readonly: new FormControl()
        });
        this.designerForm.get('gridColumns').valueChanges.pipe(
            debounceTime(this.delay),
            distinctUntilChanged()
        ).subscribe(data => {
            this.changeGridColumns(data);
        });
        this.designerForm.get('field').valueChanges.subscribe(data => {
            this.changeField(data);
        });
        this.designerForm.get('caption').valueChanges.pipe(
            debounceTime(this.delay),
            distinctUntilChanged()
        ).subscribe(data => {
            this.changeCaption(data);
        });
        this.designerForm.get('group').valueChanges.subscribe(data => {
            this.changeGroup(data);
        });
        this.designerForm.get('span').valueChanges.subscribe(data => {
            this.changeSpan(data);
        });
        this.designerForm.get('seqno').valueChanges.pipe(
            debounceTime(this.delay),
            distinctUntilChanged()
        ).subscribe(data => {
            this.changeSeqno(data);
        });
        this.designerForm.get('gridSeqno').valueChanges.subscribe(data => {
            this.changeGridSeqno(data);
        });
        this.designerForm.get('guiCustomWidth').valueChanges.subscribe(data => {
            this.changeGUICustomWidth(data);
        });
        this.designerForm.get('displayed').valueChanges.subscribe(data => {
            this.changeDisplayed(data);
        });
        this.designerForm.get('showingrid').valueChanges.subscribe(data => {
            this.changeShowingrid(data);
        });
        this.designerForm.get('startnewrow').valueChanges.subscribe(data => {
            this.changeStartnewrow(data);
        });
        this.designerForm.get('readonly').valueChanges.subscribe(data => {
            this.changeReadonly(data);
        });
        this.subscriptions.push(
            this._globalEventsService.selectedTabChange$.subscribe(data => {
                if (data.idTab === VINCOFW_CONFIG.TABS.TAB_FORM_DESIGNER) {
                    this.loadFields();
                }
            })
        );
    }

    ngOnInit(): void {
        this._utilitiesService.loadTableContent(
            UtilitiesService.TABLES.AdFieldGroup, {
                q: 'active=1'
            }, [{
                property: 'module.restPath',
                direction: SortOrder.ASC
            }, {
                property: 'name',
                direction: SortOrder.ASC
            }]
        ).then(response => {
            this.groups = [];
            let lastGroup = null;
            response.forEach(grp => {
                if (lastGroup === null || grp['idModule'] !== lastGroup.id) {
                    lastGroup = new AdFieldGroupViewModel(grp['idModule'], grp['moduleName']);
                    this.groups.push(lastGroup);
                }
                lastGroup.groups.push({
                    id: grp['idFieldGroup'],
                    name: grp['name'],
                    gridColumns: grp['gridColumns']
                });
            });
            // Reset form values
            this.ungroupedGridColumns = this.parentItem.gridColumns;
            this.designerForm.patchValue({ gridColumns: this.ungroupedGridColumns });
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscrition => subscrition.unsubscribe());
    }

    buildForm(): void {
        this.showFormFields = !isNullOrUndefined(this.fields)  && this.fields.length > 0;
        if (!this.showFormFields) {
            const value = {
                field: null,
                caption: '',
                group: null,
                span: '',
                seqno: '',
                gridSeqno: '',
                guiCustomWidth: '',
                displayed: false,
                showingrid: false,
                startnewrow: false,
                readonly: false
            };
            this.designerForm.patchValue(value);
            return;
        }
        this.designerModify = !isNullOrUndefined(this.fields.find(fld => fld.designerModify));
        if (this.selectedField) {
            // Build field groups
            const ungroupedFields: AdFieldDtoModel[] = [];
            this.fieldGroups = [];
            this.fields.forEach(field => {
                field.designerSelected = this.selectedField && field.idField === this.selectedField.idField;
                if (!isNullOrUndefined(field.idFieldGroup)) {
                    let fieldGroup = this.fieldGroups.find(fg => fg.id === field.idFieldGroup);
                    if (isNullOrUndefined(fieldGroup)) {
                        let grp;
                        this.groups.forEach(g => {
                            if (!grp) {
                                grp = g.groups.find(gg => gg.id === field.idFieldGroup);
                            }
                        });
                        if (grp) {
                            fieldGroup = new AdFieldGroupDesignerModel(grp.id, grp.name, grp.gridColumns);
                            this.fieldGroups.push(fieldGroup);
                        }
                    }
                    fieldGroup.fields.push(field);
                } else {
                    ungroupedFields.push(field);
                }
            });
            this.ungroupedFields = ungroupedFields.sort((f1, f2) => f1.seqno > f2.seqno ? 1 : -1);
            this.fieldGroups.forEach(grp => {
                grp.fields = grp.fields.sort((f1, f2) => f1.seqno > f2.seqno ? 1 : -1);
            });
        } else {
            const value = {
                field: this.fields[0].idField
            };
            this.designerForm.patchValue(value);
        }
    }

    changeGridColumns(selectedValue): void {
        this.ungroupedGridColumns = selectedValue;
        this.buildForm();
    }

    changeField(selectedValue): void {
        if (this.fields) {
            this.selectedField = this.fields.find(f => f.idField === selectedValue);
            if (this.selectedField) {
                // Update form values
                const value = {
                    caption: this.selectedField.caption,
                    group: this.selectedField.idFieldGroup,
                    span: this.selectedField.span,
                    seqno: this.selectedField.seqno,
                    gridSeqno: this.selectedField.gridSeqno,
                    guiCustomWidth: this.selectedField.guiCustomWidth,
                    displayed: this.selectedField.displayed,
                    showingrid: this.selectedField.showingrid,
                    startnewrow: this.selectedField.startnewrow,
                    readonly: this.selectedField.readonly,
                };
                this.designerForm.patchValue(value);
                this.buildForm();
            }
        }
    }

    changeCaption(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.caption !== selectedValue;
            this.selectedField.caption = selectedValue;
            this.buildForm();
        }
    }

    changeGroup(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.idFieldGroup !== selectedValue;
            this.selectedField.idFieldGroup = selectedValue;
            this.buildForm();
        }
    }

    changeSpan(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.span !== selectedValue;
            this.selectedField.span = selectedValue;
            this.buildForm();
        }
    }

    changeSeqno(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.seqno !== selectedValue;
            this.selectedField.seqno = selectedValue;
            this.buildForm();
        }
    }

    changeGridSeqno(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.gridSeqno !== selectedValue;
            this.selectedField.gridSeqno = selectedValue;
        }
    }

    changeGUICustomWidth(selectedValue): void {
        if (this.checkFieldSelected()) {
            if (selectedValue && selectedValue.replace(this.patternGUICustomWidth, '').length > 0) {
                this._notificationService.showError(this._cacheService.getTranslation('AD_ValidationErrorColumnGuiCustomWidth'));
            } else {
                this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.gridSeqno !== selectedValue;
                this.selectedField.guiCustomWidth = selectedValue;
            }
        }
    }

    changeDisplayed(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.displayed !== selectedValue;
            this.selectedField.displayed = selectedValue;
            this.buildForm();
        }
    }

    changeShowingrid(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.showingrid !== selectedValue;
            this.selectedField.showingrid = selectedValue;
        }
    }

    changeStartnewrow(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.startnewrow !== selectedValue;
            this.selectedField.startnewrow = selectedValue;
            this.buildForm();
        }
    }

    changeReadonly(selectedValue): void {
        if (this.checkFieldSelected()) {
            this.selectedField.designerModify = this.selectedField.designerModify || this.selectedField.readonly !== selectedValue;
            this.selectedField.readonly = selectedValue;
        }
    }

    doSelectedField(field: AdFieldDtoModel): void {
        const value = {
            field: field.idField
        };
        this.designerForm.patchValue(value);
        this.selectedField = field;
        this.buildForm();
    }

    doSave(): void {
        const itemToSave: AdDesignerInfoModel = {
            gridColumns: this.designerForm.value['gridColumns'],
            fields: []
        };
        this.fields.forEach(fld => {
            if (fld.designerModify) {
                itemToSave.fields.push({
                    idField: fld.idField,
                    caption: fld.caption,
                    idFieldGroup: fld.idFieldGroup,
                    span: fld.span,
                    seqno: fld.seqno,
                    gridSeqno: fld.gridSeqno,
                    guiCustomWidth: fld.guiCustomWidth,
                    displayed: fld.displayed,
                    showingrid: fld.showingrid,
                    startnewrow: fld.startnewrow,
                    readonly: fld.readonly
                });
            }
        });
        this._tabService.designerSave(this.parentItem.idTab, itemToSave).subscribe(() => {
            this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
            this.fields.forEach(fld => fld.designerModify = false);
            this.designerModify = !isNullOrUndefined(this.fields.find(fld => fld.designerModify));
        });
    }

    checkFieldSelected(): boolean {
        if (isNullOrUndefined(this.selectedField)) {
            this._notificationService.showWarning(this._cacheService.getTranslation('AD_DesignerSelectField'));
            return false;
        }
        return true;
    }

    private loadFields(): void {
        const reloadFields = isNullOrUndefined(this.fields) || isNullOrUndefined(this.fields.find(fld => fld.designerModify));
        if (reloadFields) {
            this._utilitiesService.loadTableContent(
                UtilitiesService.TABLES.AdField,
                { q: `idTab=${this.parentItem.idTab}`},
                [{ property: 'caption', direction: SortOrder.ASC }]
            ).then((data: AdFieldDtoModel[]) => {
                this.fields = data;
                this.fields.forEach(fld => {
                    fld.columnReferenceType = fld['column'].reference.rtype;
                    fld.designerModify = false;
                });
                if (this.fields.length > 0) {
                    this.doSelectedField(this.fields[0]);
                } else {
                    this.buildForm();
                }
            });
        }
    }

}
