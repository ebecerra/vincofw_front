import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AdFieldDtoModel} from '../../../models/dto/ad.field.dto.model';
import {AutogenFieldsComponent} from '../../autogen.fields/autogen.fields.component';
import {AdRowFieldModel} from '../../../models/ad.grid.model';

@Component({
  selector: 'form-designer-fields',
  templateUrl: './form.fields.component.html',
  styleUrls: ['./form.fields.component.scss']
})
export class FormFieldsComponent implements OnInit {

    private _fields: AdFieldDtoModel[];
    private _gridColumns: string;

    rows: AdRowFieldModel[];
    firstSeqno = 10;

    @Input()
    set fields(fields: AdFieldDtoModel[]) {
        this._fields = fields;
        if (this._gridColumns) {
            this.processForm();
        }
    }

    get fields(): AdFieldDtoModel[] {
        return this._fields;
    }

    @Input()
    set gridColumns(gridColumns: string) {
        this._gridColumns = gridColumns;
        if (this._fields) {
            this.processForm();
        }
    }

    get gridColumns(): string {
        return this._gridColumns;
    }

    @Output()
    public onSelect: EventEmitter<AdFieldDtoModel> = new EventEmitter();

    constructor() {
        this.rows = [];
    }

    ngOnInit(): void {
    }

    processForm(): void {
        if (this._gridColumns && this._fields) {
            this.rows = AutogenFieldsComponent.buildFormGrid(this._gridColumns, this._fields);
            this.firstSeqno = 1000000;
            this._fields.forEach(fld => {
                if (fld.displayed) {
                    if (fld.seqno && this.firstSeqno > fld.seqno) {
                        this.firstSeqno = fld.seqno;
                    }
                }
            });
            if (this.firstSeqno === 1000000) {
                this.firstSeqno = 10;
            }
        }
    }

    selectField(field: AdFieldDtoModel): void {
        this.onSelect.emit(field);
    }

    drop(event: CdkDragDrop<string[]>): void {
        console.log(`previousIndex: ${event.previousIndex}, currentIndex: ${event.currentIndex}`);
        moveItemInArray(this._fields, event.previousIndex, event.currentIndex);
        let indx = 0;
        this._fields.forEach(fld => {
            if (fld.displayed) {
                fld.designerModify = true;
                fld.seqno = this.firstSeqno + 10 * indx;
                indx++;
            }
        });
        this.processForm();
    }

}
