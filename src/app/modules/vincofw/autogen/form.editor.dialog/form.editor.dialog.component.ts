import {Component, Inject} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {isNullOrUndefined} from 'util';
import {AdMenuModel} from '../../models/ad.menu.model';
import {AdTabMainDtoModel} from '../../models/dto/ad.tab.main.dto.model';
import {AutogenEditElementsModel} from '../models/autogen.edit.elements.model';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AppCacheService} from '../../services/cache.service';
import {AutogenService} from '../../services/autogen.service';
import {ConditionalService} from '../../services/conditional.service';
import {AdHookNames, HookService} from '../../services/hook.service';

export interface FormEditorDialogData {
    rowkey: string;
    menu: AdMenuModel;
    field: AdFieldDtoModel;
    parentItem: any;
    item: any;
}

@Component({
    selector: 'form-editor-dialog',
    templateUrl: './form.editor.dialog.component.html',
    styleUrls: ['./form.editor.dialog.component.scss']
})
export class FormEditorDialogComponent {

    private _service: AutogenService;

    tab: AdTabMainDtoModel = null;
    selectedItems: AutogenEditElementsModel = null;
    newError: string = null;

    constructor(
        private _autogenService: AutogenService,
        private _cacheService: AppCacheService,
        private _conditionalService: ConditionalService,
        private _hookService: HookService,
        public dialogRef: MatDialogRef<FormEditorDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: FormEditorDialogData
    ) {
        this._cacheService.getWindowInfo(data.menu.idWindow).subscribe(window => {
            if (!isNullOrUndefined(window)) {
                this.tab = window.mainTab;
                if (data.rowkey) {
                    this._service = this._autogenService.bindedService(this.tab.tab.table);
                    let q = data.field.relatedColumn.reference.refTable.keyName + '=' + data.rowkey;
                    if (data.field.relatedColumn.reference.refTable.sqlwhere) {
                        const runtimeSqlwhere = this._conditionalService.replaceJSConditional(data.field.relatedColumn.reference.refTable.sqlwhere, null, null, data.parentItem);
                        q += ',$extended$={' + runtimeSqlwhere + '}';
                    }
                    let params = new HttpParams();
                    params = params.append('q', q);
                    this._service.list(params).subscribe(response => {
                        if (response.content.length > 0) {
                            this.selectedItems = {
                                items: [response.content[0]],
                                index: 0,
                                isNew: false
                            };
                        } else {
                            this.selectedItems = {
                                items: null,
                                index: 0,
                                isNew: true
                            };
                        }
                    });
                } else {
                    this._hookService.execHook(
                        AdHookNames.AD_CAN_NEW_ITEM, {
                            tab: this.tab.tab,
                            origin: 'FORM'
                        }
                    ).then(response => {
                        if (response.success) {
                            this.selectedItems = {
                                items: [data.item],
                                index: 0,
                                isNew: true
                            };
                        }
                    }).catch(() => {
                        this.dialogRef.close({ success: false } );
                    });
                }
            }
        });
    }

    doListItems(item): void {
        if (item) {
            this.dialogRef.close({ success: true, item: item } );
        } else {
            this.dialogRef.close({ success: false } );
        }
    }

    doClose(): void {
        this.dialogRef.close({ success: false } );
    }
}
