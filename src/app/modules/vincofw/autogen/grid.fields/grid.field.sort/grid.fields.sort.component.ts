import {Component, Input, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AdFieldDtoModel} from '../../../models/dto/ad.field.dto.model';

@Component({
  selector: 'grid-fields-sort',
  templateUrl: './grid.fields.sort.component.html',
  styleUrls: ['./grid.fields.sort.component.scss']
})
export class GridFieldsSortComponent implements OnInit {

    @Input() fields: AdFieldDtoModel[];

    constructor() {
    }

    ngOnInit(): void {
    }

    drop(event: CdkDragDrop<string[]>): void {
        console.log(`previousIndex: ${event.previousIndex}, currentIndex: ${event.currentIndex}`);
        moveItemInArray(this.fields, event.previousIndex, event.currentIndex);
    }

}
