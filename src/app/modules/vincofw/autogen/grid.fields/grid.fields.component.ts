import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet} from '@angular/material/bottom-sheet';
import {AdFieldDtoModel} from '../../models/dto/ad.field.dto.model';
import {AdFieldGridService} from '../../services/ad.field.grid.service';
import {AppCacheService} from '../../services/cache.service';
import {AuthService} from '../../authentication/auth.service';
import {NotificationService} from '../../notification/notification.service';
import {AppCoreSettings} from '../../configs/app.core.settings';

export interface GridFieldsDialogData {
    idTab: string;
    fields: AdFieldDtoModel[];
    buttonType: string;
}

@Component({
    selector: 'grid-fields',
    templateUrl: './grid.fields.component.html',
    styleUrls: ['./grid.fields.component.scss']
})
export class GridFieldsComponent implements OnInit {

    idTab: string;
    fields: AdFieldDtoModel[];
    buttonType: string;
    properties: any = null;

    constructor(
        private _authService: AuthService,
        private _coreSettings: AppCoreSettings,
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _fieldGridService: AdFieldGridService,
        private _bottomSheet: MatBottomSheet,
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: GridFieldsDialogData
    ) {
        this.idTab = data.idTab;
        this.buttonType = data.buttonType;
        this.fields = [];
        this.data.fields.forEach(fld => {
            if (!fld.showingrid) {
                fld.gridSeqno = this._coreSettings.MAX_SEQNO;
            }
        });
        this.fields = data.fields.sort((f1, f2) => f1.gridSeqno > f2.gridSeqno ? 1 : -1);
    }

    ngOnInit(): void {

    }

    doAccept(): void {
        const fieldIds = this.fields.filter(fld => fld.showingrid).map(fld => fld.idField);
        this._fieldGridService.saveColumns(this.idTab, this._authService.getLoggedIdUser(), fieldIds.join(';')).subscribe(resp => {
            if (resp.success) {
                this._bottomSheet.dismiss();
            } else {
                this._notificationService.showError(this._cacheService.getTranslation(resp.message));
            }
        });
    }

    doRestore(): void {
        this._fieldGridService.deleteColumns(this.idTab, this._authService.getLoggedIdUser()).subscribe(resp => {
            this._bottomSheet.dismiss();
        });
    }

}
