import {Component, Input, OnInit} from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FlatTreeControl} from '@angular/cdk/tree';
import {SortOrder} from '../../models/ad.base.model';
import {AdMenuModel} from '../../models/ad.menu.model';
import {AdGUIDtoModel} from '../../models/dto/ad.gui.dto.model';
import {AutogenSelectValueModel} from '../models/autogen.select.value.model';
import {AppCacheService} from '../../services/cache.service';
import {AdMenuService} from '../../services/ad.menu.service';
import {UtilitiesService} from '../../services/utilities.service';
import {AdMenuRolesService} from '../../services/ad.menu.roles.service';
import {NotificationService} from '../../notification/notification.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

interface MenuNode {
    id: string;
    idModule: string;
    name: string;
    icon: string;
    children?: MenuNode[];
}

interface MenuFlatNode {
    id: string;
    idModule: string;
    name: string;
    icon: string;
    expandable: boolean;
    level: number;
}

@Component({
    selector: 'menu-role',
    templateUrl: './menu.role.component.html',
    styleUrls: ['./menu.role.component.scss']
})
export class MenuRoleComponent implements OnInit {

    menuRoleForm: FormGroup;
    loading: boolean;
    modules: AutogenSelectValueModel[];
    menuList: AdMenuModel[];
    changedNodes: MenuFlatNode[] = [];

    treeControl: FlatTreeControl<MenuFlatNode>;
    treeFlattener: MatTreeFlattener<any, any>;

    dataSource: MatTreeFlatDataSource<any, any>;

    @Input() parentItem: any;
    @Input() gui: AdGUIDtoModel;

    constructor(
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _menuService: AdMenuService,
        private _utilitiesService: UtilitiesService,
        private _menuRolesService: AdMenuRolesService
    ) {
        this.treeControl = new FlatTreeControl<MenuFlatNode>(node => node.level, node => node.expandable);
        this.treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

        this.menuRoleForm = new FormGroup({
            sqlType: new FormControl('SQL', Validators.required),
            sqlQuery: new FormControl('', Validators.required)
        });
    }

    private _transformer = (node: MenuNode, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            id: level > 0 ? node.id : null,
            idModule: node.idModule,
            name: node.name,
            icon: node.icon,
            level: level,
        };
    }

    ngOnInit(): void {
        this.loading = true;
        const sortOrder = { property: 'name', direction: SortOrder.ASC};
        this._utilitiesService.loadTableContent(VINCOFW_CONFIG.TABLES.AD_MODULE, { active: true }, [sortOrder]).then(data => {
            this.modules = [];
            const menuTree: MenuNode[] = [];
            data.forEach((module: any) => {
                this.modules.push({ value: module.idModule, caption: module.name});
                menuTree.push({
                    id: module.idModule,
                    idModule: null,
                    icon: null,
                    name: module.name,
                });
            });
            this._menuService.allMenu(this.parentItem.idRole).subscribe(response => {
                this.menuList = response;
                menuTree.forEach(node => {
                    node.children = this.buildTree(node.id, response);
                });
                this.dataSource.data = menuTree.filter(node => node.children !== null);
                this.loading = false;
            });
        });
    }

    hasChild = (_: number, node: MenuFlatNode) => node.expandable;

    doSave(): void {
        const menu = [];
        this.changedNodes.forEach(node => {
            const item = this.getMenuItem(node.id, this.menuList);
            if (item.idModule !== node.idModule) {
                menu.push({
                    idMenu: node.id,
                    idModule: node.idModule,
                    selected: node.idModule != null
                });
            }
        });
        if (menu.length === 0) {
            this._notificationService.showWarning(this._cacheService.getTranslation('AD_MenuRoleSaveNotChanged'));
        } else {
            const dialogRef = this._notificationService.confirmDialog(
                this._cacheService.getTranslation('AD_GlobalConfirm'),
                this._cacheService.getTranslation('AD_MenuRoleSave'),
                'warning', 'confirm-icon-warn'
            );
            dialogRef.afterClosed().subscribe(result => {
                if (result === 'OK') {
                    const params = {
                        idRole: this.parentItem.idRole,
                        menu: menu
                    };
                    this._menuRolesService.saveMenu(params).subscribe(response => {
                        if (response.success) {
                            if (response.properties.inserted > 0 || response.properties.deleted > 0) {
                                this._notificationService.showSuccess(
                                    this._cacheService.getTranslation('AD_MenuRoleSaveSuccess', [response.properties.inserted, response.properties.deleted])
                                );
                            } else {
                                this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
                            }
                            this.changedNodes = [];
                        }
                    });
                }
            });
        }
    }

    changeModule(node: MenuFlatNode): void {
        if (!this.changedNodes.find(n => n.id === node.id)) {
            this.changedNodes.push(node);
        }
    }

    private getMenuItem(idMenu, submenus: AdMenuModel[]): AdMenuModel {
        let result = null;
        submenus.forEach(menu => {
            if (menu.idMenu === idMenu) {
                result = menu;
            }
            if (!result && menu.subMenus) {
                result = this.getMenuItem(idMenu, menu.subMenus);
            }
        });
        return result;
    }

    private buildTree(idModule: string, submenus: AdMenuModel[]): MenuNode[] {
        const nodes: MenuNode[] = [];
        submenus.forEach(menu => {
            if (menu.idModuleMenu === idModule) {
                const node: MenuNode = {
                    id: menu.idMenu,
                    idModule: menu.idModule,
                    icon: menu.icon,
                    name: menu.name
                };
                if (menu.subMenus && menu.subMenus.length > 0) {
                    node.children = this.buildTree(idModule, menu.subMenus);
                }
                nodes.push(node);
            }
        });
        return nodes.length > 0 ? nodes : null;
    }
}
