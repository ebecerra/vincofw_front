import {AutogenSelectValueModel} from './autogen.select.value.model';

export class AutogenSelectGroupModel {
    id: string;
    name: string;
    values: AutogenSelectValueModel[];
}
