interface AutogenTemplateContextModel {
    templContext: {
        label: string;
        name: string;
        description: string;
        hint: string;
    };
}
