import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AdProcessDtoModel} from '../../models/dto/ad.process.dto.model';
import {AdBaseModel} from '../../models/ad.base.model';

export interface ProcessDialogData {
    process: AdProcessDtoModel;
    buttonType: string;
    item: AdBaseModel;
}

@Component({
  selector: 'process-dialog',
  templateUrl: './process.dialog.component.html',
  styleUrls: ['./process.dialog.component.scss']
})
export class ProcessDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ProcessDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ProcessDialogData
    ) { }

    ngOnInit(): void {
    }

    doCancel(): void {
        this.dialogRef.close('CANCEL');
    }

}
