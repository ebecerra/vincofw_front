import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {AdGUIDtoModel} from '../../models/dto/ad.gui.dto.model';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {AutogenSelectValueModel} from '../models/autogen.select.value.model';
import {AdUserModel} from '../../models/ad.user.model';
import {AppCacheService} from '../../services/cache.service';
import {AdPrivilegeService} from '../../services/ad.privilege.service';
import {AdProcessService} from '../../services/ad.process.service';
import {AppCoreSettings} from '../../configs/app.core.settings';
import {AuthService} from '../../authentication/auth.service';
import {NotificationService} from '../../notification/notification.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

import * as moment_ from 'moment';

const moment = moment_;

@Component({
    selector: 'process-planning',
    templateUrl: './process.planning.component.html',
    styleUrls: ['./process.planning.component.scss']
})
export class ProcessPlanningComponent implements OnInit {

    constructor(
        private _authService: AuthService,
        private _coreSettings: AppCoreSettings,
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _privilegeService: AdPrivilegeService,
        private _processService: AdProcessService
    ) {
        this.planningForm = new FormGroup({
            idClient: new FormControl('0', Validators.required),
            idUser: new FormControl('', Validators.required),
            executionType: new FormControl('DAILY', Validators.required),
            oneExecDay: new FormControl(moment().format('DD/MM/YYYY'), Validators.required),
            weekDay: new FormControl('MON'),
            monthDay: new FormControl('1'),
            hour: new FormControl('00:00', Validators.required),
            minutes: new FormControl('5'),
            hours: new FormControl('0'),
            MON: new FormControl(true),
            TUE: new FormControl(true),
            WED: new FormControl(true),
            THU: new FormControl(true),
            FRI: new FormControl(true),
            SAT: new FormControl(true),
            SUN: new FormControl(true)
        });
        this.currentExecutionType = 'DAILY';
        this.planningForm.valueChanges.subscribe(value => {
            this.currentExecutionType = value['executionType'];
            this.setPlanningQuartz(this.buildCronDef(value));
            if (this.currentExecutionType === 'FREQUENTLY') {
                this.mustOneZero = (value['minutes'] !== '0' && value['hours'] !== '0') || (value['minutes'] === '0' && value['hours'] === '0');
                this.zeroMessage = this._cacheService.getTranslation(
                    value['minutes'] === '0' && value['hours'] === '0' ? 'AD_processPlanningFieldNotAllZero' : 'AD_processPlanningFieldOneZero'
                );
            }
        });
    }

    planningForm: FormGroup;
    referenceClient: AdReferenceDtoModel = null;
    referenceUser: AdReferenceDtoModel = null;
    executionType: AdReferenceDtoModel = null;
    executionDays: AdReferenceDtoModel = null;
    users: AdUserModel[];
    monthDays: AutogenSelectValueModel[];
    hourMinutes: AutogenSelectValueModel[];
    dayHours: AutogenSelectValueModel[];
    currentExecutionType: string;
    planningQuartz: string;
    loading: boolean;
    mustOneZero = false;
    zeroMessage: string;

    @Input() parentItem: any;
    @Input() gui: AdGUIDtoModel;

    /**
     * Fill left with zero
     *
     * @param splitted Values
     * @param from From index
     * @param to To index
     */
    private static leftZeroFill(splitted: string[], from: number, to: number): void {
        for (let i = from; i <= to; i++) {
            splitted[i] = splitted[i].length === 2 ? splitted[i] : '0' + splitted[i];
        }
    }

    ngOnInit(): void {
        this.loading = true;
        this._cacheService.getReferenceInfo(
            [
                VINCOFW_CONFIG.REFERENCES.TABLEDIR_CLIENT, VINCOFW_CONFIG.REFERENCES.TABLE_USER,
                VINCOFW_CONFIG.REFERENCES.LIST_BACKGROUND_EXECUTION_TYPE, VINCOFW_CONFIG.REFERENCES.LIST_BACKGROUND_EXECUTION_DAYS
            ]
        ).subscribe(references => {
            this.referenceClient = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.TABLEDIR_CLIENT);
            this.referenceUser = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.TABLE_USER);
            this.executionType = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.LIST_BACKGROUND_EXECUTION_TYPE);
            this.executionDays = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.LIST_BACKGROUND_EXECUTION_DAYS);
            if (this.referenceUser) {
                this.referenceUser.autoLoadValues = false;
            }
            this.setPlanningQuartz(this.parentItem.cronExpression);
            if (this.parentItem.cronExpression) {
                this.reverseCronDef(this.parentItem.cronExpression);
                this.planningForm.get('idClient').setValue(this.parentItem.executingIdClient);
                this.planningForm.get('idUser').setValue(this.parentItem.executingIdUser);
            }
            this.loading = false;
        });
        this._privilegeService.users('ROLE_BACKGROUND_PROCESS').subscribe(response => {
            this.users = response;
        });
        this.monthDays = Array(31).fill({}).map((x, i) => {
            return { value: '' + (i + 1), caption: '' + (i + 1) };
        });
        this.dayHours = Array(24).fill({}).map((x, i) => {
            return { value: '' + i, caption: '' + i };
        });
        this.hourMinutes = Array(60).fill({}).map((x, i) => {
            return { value: '' + i, caption: '' + i };
        });
    }

    doSave(): void {
        this.planningForm.updateValueAndValidity();
        Object.keys(this.planningForm.controls).forEach(key => {
            this.planningForm.controls[key].markAsTouched();
        });
        if (this.planningForm.valid && !this.mustOneZero) {
            this._processService.schedule(
                this.parentItem.idProcess, this.planningForm.value['idClient'],
                this.planningForm.value['idUser'], this.buildCronDef(this.planningForm.value)
            ).subscribe(response => {
                if (response.success) {
                    this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgDataSave'));
                } else {
                    this._notificationService.showError(this._cacheService.getTranslation(response.message));
                }
            });
        }
    }

    doRemove(): void {
        const dialogRef = this._notificationService.questionDialog(
            this._cacheService.getTranslation('AD_GlobalConfirm'),
            this._cacheService.getTranslation('AD_msgConfirmClearSchedule'),
            'warning', 'confirm-icon-warn'
        );
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'YES') {
                this._processService.clearSchedule(this.parentItem.idProcess).subscribe(() => {
                    this._notificationService.showSuccess(this._cacheService.getTranslation('AD_ClearScheduleSuccess'));
                });
            }
        });
    }

    /**
     * Set planning quart expression
     *
     * @param value
     */
    private setPlanningQuartz(value): void {
        this.planningQuartz = this._cacheService.getTranslation('AD_processPlanningCurrentCronDef') + ': <b>' + (value ? value : '') + '</b>';
    }

    /**
     * Reverting cron expression to show corresponding visual values
     *
     * @param cronExpression: Quartz cron expression
     */
    private reverseCronDef(cronExpression: string): void {
        if (cronExpression === null || cronExpression === undefined) {
            return;
        }
        const splitted = cronExpression.split(' ');
        if (new RegExp('^(.+[ ]){6}.+').test(cronExpression)) {
            this.parseOneExec(splitted);
        } else if (new RegExp('^(\\d+[ ]){4}\\*[ ]\\?').test(cronExpression)) {
            this.parseMonthly(splitted);
        } else if (new RegExp('^(\\d+[ ]){3}\\?[ ]\\*[ ]\\w{3}$').test(cronExpression)) {
            this.parseWeekly(splitted);
        } else if (new RegExp('^(\\d+[ ]){3}\\?[ ]\\*[ ]\\w{3}.+').test(cronExpression)) {
            this.parseDaily(splitted);
        } else {
            this.parseFrequently(splitted);
        }
    }

    /**
     * Parsing cron definition for ONE_EXEC type to be able to show the corresponding visual values
     *
     * @param splitted Splitted cron definition
     */
    private parseOneExec(splitted: string[]): void {
        this.planningForm.get('executionType').setValue('ONE_EXEC');
        ProcessPlanningComponent.leftZeroFill(splitted, 1, 4);
        this.planningForm.get('oneExecDay').setValue(moment(splitted[3] + splitted[4] + splitted[6], 'DDMMYYYY'));
        this.planningForm.get('hour').setValue(splitted[2] + ':' + splitted[1]);
    }

    /**
     * Parsing cron definition for MONTHLY type to be able to show the corresponding visual values
     *
     * @param splitted Splitted cron definition
     */
    private parseMonthly(splitted: string[]): void {
        this.planningForm.get('executionType').setValue('MONTHLY');
        ProcessPlanningComponent.leftZeroFill(splitted, 1, 4);
        this.planningForm.get('monthDay').setValue(splitted[3]);
        this.planningForm.get('hour').setValue(splitted[2] + ':' + splitted[1]);
    }

    /**
     * Parsing cron definition for WEEKLY type to be able to show the corresponding visual values
     *
     * @param splitted Splitted cron definition
     */
    private parseWeekly(splitted: string[]): void {
        this.planningForm.get('executionType').setValue('WEEKLY');
        ProcessPlanningComponent.leftZeroFill(splitted, 1, 2);
        this.planningForm.get('hour').setValue(splitted[2] + ':' + splitted[1]);
        this.planningForm.get('weekDay').setValue(splitted[5]);
    }

    /**
     * Parsing cron definition for DAILY type to be able to show the corresponding visual values
     *
     * @param splitted Splitted cron definition
     */
    private parseDaily(splitted: string[]): void {
        this.planningForm.get('executionType').setValue('DAILY');
        ProcessPlanningComponent.leftZeroFill(splitted, 1, 2);
        this.planningForm.get('hour').setValue(splitted[2] + ':' + splitted[1]);
        const days = splitted[5].split(',');
        this.executionDays.refList.forEach((day, index) => {
            this.planningForm.get(day.value).setValue(!isNullOrUndefined(days.find(d => d === day.value)));
        });
    }

    /**
     * Parsing cron definition for FREQUENTLY type to be able to show the corresponding visual values
     *
     * @param splitted Splitted cron definition
     */
    private parseFrequently(splitted: string[]): void {
        this.planningForm.get('executionType').setValue('FREQUENTLY');
        if (splitted[2] === '*') {
            const minutes = splitted[1].split('/');
            this.planningForm.get('minutes').setValue(minutes[1]);
            this.planningForm.get('hours').setValue('0');
        } else if (splitted[1] === '*') {
            const hours = splitted[2].split('/');
            this.planningForm.get('minutes').setValue('0');
            this.planningForm.get('hours').setValue(hours[1]);
        }
    }

    /**
     * Assembles a cron expression from the input
     *
     * @param value: Form values
     * @returns {string} Cron expression
     */
    private buildCronDef(value: any): string {
        const hourDefinition = this.getHourDef(value['hour']);
        let cronDef = '0';
        cronDef += (' ' + hourDefinition.minute);
        cronDef += (' ' + hourDefinition.hour);
        switch (value['executionType']) {
            case 'DAILY':
                cronDef += ' ? *';
                const days = this.executionDays.refList.filter(d => value[d.value]);
                let cronDay = '';
                days.forEach((current, index) => {
                    const comma = (index + 1) !== days.length ? ',' : '';
                    cronDay += (current.value + comma);
                });
                cronDef += (cronDay !== '' ? (' ' + cronDay) : ' *');
                break;

            case 'WEEKLY':
                cronDef += ' ? *';
                if (value['weekDay']) {
                    cronDef += (' ' + value['weekDay']);
                } else {
                    cronDef += (' *');
                }
                break;

            case 'MONTHLY':
                cronDef += (' ' + value['monthDay']);
                cronDef += ' * ?';
                break;

            case 'ONE_EXEC':
                const day = moment(value['oneExecDay'], 'DD/MM/YYYY').date();
                const month = moment(value['oneExecDay'], 'DD/MM/YYYY').month() + 1;
                const year = moment(value['oneExecDay'], 'DD/MM/YYYY').year();
                cronDef += ' ' + day + ' ' + month + ' ?' + ' ' + year;
                break;

            case 'FREQUENTLY':
                cronDef = '0';
                if (value['minutes'] > 0) {
                    cronDef += (' ' + '0/' + value['minutes'] + ' * * * ?');
                }
                else {
                    cronDef += (' * ' + '0/' + value['hours'] + ' * * ?');
                }
                break;
        }
        return cronDef;
    }

    /**
     * Returns the splitted hour and minute
     *
     * @param hour: Hour value
     * @returns {*}
     */
    private getHourDef(hour: string): any {
        if (moment(hour).isValid()) {
            // We are dealing with a valid date object, so we are just going to retrieve the hour from it
            return {
                hour: moment(hour).hour(),
                minute: moment(hour).minute()
            };
        } else {
            const regex = new RegExp('^(\\d{2})\\:\\d{2}$');
            if (regex.test(hour)) {
                const splitted = hour.split(':');
                return {
                    hour: Number(splitted[0]).toString(),
                    minute: Number(splitted[1]).toString()
                };
            }
            return {
                hour: 0,
                minute: 0
            };
        }
    }

}
