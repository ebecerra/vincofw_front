import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {AppCacheService} from '../../services/cache.service';
import {AdSqlService} from '../../services/ad.sql.service';
import {NotificationService} from '../../notification/notification.service';
import {VINCOFW_CONFIG} from '../../vincofw.constants';

@Component({
    selector: 'query-exec',
    templateUrl: './query.exec.component.html',
    styleUrls: ['./query.exec.component.scss']
})
export class QueryExecComponent implements OnInit {

    queryForm: FormGroup;
    queryLanguages: AdReferenceDtoModel = null;
    loading: boolean;
    values: any[];
    columns: any[];
    mode = 'SQL';

    constructor(
        private _notificationService: NotificationService,
        private _cacheService: AppCacheService,
        private _sqlService: AdSqlService
    ) {
        this.queryForm = new FormGroup({
            sqlType: new FormControl('SQL', Validators.required),
            sqlQuery: new FormControl('', Validators.required)
        });
    }

    ngOnInit(): void {
        this.loading = true;
        this._cacheService.getReferenceInfo([VINCOFW_CONFIG.REFERENCES.LIST_QUERY_LANGUAGES]).subscribe(references => {
            this.queryLanguages = references.find(ref => ref.idReference === VINCOFW_CONFIG.REFERENCES.LIST_QUERY_LANGUAGES);
            this.loading = false;
        });
    }

    doExecute(): void {
        this.queryForm.updateValueAndValidity();
        Object.keys(this.queryForm.controls).forEach(key => {
            this.queryForm.controls[key].markAsTouched();
        });
        if (this.queryForm.valid) {
            this.values = [];
            this.mode = this.queryForm.value['sqlType'];
            this.loading = true;
            this._sqlService.exec(this.queryForm.value).subscribe(
                response => {
                    this.loading = false;
                    if (response.success) {
                        this._notificationService.showSuccess(this._cacheService.getTranslation('AD_msgSqlQueryExecuted'));
                        this.values = response.properties.resultData;
                        if (this.mode === 'SQL' && this.values.length > 0) {
                            this.columns = Object.keys(response.properties.resultData[0]);
                        } else {

                        }
                    } else {
                        this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationUnknow'));
                    }
                },
                error => {
                    this.loading = false;
                    if (error.error && error.error.objectErrors) {
                        error.error.objectErrors.forEach(current => {
                            this._notificationService.showError(this._cacheService.getTranslation(current.message));
                        });
                    } else {
                        this._notificationService.showError(this._cacheService.getTranslation('AD_ErrValidationUnknow'));
                    }
                }
            );                    
        }
    }

}
