import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {AdReferenceDtoModel} from '../../models/dto/ad.reference.dto.model';
import {AdLanguageModel} from '../../models/ad.language.model';
import {AppCacheService} from '../../services/cache.service';
import {AdTranslationService} from '../../services/ad.translation.service';
import {AdLanguageService} from '../../services/ad.language.service';
import {AuthService} from '../../authentication/auth.service';

export interface TranslationDialogData {
    idTable: string;
    idColumn: string;
    rowkey: string;
    editType: string;
    buttonType: string;
}

@Component({
  selector: 'process-dialog',
  templateUrl: './translation.dialog.component.html',
  styleUrls: ['./translation.dialog.component.scss']
})
export class TranslationDialogComponent implements OnInit {

    public ckEditor = ClassicEditor;
    translationForm: FormGroup;
    reference: AdReferenceDtoModel;
    languages: AdLanguageModel[];
    idTable: string;
    idColumn: string;
    rowkey: string;
    editType: string;
    buttonType: string;
    admin: boolean;

    constructor(
        private _authService: AuthService,
        private _cacheService: AppCacheService,
        private _languageService: AdLanguageService,
        private _translationService: AdTranslationService,
        public dialogRef: MatDialogRef<TranslationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: TranslationDialogData
    ) {
        this.admin = this._authService.hasPrivilege('ROLE_SUPERADMIN');
        this.idTable = data.idTable;
        this.idColumn = data.idColumn;
        this.rowkey = data.rowkey;
        this.editType = data.editType;
        this.buttonType = data.buttonType || 'mat-raised-button';
        this._cacheService.getReferenceInfo(['A05ACA9B2E1A435CA2829CD738279512']).subscribe(response => {
            this.reference = response[0];
            const idClient = this._authService.getLoggedIdClient();
            this._languageService.getClientLanguages(idClient).subscribe(result => {
                this.languages = result;
                const formElements: any = {};
                if (this.admin) {
                    formElements.idClient = new FormControl('0', Validators.required);
                }
                this.languages.forEach(lang => {
                    formElements[lang.iso2] = new FormControl();
                });
                this.translationForm = new FormGroup(formElements);
                this.doChangeClient(this.admin ? '0' : idClient);
                if (this.admin) {
                    this.translationForm.get('idClient').valueChanges.subscribe(value => {
                        this.doChangeClient(value);
                    });
                }
            });
        });
    }

    ngOnInit(): void {
    }

    doAccept(): void {
        const itemToSave = {
            idClient: this.admin ? this.translationForm.value['idClient'] : this._authService.getLoggedIdClient(),
            idUser: this._authService.getLoggedIdUser(),
            idTable: this.idTable,
            idColumn: this.idColumn,
            rowkey: this.rowkey,
            translations: []
        };
        this.languages.forEach(lang => {
            itemToSave.translations.push({ idLanguage: lang.idLanguage, translation: this.translationForm.value[lang.iso2] });
        });
        this._translationService.saveTranslations(itemToSave).subscribe(() => {
            this.dialogRef.close('OK');
        });
    }

    doCancel(): void {
        this.dialogRef.close('CANCEL');
    }

    doChangeClient(idClient: string): void {
        this._translationService.getTranslations(idClient, this.idTable, this.idColumn, this.rowkey).subscribe(response => {
            const value = {};
            this.languages.forEach(lang => {
                value[lang.iso2] = null;
            });
            response.forEach(trans => {
                value[trans.language.iso2] = trans.translation;
            });
            this.translationForm.patchValue(value);
        });
    }
}
