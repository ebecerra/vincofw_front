import {Component, Inject} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';

@Component({
  selector: 'vinco-help-show',
  templateUrl: './help.show.component.html',
  styleUrls: ['./help.show.component.scss']
})
export class HelpShowComponent {

    constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    }

}
