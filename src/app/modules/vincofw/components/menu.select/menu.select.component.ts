import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

@Component({
  selector: 'menu-select',
  templateUrl: './menu.select.component.html',
  styleUrls: ['./menu.select.component.scss']
})
export class MenuSelectComponent implements OnInit, OnDestroy {

    @Input() menuItems: MenuSelectItemModel[];
    @Input() icons = false;
    @Input() images = false;
    @Input() btnImageClass = '';
    @Input() itemImageClass = '';
    @Input() activeItem: MenuSelectItemModel = null;

    @Output() onSelect: EventEmitter<MenuSelectItemModel> = new EventEmitter();

    constructor() {
    }

    /**
     * On init
     */
    ngOnInit(): void {
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
    }

    selectItem(item): void {
        this.activeItem = item;
        this.onSelect.emit(item);
    }
}

export class MenuSelectItemModel {
    id: string;
    name: string;
    icon?: string;
    imageCss?: string;
}
