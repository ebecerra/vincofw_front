import {Component, Input} from '@angular/core';

@Component({
  selector: 'vinco-button',
  templateUrl: './vinco.button.component.html',
  styleUrls: ['./vinco.button.component.scss']
})
export class VincoButtonComponent {

    @Input() buttonType = 'mat-raised-button';
    @Input() caption = '';
    @Input() cssClass: string;
    @Input() color: string;
    @Input() icon: string = null;

    constructor() {
    }

}
