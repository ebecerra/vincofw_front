import {Injectable} from '@angular/core';
import {BaseSettingsModel} from '../models/base.settings.models';

@Injectable()
export class AppCoreSettings {

    public MAX_SEQNO = 1000000000;

    constructor(public settings: BaseSettingsModel) {
        console.log('AppCoreSettings constructor ...');
    }

    getRestHostPath(): string {
        return this.settings.REST_HOST_PATH;
    }

    getRestHostCore(): string {
        return this.settings.REST_HOST_CORE;
    }

    getRestHostLogin(): string {
        return this.settings.REST_HOST_LOGIN;
    }

    get idClient(): string {
        return this.settings.ID_CLIENT;
    }

    set idClient(idClient: string) {
        this.settings.ID_CLIENT = idClient;
    }

    getAppName(): string {
        return this.settings.APP_NAME;
    }

    setAppName(appName: string): void {
        this.settings.APP_NAME = appName;
    }

    get secretKey(): string {
        return this.settings.SECRET_KEY;
    }

    get appLogoUrl(): string {
        return this.settings.APPLICATION_LOGO_URL;
    }

    set appLogoUrl(appLogoUrl: string) {
        this.settings.APPLICATION_LOGO_URL = appLogoUrl;
    }

    get idLanguage(): string {
        return this.settings.ID_LANGUAGE;
    }

    setItem(key: string, value: string): void {
        localStorage.setItem(this.getAppName() + '_' + key, value);
    }

    removeItem(key: string): void {
        localStorage.removeItem(this.getAppName() + '_' + key);
    }

    getItem(key: string): string {
        return localStorage.getItem(this.getAppName() + '_' + key);
    }
}
