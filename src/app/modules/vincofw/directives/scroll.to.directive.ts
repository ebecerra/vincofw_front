import {Directive, HostListener, Input} from '@angular/core';
import {isNullOrUndefined} from 'util';

/**
 * Scrolls the browser window so the selected component will be visible
 */
@Directive({
    selector: '[scrollTo]'
})
export class ScrollToDirective{

    /**
     * Component selector to scroll to
     */
    @Input() componentSelector: string;

    constructor() {
    }

    @HostListener('click', ['$event']) onClick($event): void {
        const element = document.querySelector(this.componentSelector);
        if (!isNullOrUndefined(element)) {
            element.scrollIntoView();
        }
    }
}
