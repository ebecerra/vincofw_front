export class AdBaseModel {
    id: string;
    idClient: string;
    active: boolean;
    created: Date;
    updated: Date;
}

export class PagedContentModel<T> {
    size: number = 0;
    number: number = 0;
    totalPages: number = 0;
    firstPage: boolean = true;
    totalElements: number = 0;
    lastPage: boolean = true;
    numberOfElements: number = 0;
    content: T[] = [];
}

export class SortData {
    property: string;
    direction: SortOrder;
}

export enum SortOrder { ASC, DESC}

