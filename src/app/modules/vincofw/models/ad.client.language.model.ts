import {AdBaseModel} from './ad.base.model';
import {AdLanguageModel} from './ad.language.model';

export class AdClientLanguageModel extends AdBaseModel {
    idClientLanguage: string;
    language: AdLanguageModel;
}

