import {AdBaseModel} from './ad.base.model';

export class AdClientModel extends AdBaseModel{
    idModule: string;
    name: string;
    description: string;
    nick: string;
    email: string;
    smtpHost: string;
    smtpPort: number;
    smtpSsl: boolean;
    smtpUser: string;
    smtpPassword: string;
    guiTableType: string;
    guiTableMode: string;
    guiButton: string;
    guiFormField: string;
    guiFilterMode: string;
    guiFilterApply: string;
    guiColorTheme: string;
    guiWidth: string;
    toolbarPosition: string;
    toolbarColor: string;
    toolbarColorVariant: string;
    footerHidden: boolean;
    footerPosition: string;
    footerColor: string;
    footerColorVariant: string;
    navbarPosition: string;
    navbarPrimaryColor: string;
    navbarPrimaryColorVariant: string;
    navbarSecondaryColor: string;
    navbarSecondaryColorVariant: string;
}

