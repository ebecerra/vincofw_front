export class AdDesignerFieldModel {
    idField: string;
    caption: string;
    idFieldGroup: string;
    span: number;
    seqno: number;
    gridSeqno: number;
    guiCustomWidth: string;
    displayed: boolean;
    showingrid: boolean;
    startnewrow: boolean;
    readonly: boolean;
}

export class AdDesignerInfoModel {
    gridColumns: string;
    fields: AdDesignerFieldModel[];
}
