import {AdBaseModel} from './ad.base.model';

export class AdFileModel extends AdBaseModel {
    idFile: string;
    caption: string;
    description: string;
    filesize: number;
    mimetype: string;
    category: string;
    fileUrl: string;
    extension: string;
    seqno: number;
}

