export class AdMultiSelectValueModel {
    id: string;
    name: string;
    selected: boolean;
}
