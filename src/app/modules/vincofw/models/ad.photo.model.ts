import {AdBaseModel} from './ad.base.model';

export class AdPhotoModel extends AdBaseModel {
    idImage: string;
    idTable: string;
    idRow: string;
    name: string;
    description: string;
    itype: string;
    url: string;
    tableName: string;
}
