export class AdProcessLogModel {
    idProcess: string;
    idProcessExec: string;
    eventType: string;
    logType: string;
    date: Date;
    info: string;
    outParamName: string;
    outParamValue: any;
}
