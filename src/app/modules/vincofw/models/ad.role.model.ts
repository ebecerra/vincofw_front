import {AdBaseModel} from './ad.base.model';
import {AdPrivilegeModel} from './ad.privilege.model';

export class AdRoleModel extends AdBaseModel  {
    idRole: string;
    name: string;
    description: string;
    rtype: string;
    privileges: AdPrivilegeModel[];
}
