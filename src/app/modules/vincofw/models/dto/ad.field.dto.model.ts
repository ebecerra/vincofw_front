import {AdColumnDtoModel} from './ad.column.dto.model';
import {AdReferenceKeyValueModel} from './ad.reference.key.value.model';
import {AdRowColumnModel} from '../ad.grid.model';

export class AdFieldDtoModel extends AdRowColumnModel {
    idField: string;
    idColumn: string;
    idFieldGroup?: string;
    caption: string;
    placeholder?: string;
    hint?: string;
    filterDefaultValue?: string;
    filterDefaultValue2?: string;
    seqno: number;
    gridSeqno?: number;
    readonly: boolean;
    showingrid: boolean;
    showinstatusbar: boolean;
    usedInChild: boolean;
    onchangefunction?: string;
    displaylogic?: string;
    filterRange: boolean;
    filterType: string;
    sortable: boolean;
    cssClass?: string;
    multiSelect?: boolean;
    valuemin?: string;
    valuemax?: string;
    valuedefault?: string;
    guiFlexGrow?: string;
    guiMinWidth?: string;
    guiMaxWidth?: string;
    guiWidth?: string;
    guiCustomWidth?: string;
    columnName: string;

    // Client side fields
    relatedColumn?: AdColumnDtoModel;
    referenceValues?: AdReferenceKeyValueModel[];
    displayListeners: string[];
    designerSelected?: boolean;
    designerModify?: boolean;
    runtimeReadonly?: boolean;
    runtimeReadonlylogic?: string;
    runtimeDisplayed?: boolean;
    runtimeDisplaylogic?: string;
    cssContainerClass?: string;
    cssItemClass?: string;
    loadComplete?: boolean;
}
