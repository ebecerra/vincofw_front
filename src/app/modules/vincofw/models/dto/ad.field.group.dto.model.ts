import {AdFieldDtoModel} from './ad.field.dto.model';
import {AdBaseModel} from '../ad.base.model';

export class AdFieldGroupDtoModel extends AdBaseModel {
    idFieldGroup: string;
    idModule: string;
    name: string;
    collapsed: boolean;
    gridColumns: string;
    displaylogic: string;
    moduleName: string;

    // Client side fields
    fields: AdFieldDtoModel[];
    runtimeDisplayed: boolean;
    runtimeDisplaylogic: string;
}

export class AdFieldGroupItemViewModel {
    id: string;
    name: string;
    gridColumns: string;

    constructor(id: string, name: string, gridColumns: string) {
        this.id = id;
        this.gridColumns = gridColumns;
        this.name = name;
    }
}

export class AdFieldGroupViewModel {
    id: string;
    name: string;
    groups: AdFieldGroupItemViewModel[];

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
        this.groups = [];
    }
}

export class AdFieldGroupDesignerModel {
    id: string;
    name: string;
    gridColumns: string;
    fields: AdFieldDtoModel[];

    constructor(id: string, name: string, gridColumns: string) {
        this.id = id;
        this.name = name;
        this.gridColumns = gridColumns;
        this.fields = [];
    }
}
