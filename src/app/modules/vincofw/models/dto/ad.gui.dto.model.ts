export class AdGUIDtoModel {
    button: string;
    formField: string;
    filterApply: string;
    filterMode: string;
    tableMode: string;
    tableType: string;
}
