import {AdReferenceDtoModel} from './ad.reference.dto.model';

export class AdProcessParamDtoModel {
    idProcessParam: string;
    name: string;
    description: string;
    valuemin: string;
    valuemax: string;
    valuedefault: string;
    mandatory: boolean;
    ranged: boolean;
    displayed: boolean;
    saveType: string;
    ptype: string;
    caption: string;
    displaylogic: string;
    span: number;
    reference: AdReferenceDtoModel;

    // Client side
    cssColspan: string;
    value: string;
    runtimeDisplaylogic: string;
    runtimeDisplayed: boolean;
}
