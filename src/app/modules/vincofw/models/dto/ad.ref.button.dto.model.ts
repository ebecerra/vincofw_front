export class AdRefButtonDtoModel {
    btype: string;
    idProcess: string;
    jsCode: string;
    icon: string;
    showMode: string;
}
