export class AdRefListDtoModel {
    value: string;
    groupValue?: string;
    name: string;
    extraInfo?: string;
}
