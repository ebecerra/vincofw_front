
export class AdRefTableDtoModel {
    idTable: string;
    tableName: string;
    tableStdName: string;
    tableFldName: string;
    restPath: string;
    idKey: string;
    keyName: string;
    idDisplay: string;
    displayName: string;
    idGroupedKey: string;
    groupedKeyName: string;
    idGroupedDisplay: string;
    groupedDisplayName: string;
    sqlwhere: string;
    sqlorderby: string;

    // Client fields
    runtimeSqlwhere: string;
}
