export class AdReferenceKeyValueModel {
    key: string;
    value: string;
    groupedKey?: string;
    groupedValue?: string;
}
