export class AdTableActionDtoModel {
    idTableAction: string;
    idProcess: string;
    idWindow: string;
    atype: string;
    displaylogic: string;
    name: string;
    restCommand: string;
    url: string;
    target: string;
    icon: string;
    uipattern: string;
    seqno: number;
    privilege: string;
}
