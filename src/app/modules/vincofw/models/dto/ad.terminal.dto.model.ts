export class AdTerminalApiDtoModel {
    idTerminalApi: string;
    name: string;
    method: string;
    url: string;
    authorization: string;
    extraHeaders: string;
}

export class AdTerminalDtoModel {
    idTerminal: string;
    name: string;
    hmUrl: string;
    hmUser: string;
    hmPassword: string;
    terminalIdentifier: string;
    valid: boolean;
    apis: AdTerminalApiDtoModel[];
}
