export class EventChartTypeModel {
    idChart: string;
    previousChartType: string;
}

export class EventChartSelectModel {
    idChart: string;
    code: string;
    name: string;
}