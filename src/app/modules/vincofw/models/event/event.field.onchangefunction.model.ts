import {FormGroup} from '@angular/forms';
import {AdFieldDtoModel} from '../dto/ad.field.dto.model';

export class EventFieldOnchangefunctionModel {
    functionName: string;
    isNew: boolean;
    form: FormGroup;
    item: any;
    value: any;
    fields: AdFieldDtoModel[];
}
