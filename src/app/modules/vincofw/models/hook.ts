export class Hook {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    handler(args): Promise<any> {
        return null;
    }

    identifier(): string {
        return this.name + ' (' + this.id + ')';
    }
}
