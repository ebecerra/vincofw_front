export class Page {
    size = 20;          // The number of elements in the page
    totalElements = 0;  // The total number of elements
    totalPages = 0;     // The total number of pages
    pageNumber = 0;     // The current page number
}
