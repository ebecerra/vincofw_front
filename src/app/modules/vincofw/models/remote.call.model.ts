export class RemoteCallModel {
    success: boolean;
    message: string;
    data: any;

    constructor(success: boolean, message: string = null) {
        this.success = success;
        this.message = message;
    }
}
