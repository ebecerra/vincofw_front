export class SortParamModel {
    field: string;
    start: number;
    step: number;
    ids: string[];
}
