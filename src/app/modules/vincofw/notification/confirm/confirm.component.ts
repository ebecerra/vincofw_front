import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AuthService} from '../../authentication/auth.service';

export interface DialogData {
    title: string;
    message: string;
    icon?: string;
    iconClass?: string;
    items?: string[];
    question?: string;
}

@Component({
  selector: 'confirm-dialog',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

    public buttonType = 'mat-raised-button';

    constructor(
        public dialogRef: MatDialogRef<ConfirmComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private authService: AuthService
    ) { }

    ngOnInit(): void {
        this.authService.getLoggedClient().then(client => {
            this.buttonType = client.guiButton;
        });
    }

    doAccept(): void {
        this.dialogRef.close('OK');
    }

    doCancel(): void {
        this.dialogRef.close('CANCEL');
    }

}
