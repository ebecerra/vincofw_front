import {MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material';

export enum NotificationTypeModel {
    Info = 1,
    Success,
    Warning,
    Error
}

export class NotificationModel {
    notificationType: NotificationTypeModel;
    duration = 7;
    message: string;
    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    panelClass: string[];
}
