import {Injectable} from '@angular/core';
import {
    MatDialog,
    MatDialogRef,
    MatSnackBar,
    MatSnackBarHorizontalPosition,
    MatSnackBarVerticalPosition
} from '@angular/material';
import {NotificationComponent} from './notification/notification.component';
import {NotificationModel, NotificationTypeModel} from './notification.model';
import {isNullOrUndefined} from 'util';
import {ConfirmComponent} from './confirm/confirm.component';
import {QuestionComponent} from './question/question.component';

/**
 * Checks if the route can be activated.
 * A route can be activated if the user is logged in
 */
@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    durationInSeconds = 10;

    constructor(
        private _snackBar: MatSnackBar,
        private _dialog: MatDialog
    ) {

    }

    /**
     * Show success message
     *
     * @param message Message to show
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     * @param duration Show duration (in seconds)
     */
    public showSuccess(
        message: string, horizontalPosition: MatSnackBarHorizontalPosition = 'end', verticalPosition: MatSnackBarVerticalPosition = 'top',
        duration: number = this.durationInSeconds
    ): void {
        const settings: NotificationModel = this.getSettings(message, horizontalPosition, verticalPosition, duration);
        settings.notificationType = NotificationTypeModel.Success;
        settings.panelClass = ['success-snackbar'];
        this.show(settings);
    }

    /**
     * Show warning
     *
     * @param message Message to show
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     * @param duration Show duration (in seconds)
     */
    public showWarning(
        message: string, horizontalPosition: MatSnackBarHorizontalPosition = 'end', verticalPosition: MatSnackBarVerticalPosition = 'bottom',
        duration: number = this.durationInSeconds
    ): void {
        const settings: NotificationModel = this.getSettings(message, horizontalPosition, verticalPosition, duration);
        settings.notificationType = NotificationTypeModel.Warning;
        settings.panelClass = ['warning-snackbar'];
        this.show(settings);
    }

    /**
     * Show error
     *
     * @param message Message to show
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     * @param duration Show duration (in seconds)
     */
    public showError(
        message: string, horizontalPosition: MatSnackBarHorizontalPosition = 'end', verticalPosition: MatSnackBarVerticalPosition = 'bottom',
        duration: number = this.durationInSeconds
    ): void {
        const settings: NotificationModel = this.getSettings(message, horizontalPosition, verticalPosition, duration);
        settings.notificationType = NotificationTypeModel.Error;
        settings.panelClass = ['error-snackbar'];
        this.show(settings);
    }

    /**
     * Show information
     *
     * @param message Message to show
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     * @param duration Show duration (in seconds)
     */
    public showMessage(
        message: string, horizontalPosition: MatSnackBarHorizontalPosition = 'end', verticalPosition: MatSnackBarVerticalPosition = 'bottom',
        duration: number = this.durationInSeconds
    ): void {
        const settings: NotificationModel = this.getSettings(message, horizontalPosition, verticalPosition, duration);
        settings.notificationType = NotificationTypeModel.Info;
        this.show(settings);
    }

    /**
     * Show notification
     *
     * @param settings Parameters
     */
    public show(settings: NotificationModel): void {
        if (isNullOrUndefined(settings.duration)) {
            settings.duration = this.durationInSeconds;
        }

        this._snackBar.openFromComponent(NotificationComponent, {
            duration: settings.duration * 1000,
            horizontalPosition: settings.horizontalPosition,
            verticalPosition: settings.verticalPosition,
            panelClass: settings.panelClass,
            data: settings
        });
    }

    /**
     * Open a confirmation dialog
     *
     * @param title Title
     * @param message Message
     * @param icon Material icon
     * @param items List of items
     * @param question Question
     * @param width Dialog width
     */
    public confirmDialog(
        title: string, message: string, icon?: string, iconClass?: string, items?: string[],
        question?: string, width: string = '30%'
    ): MatDialogRef<any, any> {
        return this._dialog.open(ConfirmComponent, {
            width: width,
            disableClose: true,
            data: {
                title: title,
                message: message,
                icon: icon,
                iconClass: iconClass,
                items: items,
                question: question
            }
        });
    }

    /**
     * Open a question dialog (Yes or No)
     *
     * @param title Title
     * @param message Message
     * @param icon Material icon
     * @param items List of items
     * @param question Question
     * @param width Dialog width
     */
    public questionDialog(
        title: string, message: string, icon?: string, iconClass?: string, items?: string[],
        question?: string, width: string = '75%'
    ): MatDialogRef<any, any> {
        return this._dialog.open(QuestionComponent, {
            width: width,
            disableClose: true,
            data: {
                title: title,
                message: message,
                icon: icon,
                iconClass: iconClass,
                items: items,
                question: question
            }
        });
    }

    /**
     * Set notification parameters
     *
     * @param message Message to show
     * @param horizontalPosition Horizontal position
     * @param verticalPosition Vertical position
     * @param duration Show duration (in seconds)
     */
    private getSettings(
        message: string, horizontalPosition: MatSnackBarHorizontalPosition,
        verticalPosition: MatSnackBarVerticalPosition, duration: number
    ): NotificationModel {
        const settings: NotificationModel = new NotificationModel();
        settings.message = message;
        settings.duration = duration;
        settings.horizontalPosition = horizontalPosition;
        settings.verticalPosition = verticalPosition;
        return settings;
    }
}
