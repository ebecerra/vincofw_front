import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material';
import {NotificationModel, NotificationTypeModel} from '../notification.model';

@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent {
    public notificationType = 'message';

    constructor(@Inject(MAT_SNACK_BAR_DATA) public notificationSettings: NotificationModel) {
        switch (notificationSettings.notificationType) {
            case NotificationTypeModel.Info:
                this.notificationType = 'info';
                break;
            case NotificationTypeModel.Success:
                this.notificationType = 'success';
                break;
            case NotificationTypeModel.Warning:
                this.notificationType = 'warning';
                break;
            case NotificationTypeModel.Error:
                this.notificationType = 'error';
                break;
            default:
                this.notificationType = 'info';
                break;
        }
    }
}
