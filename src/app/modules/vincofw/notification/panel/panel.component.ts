import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'notification-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

    @Input()
    height = '3.8em';

    @Input()
    mode = 'error';

    @Input()
    message = 'Error';

    constructor() { }

    ngOnInit(): void {
    }

}
