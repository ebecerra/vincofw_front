import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AuthService} from '../../authentication/auth.service';

export interface DialogData {
    title: string;
    message: string;
    icon?: string;
    iconClass?: string;
    items?: string[];
    question?: string;
}

@Component({
  selector: 'question-dialog',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

    public buttonType = 'mat-raised-button';

    constructor(
        public dialogRef: MatDialogRef<QuestionComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private authService: AuthService
    ) { }

    ngOnInit(): void {
        this.authService.getLoggedClient().then(client => {
            this.buttonType = client.guiButton;
        });
    }

    doYes(): void {
        this.dialogRef.close('YES');
    }

    doNo(): void {
        this.dialogRef.close('NO');
    }

}
