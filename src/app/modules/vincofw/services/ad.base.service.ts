import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {SortOrder} from '../models/sort-order.enum';
import 'rxjs/add/operator/map';
import {PagedContentModel, SortData} from '../models/ad.base.model';
import {Injectable} from '@angular/core';

@Injectable()
export class AdBaseService<T> {

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        this.idLanguage = this._settings.idLanguage;
    }

    protected get idClient(): string{
        return this._settings.idClient;
    }

    protected set idClient(idClient: string) {
        this._settings.idClient = idClient;
    }
    protected idLanguage: string;
    protected url: string;

    /**
     * Get the string representation of the SortData information to be sent to the backend
     * @param sortData Sort information
     */
    public static getSortString(sortData: SortData[]): string {
        if (!isNullOrUndefined(sortData)) {
            let str = '[';
            sortData.forEach((current, index) => {
                str += ('{ "property": "' + current.property + '", "direction": "' + SortOrder[current.direction] + '"}');
                if (index < sortData.length - 1) {
                    str += ',';
                }
            });
            str += ']';
            return str;
        }
        return null;
    }

    protected getQuery(model: T): string {
        return this.processQuery(model, false);
    }

    protected processQuery(model: T, exact: boolean): string {
        let query = '';
        const keys = Object.keys(model);
        let hasFilterActive = false;
        keys.forEach(current => {
            const value = model[current];
            if (!isNullOrUndefined(value) && value !== '') {
                if (current === 'active') {
                    hasFilterActive = true;
                }
                if ((isNullOrUndefined(exact) || !exact) && typeof value === 'string' && value.indexOf('[') !== 0) {
                    query += (',' + current + '=%' + value + '%');
                } else {
                    query += (',' + current + '=' + value);
                }

            }
        });
        if (!hasFilterActive && keys.find(current => current === 'active')) {
            query += (',active=true');
        }
        return query;
    }

    /**
     * Make transformation to loaded model
     *
     * @param model
     */
    protected transformModel(model: T): void {

    }

    /**
     * Returns the list of elements
     * @param params HttpParams
     * @param sortOrder Sort order
     * @param limit Page size. Defaults to 1000
     * @param page Page index. Defaults to 1
     */
    list(params: HttpParams, sortOrder: SortData[] = null, limit: number = 1000, page: number = 1): Observable<PagedContentModel<T>> {
        if (isNullOrUndefined(params)) {
            params = new HttpParams();
        }
        let qParam = params.get('q');
        if (!isNullOrUndefined(qParam) && !qParam.includes('active=')) {
            qParam += (',active=true');
            params = params.set('q', qParam);
        } else if (isNullOrUndefined(qParam)) {
            qParam = ('active=true');
            params = params.append('q', qParam);
        }

        if (!isNullOrUndefined(sortOrder)) {
            params = params.append('sort', AdBaseService.getSortString(sortOrder));
        }
        if (!isNullOrUndefined(limit)) {
            params = params.append('limit', limit.toString());
        }
        if (!isNullOrUndefined(page)) {
            params = params.append('page', page.toString());
        }
        return this._http.get(this.getUrlWithClient(), {params: params})
            .map(response => {
                return this.mapPagedContent(response);
            });
    }

    /**
     * Calls the GET method to retrieve the entity
     * @param id Entity Key
     * @param idClient
     */
    get(id: string, idClient: string = null): Observable<T> {
        let url = this.getUrlWithClient();
        if (!isNullOrUndefined(idClient)){
            url = this.url + idClient + '/';
        }
        return this._http.get(url + id, {})
            .map(response => {
                this.transformModel(response as T);
                return response as T;
            });
    }

    /**
     * Calls the PUT method to update the entity
     * @param model Entity to be updated
     */
    update(model: T): Observable<void | string> {
        // Initialize Params Object
        return this._http.put(this.getUrlWithClient() + model['id'], model, {
            responseType: 'text'
        }).map(response => {
            return response ? response as string : null;
        });
    }

    /**
     * Calls the POST method to save the entity
     *
     * @param model Entity to be saved
     */
    save(model: T): Observable<any> {
        // Initialize Params Object
        return this._http.post(this.getUrlWithClient(), model, {
            responseType: 'text'
        });
    }

    /**
     * Generates the correct URL by adding the client ID section
     */
    getUrlWithClient(): string {
        return this.url + this.idClient + '/';
    }

    /**
     * Generates the correct URL
     */
    getUrl(): string {
        return this.url + '/';
    }

    /**
     * Maps the JSON response to object
     * @param response Mapped data
     */
    public mapPagedContent(response: any): PagedContentModel<T> {
        const responseData = response['content'] as T[];
        responseData.forEach(value => {
            if (!isNullOrUndefined(value['updated'])) {
                value['updated'] = new Date(value['updated']);
            }
            this.transformModel(value);
        });
        const pagedContent = new PagedContentModel<T>();
        pagedContent.content = responseData;
        pagedContent.firstPage = response['firstPage'] as boolean;
        pagedContent.lastPage = response['lastPage'] as boolean;
        pagedContent.number = response['number'] as number;
        pagedContent.totalPages = response['totalPages'] as number;
        pagedContent.totalElements = response['totalElements'] as number;
        pagedContent.numberOfElements = response['numberOfElements'] as number;
        pagedContent.size = response['size'] as number;
        return pagedContent;
    }

}
