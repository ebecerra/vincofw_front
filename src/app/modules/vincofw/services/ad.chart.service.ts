import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdChartResultModel} from '../models/ad.chart.result.model';
import {AdBaseModel} from '../models/ad.base.model';

@Injectable({
    providedIn: 'root'
})
export class AdChartService extends AdBaseService<AdBaseModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_chart/';
    }

    evaluate(idChart: string, idUser: string, constraints: string): Observable<AdChartResultModel> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('idLanguage', this.idLanguage);
        params = params.append('idUser', idUser);
        params = params.append('q', constraints);
        return this._http.get(this.getUrlWithClient() + 'evaluate/' + idChart, { params: params })
            .map(response => {
                return response as AdChartResultModel;
            });
    }

}
