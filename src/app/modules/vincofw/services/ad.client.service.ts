import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdBaseService} from './ad.base.service';
import {AdClientModel} from '../models/ad.client.model';

@Injectable({
    providedIn: 'root'
})
export class AdClientService extends AdBaseService<AdClientModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_client';
    }

    getUrlWithClient(): string {
        return this.url  + '/';
    }
}
