import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdBaseService} from './ad.base.service';
import {AdFieldGridModel} from '../models/ad.field.grid.model';
import {Observable} from 'rxjs';
import {ControllerResultModel} from '../models/controller.result.model';

@Injectable({
    providedIn: 'root'
})
export class AdFieldGridService extends AdBaseService<AdFieldGridModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_field_grid/';
    }

    saveColumns(idTab: string, idUser: string, fields: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('idTab', idTab);
        params = params.append('idUser', idUser);
        params = params.append('fields', fields);
        return this._http.post(this.getUrlWithClient() + 'save', null, { params: params }).map(resp => resp as ControllerResultModel);
    }

    deleteColumns(idTab: string, idUser: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('idTab', idTab);
        params = params.append('idUser', idUser);
        return this._http.delete(this.getUrlWithClient() + 'delete', { params: params });
    }
}