import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdBaseService} from './ad.base.service';
import {Observable} from 'rxjs';
import {AdBaseModel} from '../models/ad.base.model';
import {AdMultiSelectValueModel} from '../models/ad.multi.select.value.model';

@Injectable({
    providedIn: 'root'
})
export class AdFieldService extends AdBaseService<AdBaseModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_field/';
    }

    multiselect(idField: string, parentFieldId: string, rowKey: string): Observable<AdMultiSelectValueModel[]> {
        let params = new HttpParams();
        params = params.append('id', idField);
        params = params.append('parentFieldId', parentFieldId);
        params = params.append('rowKey', rowKey);
        return this._http.get(this.getUrlWithClient() + idField + '/multiselect', { params: params }).map(resp => resp['content'] as AdMultiSelectValueModel[]);
    }

}