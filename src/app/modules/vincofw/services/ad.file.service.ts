import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {isNullOrUndefined} from 'util';
import {AdFileModel} from '../models/ad.file.model';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';

export interface IAdFileService {
    listFiles(idTable: string, idRow: string): Observable<AdFileModel[]>;
}

@Injectable({
    providedIn: 'root'
})
export class AdFileService extends AdBaseService<AdFileModel> implements IAdFileService {

    constructor(http: HttpClient,
                settings: AppCoreSettings) {
        super(http, settings);
        this.url = settings.getRestHostCore() + 'ad_file/';
    }

    listFiles(idTable: string, idRow: string): Observable<AdFileModel[]> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('q', 'idTable=' + idTable + ',idRow=' + idRow + ',active=true');

        return this.list(params, null, null, null)
        // return this.http.get(this.url, { params: params })
            .map(response => {
                const files = response.content as AdFileModel[];
                files.forEach(value => {
                    if (!isNullOrUndefined(value) && !isNullOrUndefined(value.mimetype)) {
                        if (value.mimetype.indexOf('pdf') > 0) {
                            value.extension = 'pdf';
                        } else if (value.mimetype.indexOf('msword') > 0 || value.mimetype.indexOf('wordprocessingml') > 0) {
                            value.extension = 'word';
                        } else if (value.mimetype.indexOf('ms-excel') > 0 || value.mimetype.indexOf('spreadsheetml') > 0) {
                            value.extension = 'excel';
                        } else if (value.mimetype.indexOf('zip') > 0 || value.mimetype.indexOf('rar') > 0
                            || value.mimetype.indexOf('compressed') > 0) {
                            value.extension = 'zip';
                        } else {
                            value.extension = 'default';
                        }
                    } else {
                        value.extension = 'default';
                    }
                });
                files.sort((first, second) => {
                    if (first.seqno < second.seqno) {
                        return -1;
                    }
                    if (first.seqno > second.seqno) {
                        return 1;
                    }
                    return 0;
                });
                return files;
            });
    }

}
