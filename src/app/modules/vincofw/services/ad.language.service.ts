import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import {isNullOrUndefined} from 'util';
import {AdLanguageModel} from '../models/ad.language.model';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';

export interface IAdLanguageService {
    getClientLanguages(): Observable<AdLanguageModel[]>;

    getLanguage(idLanguage: string): Observable<AdLanguageModel>;
}

@Injectable({
    providedIn: 'root'
})
export class AdLanguageService extends AdBaseService<AdLanguageModel> implements IAdLanguageService {

    private _languages: AdLanguageModel[];
    private urlClientLanguage: string;

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_language/';
    }

    getCachedLanguage(idLanguage: string): AdLanguageModel {
        return this._languages.find(current => current.idLanguage === idLanguage);
    }

    getLanguage(idLanguage: string): Observable<AdLanguageModel> {
        return this.getClientLanguages().map(response => {
            return response.find(current => {
                return current.idLanguage === idLanguage;
            });
        });
    }

    getClientLanguages(idClient: string = null, ignoreCache: boolean = false): Observable<AdLanguageModel[]> {
        if (!ignoreCache && !isNullOrUndefined(this._languages)) { // static data
            return Observable.of(this._languages);
        }

        this.urlClientLanguage = this._settings.getRestHostCore() + 'ad_client_language/' + (idClient ? idClient : this._settings.idClient);
        let params = new HttpParams();
        params = params.append('q', 'active=true');
        return this._http.get(this.urlClientLanguage + '/list', { params: params }).map(response => {
            this._languages = response['content'] as AdLanguageModel[];
            return this._languages;
        });
    }

    set idLanguage(idLanguage: string) {
        if (idLanguage !== this._settings.idLanguage) {
            const language = this.getCachedLanguage(idLanguage);
            this._settings.setItem('idLanguage', idLanguage);
            this._settings.setItem('language', language.iso2);
            this._settings.setItem('countryCode', language.countrycode);
            this._settings.settings.ID_LANGUAGE = idLanguage;
        }
    }

}
