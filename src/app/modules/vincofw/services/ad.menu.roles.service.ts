import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {GlobalEventsService} from './global.events.service';
import {AdBaseModel} from '../models/ad.base.model';
import {ControllerResultModel} from '../models/controller.result.model';

@Injectable({
    providedIn: 'root'
})
export class AdMenuRolesService extends AdBaseService<AdBaseModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings,
        private _eventService: GlobalEventsService
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_menu_roles/' + this.idClient;
    }

    saveMenu(params: any): Observable<ControllerResultModel> {
        return this._http.post(this.getUrlWithClient() + 'save_menu', params).map(response => {
            return response as ControllerResultModel;
        });
    }

}
