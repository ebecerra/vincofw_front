import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdMenuModel} from '../models/ad.menu.model';
import {GlobalEventsService} from './global.events.service';

@Injectable({
    providedIn: 'root'
})
export class AdMenuService extends AdBaseService<AdMenuModel> {

    private menu: AdMenuModel[] = [];

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings,
        private _eventService: GlobalEventsService
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_menu/' + this.idClient;
    }

    public listMenus(roleId: string, idLanguage: string): Observable<AdMenuModel[]> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.set('notExpand', 'true');
        params = params.set('roleId', roleId);
        params = params.set('idLanguage', idLanguage);

        return this.list(params).map(response => {
            this.menu = response.content;
            this._eventService.notifyMenuLoaded();
            return this.menu;
        });
    }

    public allMenu(idRole: string): Observable<AdMenuModel[]> {
        let params = new HttpParams();
        params = params.set('idRole', idRole);
        return this._http.get(this.getUrlWithClient() + 'all_menu', { params: params })
            .map(response => {
                return response['properties'].menu;
            });
    }

    isLoaded(): boolean {
        return this.menu.length > 0;
    }

    getMenu(idMenu: string): AdMenuModel {
        for (const item of this.menu) {
            if (item.idMenu === idMenu) {
                return item;
            }
            const result = this.findMenuItem(item.subMenus, idMenu);
            if (result) {
                return result;
            }
        }
        return null;
    }

    findMenu(search: string): AdMenuModel[] {
        const result: AdMenuModel[] = [];
        this.findMenuItemByName(result, this.menu, search);
        return result;
    }

    getMenuByWindow(idWindow: string): AdMenuModel {
        for (const item of this.menu) {
            if (item.action === 'WINDOW' && item.idWindow === idWindow) {
                return item;
            }
            const result = this.findMenuWindowItem(item.subMenus, 'idWindow', idWindow);
            if (result) {
                return result;
            }
        }
        return null;
    }

    getMenuByTable(idTable: string): AdMenuModel {
        for (const item of this.menu) {
            if (item.action === 'WINDOW' && item.idTable === idTable) {
                return item;
            }
            const result = this.findMenuWindowItem(item.subMenus, 'idTable', idTable);
            if (result) {
                return result;
            }
        }
        return null;
    }

    private findMenuItemByName(result: AdMenuModel[], subMenus: AdMenuModel[], search: string): void {
        for (const item of subMenus) {
            if (item.subMenus && item.subMenus.length > 0) {
                this.findMenuItemByName(result, item.subMenus, search);
            } else {
                if (item.name.toLowerCase().indexOf(search) !== -1) {
                    result.push(item);
                }
            }
        }
    }

    private findMenuItem(subMenus: AdMenuModel[], idMenu: string): AdMenuModel {
        if (!subMenus) {
            return null;
        }
        for (const item of subMenus) {
            if (item.idMenu === idMenu) {
                return item;
            }
            const result = this.findMenuItem(item.subMenus, idMenu);
            if (result) {
                return result;
            }
        }
        return null;
    }

    private findMenuWindowItem(subMenus: AdMenuModel[], field: string, id: string): AdMenuModel {
        if (!subMenus) {
            return null;
        }
        for (const item of subMenus) {
            if (item.action === 'WINDOW' && item[field] === id) {
                return item;
            }
            const result = this.findMenuWindowItem(item.subMenus, field, id);
            if (result) {
                return result;
            }
        }
        return null;
    }
}
