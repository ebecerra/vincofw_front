import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {AdReferenceModel} from '../models/ad.reference.model';
import {AdPermissionModel} from '../models/ad.permission.model';
import {AdPreferenceValueModel} from '../models/ad.preference.value.model';

export interface IAdPreferenceService {
    permissions(idRole: string): Observable<AdPermissionModel[]>;
    preference(name: string, idRole: string, params: string): Observable<AdPreferenceValueModel>;
    preferences(idRole: string): Observable<AdPreferenceValueModel[]>;
}

@Injectable({
    providedIn: 'root'
})
export class AdPreferenceService extends AdBaseService<AdReferenceModel> implements IAdPreferenceService {

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_preference/';
    }

    permissions(idRole: string): Observable<AdPermissionModel[]> {
        let params = new HttpParams();
        params = params.set('idRole', idRole);
        return this._http.get(this.getUrlWithClient() + 'permissions', { params: params })
            .map(response => {
                return response['properties'].permissions as AdPermissionModel[];
            });
    }

    preference(name: string, idRole: string, params: string = null): Observable<AdPreferenceValueModel> {
        let args = new HttpParams();
        args = args.set('name', name);
        args = args.set('idRole', idRole);
        if (!isNullOrUndefined('params')){
            args = args.set('params', params);
        }
        return this._http.get(this.getUrlWithClient() + 'preference', { params: args })
            .map(response => {
                return response['properties'].preference as AdPreferenceValueModel;
            });
    }

    preferences(idRole): Observable<AdPreferenceValueModel[]> {
        let args = new HttpParams();
        args = args.set('idRole', idRole);
        return this._http.get(this.getUrlWithClient() + 'preferences', { params: args })
            .map(response => {
                return response['properties'].preferences as AdPreferenceValueModel[];
            });
    }
}
