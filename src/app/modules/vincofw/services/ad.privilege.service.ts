import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdPrivilegeModel} from '../models/ad.privilege.model';
import {AdUserModel} from '../models/ad.user.model';

@Injectable({
    providedIn: 'root'
})
export class AdPrivilegeService extends AdBaseService<AdPrivilegeModel> {

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_privilege/';
    }

    users(privilege: string): Observable<AdUserModel[]> {
        return this._http.get(this.getUrlWithClient() + privilege + '/users', { })
            .map((response: any) => {
                return response as AdUserModel[];
            });
    }

}
