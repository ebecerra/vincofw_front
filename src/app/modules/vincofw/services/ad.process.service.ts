import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdProcessDtoModel} from '../models/dto/ad.process.dto.model';
import {ControllerResultModel} from '../models/controller.result.model';
import {AdProcessResponse} from '../models/ad.process.model';

@Injectable({
    providedIn: 'root'
})
export class AdProcessService extends AdBaseService<AdProcessDtoModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_process/';
    }

    public load(idProcess: string, idLanguage: string = null): Observable<AdProcessDtoModel> {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.append('idLanguage', idLanguage);

        return this._http.get(this.getUrlWithClient() + idProcess + '/load', { params: params })
            .map(response => {
                return response as AdProcessDtoModel;
            });
    }

    public exec(idProcess: string, idProcessExec: string, paramValues: string, idLanguage: string = null): Observable<AdProcessResponse> {
        const params = this.getParams(paramValues, idLanguage);
        return this._http.post(
            this.getUrlWithClient() + 'exec/' + idProcess + '/' + idProcessExec, null,
            { params: params, responseType: 'blob', observe: 'response' }
            ).map(response => {
                return {
                    body: response.body as Blob,
                    fileName: response.headers.get('X-Filename')
                };
            });
    }

    public execHtml(idProcess: string, idProcessExec: string, paramValues: string, idLanguage: string = null): Observable<string> {
        const params = this.getParams(paramValues, idLanguage);
        return this._http.post(
            this.getUrlWithClient() + 'exec/' + idProcess + '/' + idProcessExec, null,
            { params: params, responseType: 'text' }
            ).map(response => {
                return response as string;
            });
    }

    public cancelExec(idProcess: string, idProcessExec: string): Observable<ControllerResultModel> {
        return this._http.post(this.getUrlWithClient() + '/cancel_exec/' + idProcess + '/' + idProcessExec, null)
            .map(response => {
                return response as ControllerResultModel;
            });
    }

    public schedule(idProcess: string, idClient, idUser: string, cron: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('idUser', idUser);
        params = params.append('cron', cron);
        return this._http.post(this.url + idClient + '/schedule/' + idProcess, null, { params: params })
            .map(response => {
                return response as ControllerResultModel;
            });
    }

    public clearSchedule(idProcess: string): Observable<void> {
        return this._http.delete(this.getUrlWithClient() + 'clear_schedule/' + idProcess)
            .map(() => {
                return null;
            });
    }

    private getParams(paramValues: string, idLanguage: string): HttpParams {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.append('idLanguage', idLanguage);
        params = params.append('params', paramValues);
        return params;
    }
}
