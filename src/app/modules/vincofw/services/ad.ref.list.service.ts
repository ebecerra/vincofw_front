import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AdRefListModel} from '../models/ad.ref.list.model';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';

export interface IAdRefListService {
    listReferenceLists(idReference: string): Observable<AdRefListModel[]>;
}

@Injectable({
    providedIn: 'root'
})
export class AdRefListService extends AdBaseService<AdRefListModel> implements IAdRefListService {
    private references = new Map<string, any[]>();

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_ref_list/';
    }

    listReferenceLists(idReference: string): Observable<AdRefListModel[]> {
        if (this.references.has(idReference) && !isNullOrUndefined(this.references.get(idReference))) {
            return Observable.of(this.references.get(idReference));
        }
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('q', 'idReference=' + idReference + ',active=1');
        return this._http.get(this.getUrlWithClient(), {params: params})
            .map(response => {
                const data = (response['content'] as AdRefListModel[]).sort(value => {
                    return value.seqno;
                });
                this.references.set(idReference, data);
                return response['content'] as AdRefListModel[];
            });
    }

    getReferenceValue(idReference: string, value: string): string {
        if (this.references.has(idReference) && !isNullOrUndefined(this.references.get(idReference))) {
            const found = this.references.get(idReference).find(current => {
                return current.value === value;
            });
            if (!isNullOrUndefined(found)) {
                return found.name;
            }
            return value;
        }
    }

}
