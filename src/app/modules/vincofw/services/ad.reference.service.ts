import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {AdReferenceModel} from '../models/ad.reference.model';
import {AdReferenceDtoModel} from '../models/dto/ad.reference.dto.model';
import {AdReferenceKeyValueModel} from '../models/dto/ad.reference.key.value.model';
import {ConditionalService} from './conditional.service';
import {AdFieldDtoModel} from '../models/dto/ad.field.dto.model';
import {SortData, SortOrder} from '../models/ad.base.model';
import {Subject} from 'rxjs';
import {AdHookNames, HookService} from './hook.service';

@Injectable({
    providedIn: 'root'
})
export class AdReferenceService extends AdBaseService<AdReferenceModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings,
        private _hookService: HookService,
        private _conditionalService: ConditionalService
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_reference/';
    }

    public load(idReference: string, idLanguage: string = null): Observable<AdReferenceDtoModel> {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.set('idLanguage', idLanguage);

        return this._http.get(this.getUrlWithClient() + idReference + 'load', {params: params})
            .map(response => response as AdReferenceDtoModel);
    }

    public loadReferences(references: string[], idLanguage: string = null): Observable<AdReferenceDtoModel[]> {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.set('idLanguage', idLanguage);
        params = params.set('references', references.join(', '));

        return this._http.get(this.getUrlWithClient() + 'load', { params: params }).map(response => {
            const result: AdReferenceDtoModel[] = [];
            const values = response as AdReferenceDtoModel[];
            values.forEach(reference => {
                result.push(new AdReferenceDtoModel(reference));
            });
            return result;
        });
    }

    public loadTabledir(fields: AdFieldDtoModel[], reference: AdReferenceDtoModel, item?: any, idLanguage: string = null): Observable<AdReferenceKeyValueModel[]> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.set('_d', '' + new Date().getTime());
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.set('idLanguage', idLanguage);

        // Sorting
        if (reference.refTable.sqlorderby) {
            const sortData: SortData[] = [];
            const orderby = reference.refTable.sqlorderby.split(',');
            orderby.forEach(ord => {
                const order = ord.trim().split(' ');
                sortData.push({
                    property: order[0],
                    direction: order.length === 2 ? (order[1].toLowerCase() === 'asc' ? SortOrder.ASC : SortOrder.DESC) : SortOrder.ASC
                });
            });
            params = params.append('sort', AdBaseService.getSortString(sortData));
        }

        const subject = new Subject<AdReferenceKeyValueModel[]>();

        // Execute Hook AD_QUERY_TABLE
        let q = '';
        this._hookService.execHook(
            AdHookNames.AD_QUERY_TABLE, {
                tableName: reference.refTable.tableName,
                constraints: q
            }
        ).then(response => {
            if (response.success) {
                response.results.forEach(r => q += ',' + r);
            }
            // Conditional WHERE
            if (!isNullOrUndefined(reference.refTable.sqlwhere))  {
                let sqlWhere = reference.refTable.sqlwhere;
                sqlWhere = this._conditionalService.replaceSQLConditional(sqlWhere, fields, item);
                if (!this._conditionalService.isCompleteConditional(sqlWhere)) {
                    subject.next([]);
                    subject.complete();
                    return;
                }
                q += ', $extended$={' + sqlWhere + '}';
            }
            params = params.set('q', q);

            // Get values from server
            const url = this._settings.getRestHostPath() + reference.refTable.restPath + '/' + reference.refTable.tableName +
                (reference.refTable.tableName === 'ad_client' ? '' : ('/' + this.idClient));
            this._http.get(url, { params: params }).map(data => {
                const responseData = data['content'] as any[];
                if (isNullOrUndefined(responseData)) {
                    return [];
                }
                const grouped = !isNullOrUndefined(reference.refTable.idGroupedKey) && !isNullOrUndefined(reference.refTable.idGroupedDisplay);
                return responseData.map(current => {
                    const transformed = new AdReferenceKeyValueModel();
                    transformed.key = current[reference.refTable.keyName];
                    transformed.value = current[reference.refTable.displayName];
                    if (grouped) {
                        transformed.groupedKey = current[reference.refTable.groupedKeyName];
                        transformed.groupedValue = current[reference.refTable.groupedDisplayName];
                    }
                    return transformed;
                });
            }).subscribe(resp => {
                subject.next(resp as AdReferenceKeyValueModel[]);
                subject.complete();
            });
        });

        return subject;
    }
}
