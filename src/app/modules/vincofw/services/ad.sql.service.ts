import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdBaseModel} from '../models/ad.base.model';
import {ControllerResultModel} from '../models/controller.result.model';

export interface SqlParams {
    sqlType: string;
    sqlQuery: string;    
}

@Injectable({
    providedIn: 'root'
})
export class AdSqlService extends AdBaseService<AdBaseModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_sql_exec/';
    }

    public exec(sql: SqlParams): Observable<ControllerResultModel> {
        return this._http.post(this.getUrlWithClient() + 'exec', sql)
            .map(response => {
                return response as ControllerResultModel;
            });
    }

}
