import {Injectable} from '@angular/core';
import {AdUserModel} from '../models/ad.user.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdTabMainDtoModel} from '../models/dto/ad.tab.main.dto.model';
import {AdDesignerInfoModel} from '../models/ad.designer.info.model';

@Injectable({
    providedIn: 'root'
})
export class AdTabService extends AdBaseService<AdUserModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_tab/';
    }

    public load(idWindow: string, idTab: string, idLanguage: string = null): Observable<AdTabMainDtoModel> {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.append('idLanguage', idLanguage);
        params = params.append('idWindow', idWindow);

        return this._http.get(this.getUrlWithClient() + idTab + '/load', { params: params })
            .map(response => {
                const tab = response as AdTabMainDtoModel;
                this.processTab(tab);
                return tab;
            });
    }

    public designerSave(idTab: string, model: AdDesignerInfoModel): Observable<any> {
        return this._http.post(this.getUrlWithClient() + idTab + '/save', model);
    }

    public processTab(tab: AdTabMainDtoModel): void {
        if (tab.ttype === 'USERDEFINED') {
            return;
        }
        if (tab.ttype === 'CHART') {
            return;
        }
        // Link columns to fields
        tab.tab.table.columns.forEach(column => {
            column.relatedField = tab.tab.fields.find(field => field.idColumn === column.idColumn);
            if (column.relatedField) {
                column.relatedField.relatedColumn = column;
            }
            if (column.primaryKey) {
                tab.tab.table.primaryKey = column;
            }
        });
        // Build field groups
        tab.tab.ungroupedFields = [];
        tab.tab.fields.forEach(field => {
            if (!isNullOrUndefined(field.cssClass)) {
                const cssClass = field.cssClass.split(',');
                field.cssClass = cssClass.join(' ');
                let colCss = '', itemCss = '';
                cssClass.forEach(css => {
                    if (css.indexOf('margin') > 0) {
                        colCss += ' ' + css;
                    } else {
                        itemCss += ' ' + css;
                    }
                });
                field.cssContainerClass = colCss;
                field.cssItemClass = itemCss;
            }
            field.columnReferenceType = field.relatedColumn.reference.rtype;
            if (!isNullOrUndefined(field.idFieldGroup)) {
                const fieldGroup = tab.tab.fieldGroups.find(grp => grp.idFieldGroup === field.idFieldGroup);
                fieldGroup.fields.push(field);
            } else {
                tab.tab.ungroupedFields.push(field);
            }
        });
        // Filter visible tabs
        tab.visibleTabs = tab.childTabs.filter(childTab => childTab.uipattern !== 'SORTABLE' && childTab.uipattern !== 'MULTISELECT');
    }

}
