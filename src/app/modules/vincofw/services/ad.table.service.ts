import {Injectable} from '@angular/core';
import {AdUserModel} from '../models/ad.user.model';
import {HttpClient} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdTableDtoModel} from '../models/dto/ad.table.dto.model';

@Injectable({
    providedIn: 'root'
})
export class AdTableService extends AdBaseService<AdUserModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_table/';
    }

    public load(idTable: string): Observable<AdTableDtoModel> {
        return this._http.get(this.getUrlWithClient() + idTable + '/load', { })
            .map(response => {
                return response as AdTableDtoModel;
            });
    }
}
