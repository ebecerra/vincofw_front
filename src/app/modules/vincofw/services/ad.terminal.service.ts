import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs';
import 'rxjs/add/operator/map';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdTerminalDtoModel} from '../models/dto/ad.terminal.dto.model';

@Injectable({
    providedIn: 'root'
})
export class AdTerminalService extends AdBaseService<any> {

    private terminalConfig: AdTerminalDtoModel = null;

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_terminal/';
    }

    /**
     * Return terminal configuration
     */
    public getTerminalConfig(): AdTerminalDtoModel {
        return this.terminalConfig;
    }

    /**
     * Link a terminal
     *
     * @param username User name (linker user)
     * @param password Password
     * @param idRole User role
     * @param terminalIdentifier Terminal identifier
     */
    public link(username: string, password: string, idRole: string, terminalIdentifier: string): Observable<any> {
        const params = {
            username: username,
            password: password,
            idRole: idRole,
            terminalIdentifier: terminalIdentifier
        };
        return this._http.post(this.getUrlWithClient() + 'link', params);
    }

    /**
     * Check if terminal is linked right
     *
     * @param terminalIdentifier Terminal identifier
     * @param linkedIdentifier Linker identifier
     */
    public validLink(terminalIdentifier: string, linkedIdentifier: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('terminalIdentifier', terminalIdentifier);
        params = params.append('linkedIdentifier', linkedIdentifier);
        return this._http.get(this.getUrlWithClient() + 'valid_link', { params: params });
    }

    /**
     * Load terminal information
     *
     * @param terminalIdentifier Terminal identifier
     * @param linkedIdentifier Linker identifier
     */
    public load(terminalIdentifier: string, linkedIdentifier: string): Observable<AdTerminalDtoModel> {
        if (this.terminalConfig) {
            return of(this.terminalConfig);
        } else {
            let params = new HttpParams();
            params = params.append('terminalIdentifier', terminalIdentifier);
            params = params.append('linkedIdentifier', linkedIdentifier);
            return this._http.get(this.getUrlWithClient() + 'load', { params: params }).map(response => {
                this.terminalConfig = response as AdTerminalDtoModel;
                return this.terminalConfig;
            });
        }
    }

    /**
     * Check the terminal linked status
     *
     * @param terminalAuthentication Terminal Authentication permission
     */
    public checkTerminalAuthentication(terminalAuthentication: boolean): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (terminalAuthentication) {
                const terminalIdentifier = this._settings.getItem('terminalIdentifier');
                const linkedIdentifier = this._settings.getItem('linkedIdentifier');
                if (terminalIdentifier && linkedIdentifier) {
                    this.validLink(terminalIdentifier, linkedIdentifier).subscribe(response => {
                        resolve(response.success);
                    });
                } else {
                    resolve(false);
                }
            } else {
                resolve(true);
            }
        });
    }

}
