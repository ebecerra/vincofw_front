import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {isNullOrUndefined} from 'util';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdUserModel} from '../models/ad.user.model';
import {AdBaseService} from './ad.base.service';
import {TranslationModel} from '../models/translation.model';
import {AdTranslationModel} from '../models/ad.translation.model';

export interface IAdTranslationService {
    messages(idLanguage: string, idClient: string): Observable<TranslationModel>;
}

@Injectable({
    providedIn: 'root'
})
export class AdTranslationService extends AdBaseService<AdUserModel> implements IAdTranslationService {

    private _translations: TranslationModel;
    private _webTranslations: TranslationModel;

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_translation/';
    }

    public clearTranslations(): void {
        this._translations = null;
        this._webTranslations = null;
    }

    /**
     * Calls the backend "messages" method
     *
     * @param idLanguage Language identifier
     * @param idClient Client identifier
     */
    public messages(idLanguage: string, idClient: string = '0'): Observable<TranslationModel> {
        if (!isNullOrUndefined(this._translations)) {
            return Observable.of(this._translations);
        }
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('onlyMsg', 'true');
        if (!isNullOrUndefined(idLanguage)) {
            params = params.append('idLanguage', idLanguage);
        }
        const url = this.url + idClient + '/';
        return this._http.get(url + 'messages', { params: params }).map(response => {
            this._translations = response['properties'] as TranslationModel;
            return this._translations;
        });
    }

    /**
     * Get translations for element
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row identifier
     */
    public getTranslations(idClient: string, idTable: string, idColumn: string, rowkey: string): Observable<AdTranslationModel[]> {
        let params = new HttpParams();
        params = params.append('q', `idClient=${idClient},idTable=${idTable},idColumn=${idColumn},rowkey=${rowkey}`);
        return this._http.get(this.url + idClient + '/', { params: params }).map(response => {
            return response['content'] as AdTranslationModel[];
        });
    }

    public saveTranslations(item: any): Observable<void> {
        return this._http.put(this.getUrlWithClient() + 'save_translations', item).map(() => null);
    }

}
