import {Injectable} from '@angular/core';
import {AdUserModel} from '../models/ad.user.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {AdBaseService} from './ad.base.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {ControllerResultModel} from '../models/controller.result.model';

@Injectable({
    providedIn: 'root'
})
export class AdUserService extends AdBaseService<AdUserModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_user/' + this.idClient;
    }

    public recover(email: string, languageCode: string): Observable<ControllerResultModel> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('email', email);
        if (!isNullOrUndefined(languageCode)) {
            params = params.append('language', languageCode);
        }

        return this._http.get(this.getUrlWithClient() + 'recover', { params: params })
            .map(response => {
                return response as ControllerResultModel;
            });
    }

    public changeProfile(idUser: string, idLanguage: string, idRole: string): Observable<void> {
        // Initialize Params Object
        let params = new HttpParams();
        params = params.append('idLanguage', idLanguage).append('idRole', idRole);

        return this._http.post(this.getUrlWithClient() +  idUser + '/change_role', null, { params: params }).pipe(
            map(() => {
                return;
            })
        );
    }

    public updateProfile(model: AdUserModel): Observable<AdUserModel> {
        // Initialize Params Object
        return this._http.put(this.getUrlWithClient() + model['id'] + '/update_profile', model)
            .map(() => model);
    }

    protected transformModel(model: AdUserModel): void {
        model.currentIdRole = model.defaultIdRole;
        model.currentIdLanguage = model.defaultIdLanguage;
    }

}
