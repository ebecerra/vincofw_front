import {Injectable} from '@angular/core';
import {AdUserModel} from '../models/ad.user.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {AdBaseService} from './ad.base.service';
import {AdWindowDtoModel} from '../models/dto/ad.window.dto.model';
import {AdTabMainDtoModel} from '../models/dto/ad.tab.main.dto.model';
import {AdTabService} from './ad.tab.service';

@Injectable({
    providedIn: 'root'
})
export class AdWindowService extends AdBaseService<AdUserModel> {

    constructor(
        _http: HttpClient,
        _settings: AppCoreSettings,
        private _tabService: AdTabService
    ) {
        super(_http, _settings);
        this.url = _settings.getRestHostCore() + 'ad_window/';
    }

    public load(idWindow: string, idLanguage: string = null): Observable<AdWindowDtoModel> {
        // Initialize Params Object
        let params = new HttpParams();
        if (isNullOrUndefined(idLanguage)) {
            idLanguage = this.idLanguage;
        }
        params = params.set('idLanguage', idLanguage);

        return this._http.get(this.getUrlWithClient() + idWindow + '/load', {params: params})
            .map(response => {
                const window = response as AdWindowDtoModel;
                window.mainTab.childTabs = response['mainTab']['childTabs'] as AdTabMainDtoModel[];
                this._tabService.processTab(window.mainTab);
                return window;
            });
    }
}
