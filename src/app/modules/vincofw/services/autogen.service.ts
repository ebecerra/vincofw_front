import {HttpClient, HttpParams} from '@angular/common/http';
import {AppCoreSettings} from '../configs/app.core.settings';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {AdTableDtoModel} from '../models/dto/ad.table.dto.model';
import {isNullOrUndefined} from 'util';
import {AdBaseModel, PagedContentModel, SortData} from '../models/ad.base.model';
import {Observable} from 'rxjs';
import {AdBaseService} from './ad.base.service';
import {ControllerResultModel} from '../models/controller.result.model';
import {SortParamModel} from '../models/sort.param.model';

@Injectable({
    providedIn: 'root'
})
export class AutogenService extends AdBaseService<AdBaseModel> {

    private _table: AdTableDtoModel;

    constructor(
        protected _http: HttpClient,
        protected _settings: AppCoreSettings
    ) {
        super(_http, _settings);
    }

    public bindedService(table: AdTableDtoModel): AutogenService {
        const service = new AutogenService(this._http, this._settings);
        service._table = table;
        return service;
    }

    /**
     * Returns the list of elements
     *
     * @param params HttpParams
     * @param sortOrder Sort order
     * @param limit Page size. Defaults to 1000
     * @param page Page index. Defaults to 1
     */
    list(params: HttpParams = new HttpParams(), sortOrder: SortData[] = null, limit: number = 1000, page: number = 1): Observable<PagedContentModel<AdBaseModel>> {
        params = this.updateParams(params, sortOrder, limit, page);
        return this._http.get(this.getUrl(), { params: params }).map(response => {
            return this.mapPagedContent(response);
        });
    }

    /**
     * Calls the GET method to retrieve the entity
     *
     * @param id Entity Key
     */
    get(id: string): Observable<any> {
        return this._http.get(this.getUrl() + '/' + id, {});
    }

    /**
     * Export table records
     *
     * @param params HttpParams
     * @param sortOrder Sort order
     * @param idTab Tab identifier
     * @param idUser User identifier
     */
    exportData(params: HttpParams = new HttpParams(), sortOrder: SortData[] = null, idTab: string, idUser: string): Observable<Blob> {
        params = this.updateParams(params, sortOrder, null, null);
        params = params.append('idUser', idUser);
        return this._http.get(this.getUrl() + '/export/' + idTab, { params: params, responseType: 'blob' });
    }

    /**
     * Delete table records
     *
     * @param entityIds Record identifier
     */
    delete(entityIds: string[]): Observable<any> {
        return this._http.post(this.getUrl() + '/delete_batch', entityIds);
    }

    /**
     * Delete image
     *
     * @param id Row identifier
     * @param field Field name
     * @param fileName File name
     */
    deleteImage(id: string, field: string, fileName: string): Observable<void> {
        return this._http.delete(this.getUrl() + '/delete_image/' + id + '/' + field + '/' + fileName).map(response => null);
    }

    /**
     * Create a new element
     *
     * @param entity Element
     */
    save(entity: AdBaseModel): Observable<String> {
        return this._http.post(this.getUrl(), entity, {
            responseType: 'text'
        });
    }

    /**
     * Calls the PUT method to update the entity
     *
     * @param model Entity to be updated
     */
    update(model: AdBaseModel): Observable<void | string> {
        // Initialize Params Object
        return this._http.put(this.getUrl() + '/' + model['id'], model, {
            responseType: 'text'
        }).map(response => {
            return response ? response as string : null;
        });
    }

    /**
     * Sort item list
     *
     * @param model Sort parameters
     */
    sort(model: SortParamModel): Observable<ControllerResultModel> {
        return this._http.post(this.getUrl() + '/sort', model).map(response => {
            return response as ControllerResultModel;
        });
    }

    /**
     * Save multiple records
     *
     * @param entity Sort parameters
     * @param entityIds Record identifier
     */
    saveMultiple(entity: AdBaseModel, entityIds: string[]): Observable<ControllerResultModel> {
        let params: HttpParams = new HttpParams();
        entityIds.forEach(id => params = params.append('ids', id));
        return this._http.post(this.getUrl() + '/save_multiple', entity, { params: params }).map(response => {
            return response as ControllerResultModel;
        });
    }

    /**
     * Get generic server URL
     */
    getUrl(): string {
        if (this._table.name !== 'ad_client') {
            const prefix = this._table.name.substring(0, this._table.name.indexOf('_'));
            const version = this._settings.settings.TABLE_PREFIX_VERSIONS.find(p => p === prefix);
            return this._settings.getRestHostPath() + this._table.restPath + '/' + this._table.name + '/' + this._settings.idClient +
                (version ? '/v' + this._table.version : '');
        } else {
            return this._settings.getRestHostPath() + this._table.restPath + '/' + this._table.name;
        }
    }

    /**
     * Get audit information
     *
     * @param idRow Row identifier
     */
    loadRowAudit(idRow: string): Observable<ControllerResultModel> {
        let params = new HttpParams();
        params = params.append('id', idRow);
        return this._http.get(this.getUrl() + '/row_audit', { params: params }).map(response => response as ControllerResultModel);
    }

    /**
     * Update Http Params list
     *
     * @param params Http params
     * @param sortOrder Sort items
     * @param limit Items by page
     * @param page Page number
     */
    private updateParams(params: HttpParams, sortOrder: SortData[], limit, page: number) : HttpParams {
        if (!isNullOrUndefined(sortOrder)) {
            params = params.append('sort', AdBaseService.getSortString(sortOrder));
        }
        if (!isNullOrUndefined(limit)) {
            params = params.append('limit', limit.toString());
        }
        if (!isNullOrUndefined(page)) {
            params = params.append('page', page.toString());
        }
        return params;
    }
}
