import {Injectable} from '@angular/core';
import localeEs from '@angular/common/locales/es';
import localeEn from '@angular/common/locales/en';
import localeIt from '@angular/common/locales/it';
import localeCa from '@angular/common/locales/ca';
import localeBr from '@angular/common/locales/br';
import localeFr from '@angular/common/locales/fr';
import localeDe from '@angular/common/locales/de';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {of} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {sprintf} from 'sprintf-js';
import {AuthService} from '../authentication/auth.service';
import {AppCoreSettings} from '../configs/app.core.settings';
import {AdTranslationService} from './ad.translation.service';
import {AdWindowDtoModel} from '../models/dto/ad.window.dto.model';
import {AdWindowService} from './ad.window.service';
import {AdReferenceDtoModel} from '../models/dto/ad.reference.dto.model';
import {AdReferenceService} from './ad.reference.service';
import {AdReferenceKeyValueModel} from '../models/dto/ad.reference.key.value.model';
import {AdPreferenceService} from './ad.preference.service';
import {AdPreferenceValueModel} from '../models/ad.preference.value.model';
import {DatePipe, DecimalPipe, registerLocaleData} from '@angular/common';
import {AdTabMainDtoModel} from '../models/dto/ad.tab.main.dto.model';
import {AdTabService} from './ad.tab.service';
import {AdColumnDtoModel} from '../models/dto/ad.column.dto.model';
import {AdTableService} from './ad.table.service';
import {AdTableDtoModel} from '../models/dto/ad.table.dto.model';
import {AdProcessDtoModel} from '../models/dto/ad.process.dto.model';
import {AdProcessService} from './ad.process.service';
import {ConditionalService} from './conditional.service';
import {AutogenService} from './autogen.service';

registerLocaleData(localeEn);
registerLocaleData(localeIt);
registerLocaleData(localeBr);
registerLocaleData(localeCa);
registerLocaleData(localeEn);
registerLocaleData(localeFr);
registerLocaleData(localeEs);
registerLocaleData(localeDe);

@Injectable({
    providedIn: 'root'
})
export class AppCacheService {
    private _cachedMessages: any;
    private _cachedLanguage: string;
    private _cachedTables: Map<string, AdTableDtoModel>;
    private _cachedProcess: Map<string, AdProcessDtoModel>;
    private _cachedWindows: Map<string, AdWindowDtoModel>;
    private _cachedPreferences: Map<string, AdPreferenceValueModel>;
    private _cachedReferences: Map<string, AdReferenceDtoModel>;
    private _tableDirValues: Map<string, AdReferenceKeyValueModel[]>;

    public onMessagesReloaded: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(
        private _authService: AuthService,
        private _translationService: AdTranslationService,
        private _settings: AppCoreSettings,
        private _referenceService: AdReferenceService,
        private _tableService: AdTableService,
        private _processService: AdProcessService,
        private _tabService: AdTabService,
        private _windowService: AdWindowService,
        private _preferenceService: AdPreferenceService,
        private _conditionalService: ConditionalService,
        private _autogenService: AutogenService
    ) {
        console.log('AppCacheService created ...');
        this.clearCache();
        this._authService.loginModified$.subscribe(() => {
            this._cachedPreferences = new Map<string, AdPreferenceValueModel>();
            if (this._authService.isLogged()) {
                this._preferenceService.preferences(this._authService.selectedRole).subscribe(result => {
                    result.forEach(current => this._cachedPreferences.set(current.property, current));
                });
                if (this._cachedLanguage !== this._settings.idLanguage) {
                    this.getMessages(true).then(response => {
                        console.log(`AppCacheService:: Message reloaded`);
                    });
                }
            }
        });
    }

    /**
     * Clear all cached information
     */
    clearCache(): void {
        this._cachedMessages = {};
        this._cachedTables = new Map<string, AdTableDtoModel>();
        this._cachedProcess = new Map<string, AdProcessDtoModel>();
        this._cachedWindows = new Map<string, AdWindowDtoModel>();
        this._tableDirValues = new Map<string, AdReferenceKeyValueModel[]>();
        this._cachedReferences = new Map<string, AdReferenceDtoModel>();
        this._cachedPreferences = new Map<string, AdPreferenceValueModel>();
        this._authService.clearCache();
    }

    /**
     * Retrieves the messages from the backend by calling the "messages" action
     */
    getMessages(ignoreCache: boolean = false): Promise<any> {
        return new Promise((resolve, reject) => {
            if (!ignoreCache && this._cachedMessages) {
                resolve(this._cachedMessages);
            } else {
                this._translationService.messages(this._settings.idLanguage).subscribe(result => {
                    this._cachedMessages = result;
                    this._cachedLanguage = this._settings.idLanguage;
                    resolve(result);
                }, error => {
                    reject(error);
                });
            }
        });
    }

    /**
     * Gets the translation searching in cached messages
     *
     * @param value Translation key
     * @param args Arguments
     */
    getTranslation(value: string, args: any[] = null): string {
        if (
            isNullOrUndefined(this._cachedMessages) || isNullOrUndefined(this._cachedMessages.messages)
            || isNullOrUndefined(this._cachedMessages.messages[value])
        ) {
            return value;
        }
        if (args && args.length > 0) {
            switch (args.length) {
                case 1:
                    return sprintf(this._cachedMessages.messages[value], args[0]);
                case 2:
                    return sprintf(this._cachedMessages.messages[value], args[0], args[1]);
                case 3:
                    return sprintf(this._cachedMessages.messages[value], args[0], args[1], args[2]);
                case 4:
                    return sprintf(this._cachedMessages.messages[value], args[0], args[1], args[2], args[3]);
                case 5:
                    return sprintf(this._cachedMessages.messages[value], args[0], args[1], args[2], args[3], args[4]);
                case 6:
                    return sprintf(this._cachedMessages.messages[value], args[0], args[1], args[2], args[3], args[4], args[5]);
            }
        }
        return sprintf(this._cachedMessages.messages[value]);
    }

    /**
     * Get parent Tab (Recursive)
     *
     * @param tabMain Main tab information
     * @param idTab Tab identifier
     */
    getParentTab(tabMain: AdTabMainDtoModel, idTab: string): AdTabMainDtoModel {
        const childTab: AdTabMainDtoModel = tabMain.childTabs ? tabMain.childTabs.find(tab => tab.idTab === idTab) : null;
        if (!childTab) {
            if (tabMain.childTabs) {
                for (const tab of tabMain.childTabs) {
                    const parentTab = this.getParentTab(tab, idTab);
                    if (parentTab) {
                        return parentTab;
                    }
                }
            }
        } else {
            return tabMain;
        }
        return null;
    }

    /**
     * Load table information
     *
     * @param idTable Table identifier
     * @param ignoreCache Ignore cache and force reload
     */
    loadTableInfo(idTable: string, ignoreCache: boolean = false): Observable<AdTableDtoModel> {
        if (this._cachedTables.has(idTable) && !ignoreCache) {
            return of(this._cachedTables.get(idTable));
        } else {
            return this._tableService.load(idTable).map(data => {
                this._cachedTables.set(idTable, data);
                return data;
            });
        }
    }

    /**
     * Get table information from cache
     *
     * @param idTable Table identifier
     */
    getTableInfo(idTable: string): AdTableDtoModel {
        return this._cachedTables.has(idTable) ? this._cachedTables.get(idTable) : null;
    }

    /**
     * Load process information
     *
     * @param idProcess Process identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getProcessInfo(idProcess: string, ignoreCache: boolean = false): Observable<AdProcessDtoModel> {
        if (this._cachedProcess.has(idProcess) && !ignoreCache) {
            return of(this._cachedProcess.get(idProcess));
        } else {
            return this._processService.load(idProcess).map(data => {
                this._cachedProcess.set(idProcess, data);
                return data;
            });
        }
    }

    /**
     * Load window information
     *
     * @param idWindow Window identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getWindowInfo(idWindow: string, ignoreCache: boolean = false): Observable<AdWindowDtoModel> {
        if (this._cachedWindows.has(idWindow) && !ignoreCache) {
            return of(this._cachedWindows.get(idWindow));
        } else {
            return this._windowService.load(idWindow).map(data => {
                this._cachedWindows.set(idWindow, data);
                return data;
            });
        }
    }

    /**
     * Get window information from application cache
     *
     * @param idWindow Window identifier
     */
    getWindowFromCache(idWindow: string): AdWindowDtoModel {
        return this._cachedWindows.get(idWindow);
    }

    /**
     * Remove window information from cache
     *
     * @param idWindow Window identifier
     */
    removeWindowFromCache(idWindow: string): void {
        this._cachedWindows.delete(idWindow);
    }

    /**
     * Load tab information (tab level > 0)
     *
     * @param idWindow Window identifier
     * @param idTab Tab identifier
     */
    getTabInfo(idWindow: string, idTab: string): Observable<AdTabMainDtoModel> {
        if (this._cachedWindows.has(idWindow)) {
            const window = this._cachedWindows.get(idWindow);
            const result: AdTabMainDtoModel = this.getTabModel(window.mainTab.childTabs, idTab);
            if (result) {
                if (result.tab) {
                    return of(result);
                } else {
                    return this._tabService.load(idWindow, idTab).map(data => {
                        this.setTabModel(window.mainTab.childTabs, data);
                        return data;
                    });
                }
            } else {
                return of(null);
            }
        } else {
            return of(null);
        }
    }

    /**
     * Get tab from child tabs (recursive)
     *
     * @param childTabs Child tab list
     * @param idTab Tab identifier
     */
    getTabModel(childTabs: AdTabMainDtoModel[], idTab: string): AdTabMainDtoModel {
        let result: AdTabMainDtoModel = childTabs.find(tab => tab.idTab === idTab);
        if (!result) {
            for (const tab of childTabs) {
                result = this.getTabModel(tab.childTabs, idTab);
                if (result) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Set tab to child tabs (recursive)
     *
     * @param childTabs Child tab list
     * @param tab Tab model
     */
    setTabModel(childTabs: AdTabMainDtoModel[], tab: AdTabMainDtoModel): boolean {
        if (childTabs) {
            const index = childTabs.findIndex(t => t.idTab === tab.idTab);
            if (index < 0) {
                for (const t of childTabs) {
                    if (this.setTabModel(t.childTabs, tab)) {
                        break;
                    }
                }
            }
            if (index >= 0) {
                childTabs[index] = tab;
                return true;
            }
        }
        return false;
    }

    /**
     * Load preference
     *
     * @param key Preference search key
     * @param ignoreCache Ignore cache and force reload
     */
    getPreference(key: string, ignoreCache: boolean = false): Observable<AdPreferenceValueModel> {
        if (!ignoreCache && this._cachedPreferences.has(key)) {
            return of(this._cachedPreferences.get(key));
        }
        return this._preferenceService.preference(key, this._authService.selectedRole).map(current => {
            this._cachedPreferences.set(key, current);
            return current;
        });
    }

    /**
     * Load preference value
     *
     * @param key Preference search key
     * @param defaultValue Default value
     */
    getPreferenceValue(key: string, defaultValue: any = null): any {
        const value = this._cachedPreferences.get(key);
        return value ? value : defaultValue;
    }

    /**
     * Get reference
     *
     * @param references Reference identifier
     * @param ignoreCache Ignore cache and force reload
     */
    getReferenceInfo(references: string[] = [], ignoreCache: boolean = false): Observable<AdReferenceDtoModel[]> {
        let nonCached, cached;
        if (ignoreCache) {
            nonCached = references;
            cached = [];
        } else {
            nonCached = references.filter(current => !this._cachedReferences.has(current));
            const cachedIds = references.filter(current => this._cachedReferences.has(current));
            cached = cachedIds.map(current => this._cachedReferences.get(current));
        }
        if (nonCached.length === 0) {
            return of(cached);
        } else {
            return this._referenceService.loadReferences(nonCached).map(refs => {
                refs.forEach(current => this._cachedReferences.set(current.idReference, current));
                return cached.concat(refs);
            });
        }
    }

    /**
     * Get references values
     *
     * @param reference Reference model
     * @param item Current item (entity)
     * @param ignoreCache Ignore cache and force reload
     */
    getReferenceValues(reference: AdReferenceDtoModel, item?: any, ignoreCache: boolean = false): Observable<AdReferenceKeyValueModel[]> {
        let keyReference = reference.idReference;
        if (reference.refTable && reference.refTable.sqlwhere) {
            keyReference += '_' + this._conditionalService.replaceSQLConditional(reference.refTable.sqlwhere, [], item);
        }

        if (!ignoreCache && this._tableDirValues.has(keyReference)) {
            return of(this._tableDirValues.get(keyReference));
        }

        switch (reference.rtype) {
            case 'TABLEDIR':
                return this._referenceService.loadTabledir([], reference, item).map(result => {
                    this._tableDirValues.set(keyReference, result);
                    return result;
                });
            case 'LIST':
                return of(reference.refList.map(current => {
                    const keyValue = new AdReferenceKeyValueModel();
                    keyValue.value = current.name;
                    keyValue.key = current.value;
                    return keyValue;
                }));
            default:
                return of([]);
        }
    }

    /**
     * Visualizes the transformed value of the reference
     *
     * @param key Reference key to transform
     * @param idReference Reference ID
     * @param row Table row value
     * @param column Column information
     */
    getReferenceValue(key: string, idReference: string, row: any = null, column: AdColumnDtoModel = null): Promise<string> {
        return new Promise((resolve, reject) => {
            this.getReferenceInfo([idReference]).subscribe(references => {
                const reference = references[0];
                const countrycode = this._settings.getItem('countryCode') || 'es';
                switch (reference.rtype) {
                    case 'YESNO':
                        const label = !key || key.toString() === false.toString() ? 'AD_labelNo' : 'AD_labelYes';
                        resolve(this.getTranslation(label));
                        break;

                    case 'LIST':
                    case 'TABLEDIR':
                        this.getReferenceValues(reference, row).map(values => {
                            const found = values.find(current => current.key === key);
                            if (!isNullOrUndefined(found)) {
                                return found.value;
                            }
                            return key;
                        }).subscribe(
                        value => resolve(value),
                        error => {
                            console.error(error);
                            reject(error);
                        });
                        break;

                    case 'TABLE':
                        let tableValue = null;
                        if (reference.refTable && row[reference.refTable.tableFldName]) {
                            tableValue = row[reference.refTable.tableFldName];
                        }
                        if (tableValue && key === tableValue[reference.refTable.displayName]) {
                            resolve(tableValue[reference.refTable.displayName]);
                        } else {
                            if (key && typeof key === 'string') {
                                this.loadTableInfo(reference.refTable.idTable).subscribe(table => {
                                    if (table) {
                                        const autogen = this._autogenService.bindedService(table);
                                        autogen.get(key).subscribe(data => {
                                            if (data && data[reference.refTable.displayName]) {
                                                resolve(data[reference.refTable.displayName]);
                                            } else {
                                                resolve(key);
                                            }
                                        });
                                    } else {
                                        resolve(key);
                                    }
                                });
                            } else {
                                resolve('');
                            }
                        }
                        break;

                    case 'PASSWORD':
                        resolve('*****');
                        break;

                    case 'NUMBER':
                        this.getPreference('NumberDecimalPlaces').subscribe(result => {
                                const parsed = +key;
                                const decimalPipe: DecimalPipe = new DecimalPipe(countrycode);
                                resolve(decimalPipe.transform(parsed, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'DATE':
                        this.getPreference('DateFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'TIME':
                        this.getPreference('TimeFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'TIMESTAMP':
                        this.getPreference('DateTimeFormat').subscribe(result => {
                                const datePipe: DatePipe = new DatePipe(countrycode);
                                resolve(datePipe.transform(key, result.value));
                            },
                            error => reject(error)
                        );
                        break;

                    case 'ID':
                    case 'IMAGE':
                    case 'FILEURL':
                    case 'FILE':
                    case 'TEXT':
                    case 'EMAIL':
                    case 'INTEGER':
                    case 'STRING':
                        resolve(key);
                        break;
                    default:
                        resolve(key);
                        break;
                }
            },
            error => {
                console.error(error);
                reject(error);
            });
        });
    }
}
