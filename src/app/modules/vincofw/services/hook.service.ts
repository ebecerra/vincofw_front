import {Injectable} from '@angular/core';
import {AuthService} from '../authentication/auth.service';
import {Hook} from '../models/hook';
import {isNullOrUndefined} from 'util';

export class AdHookNames {

    // When: In form editor after item values are loaded
    //
    // Arguments:
    //      tab: Tab information
    //      item: Current item values
    //      form: Form
    //      isNew: Is new item
    static readonly AD_AFTER_LOAD_ITEM = 'AdAfterLoadItem';

    // When: In form editor after tab is loaded
    //
    // Arguments:
    //      tab: Tab information
    static readonly AD_AFTER_LOAD_TAB = 'AdAfterLoadTab';

    // When: After a new item is created (Button '+'), before is shown in form editor
    //
    // Arguments:
    //      tableName: Table name
    //      tab: Tab information
    //      item: New empty item
    static readonly AD_AFTER_NEW_ITEM = 'AdAfterNewItem';

    // When: Before the item will be send to server
    //
    // Arguments:
    //      tableName: Table name
    //      tab: Tab information
    //      item: Item to be saved
    //      isNew: Is new item
    static readonly AD_BEFORE_SAVE_ITEM = 'AdBeforeSaveItem';

    // When: Button '+' is pressed
    //
    // Arguments:
    //      tab: Tab information
    //      origin: LIST or FORM
    // Return:
    //      If not successful the event is cancelled
    static readonly AD_CAN_NEW_ITEM = 'AdCanNewItem';

    // When: In form editor push save button
    //
    // Arguments:
    //      tableName: Table name
    static readonly AD_FORM_BUTTON_SAVE = 'AdFormButtonSave';

    // When: Need global parameters to conditional replacements (displaylogic, readonlylogig, defaultvalues)
    //
    // Arguments:
    //      parameters: Global parameters objects
    static readonly AD_GET_GLOBAL_PARAMETERS = 'AdGetGlobalParameters';

    // When: Editor list values must be change
    //
    // Arguments:
    //      fieldName: Field name
    //      values: New values
    static readonly AD_LIST_CHANGE_VALUES = 'AdListChangeValues';

    // When: Before list table values
    //
    // Arguments:
    //      tableName: Table name
    //      constraints: Constraints to be applied
    // Return:
    //      Value to be add to constraints or null
    static readonly AD_QUERY_TABLE = 'AdQueryTable';

    static readonly AD_BEFORE_LOAD_PREFERENCE = 'AdBeforeLoadPreference';
}

export class HookExecResult {
    success: boolean;
    results: any[];
}

@Injectable({
    providedIn: 'root'
})
export class HookService {

    hooks: Hook[] = [];

    constructor(private _authService: AuthService) {

    }

    /**
     * Register a Hook
     *
     * @param hook Hook
     */
    registerHook(hook: Hook): void {
        console.info('Register hook: ' + hook.identifier());
        const found = this.hooks.find(h => h.id === hook.id);
        if (!found) {
            this.hooks.push(hook);
        }
    }

    /**
     * Unregister a Hook
     *
     * @param id Hook identifier
     */
    unregisterHook(id: string): void {
        const indx = this.hooks.findIndex(hook => hook.id === id);
        if (indx >= 0) {
            console.info('Remove hook: ' + this.hooks[indx].identifier());
            this.hooks.splice(indx, 1);
        } else {
            console.error('Not found hook: ' + id);
        }
    }

    /**
     * Get a list of hooks
     *
     * @param name Hook name
     * @param args Arguments
     * @returns {Array}
     */
    getHooks(name: string, args: any): Promise<any>[] {
        const promises = [];
        this.hooks.forEach(hook => {
            if (hook.name === name) {
                promises.push(hook.handler(args).then());
            }
        });
        return promises;
    }

    /**
     * Excecute register hooks
     *
     * @param name Hook name
     * @param args Arguments
     */
    execHook(name, args): Promise<HookExecResult> {
        const execHooks = this.getHooks(name, args);
        return new Promise((resolve, reject) => {
            if (execHooks.length > 0) {
                Promise.all(execHooks)
                    .then(data => {
                        resolve({ success: true, results: data.filter(d => !isNullOrUndefined(d)) });
                    })
                    .catch(error => {
                        console.warn('Reject Hook: ' + name);
                        if (error) {
                            console.warn(error);
                        }
                        reject(error);
                    });
            } else {
                resolve({ success: true, results: [] });
            }
        });
    }

}
