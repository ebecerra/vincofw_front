import {Injectable} from '@angular/core';
import {Page} from '../models/page';
import {isNullOrUndefined} from 'util';
import {AppCoreSettings} from '../configs/app.core.settings';
import {SortData} from '../models/ad.base.model';

@Injectable({
    providedIn: 'root'
})
export class LocalstoreService {

    constructor(private _settings: AppCoreSettings) {

    }

    /**
     * Save filters and page information to local store
     *
     * @param idTab Tab identifier
     * @param filterValues Filter values
     * @param page Page information
     * @param sortData Sort column information
     */
    public saveTabInfo(idTab: string, filterValues: any, page: Page, sortData: SortData): void {
        const key = 'tabInfo_' + idTab;
        const value = this._settings.getItem(key);
        const info = !isNullOrUndefined(value) ? JSON.parse(value) : { filters: null, page: null, sortData: null };
        if (!isNullOrUndefined(filterValues)) {
            info.filters = { ...filterValues};
            Object.keys(info.filters).forEach(flt => {
                if (info.filters[flt] instanceof Object && info.filters[flt]._isAMomentObject) {
                    if (info.filters[flt].isValid()) {
                        info.filters[flt] = info.filters[flt].format('DD/MM/YYYY');
                    } else {
                        info.filters[flt] = '';
                    }
                }
            });
        }
        if (!isNullOrUndefined(page)) {
            info.page = page;
        }
        if (!isNullOrUndefined(sortData)) {
            info.sortData = sortData;
        }
        this._settings.setItem(key, JSON.stringify(info));
    }

    /**
     * Load filters from local store
     *
     * @param idTab Tab identifier
     */
    public loadFilters(idTab: string): any {
        const value = this._settings.getItem('tabInfo_' + idTab);
        return value ? JSON.parse(value).filters : { };
    }

    /**
     * Load page from local store (return a new page if not found)
     *
     * @param idTab Tab identifier
     */
    public loadPage(idTab: string): Page {
        const value = this._settings.getItem('tabInfo_' + idTab);
        const storeData = value ? JSON.parse(value) : null;
        return storeData && storeData.page ? storeData.page : new Page();
    }

    /**
     * Load sort from local store
     *
     * @param idTab Tab identifier
     */
    public loadSort(idTab: string): SortData {
        const value = this._settings.getItem('tabInfo_' + idTab);
        const storeData = value ? JSON.parse(value) : null;
        return storeData && storeData.sortData ? storeData.sortData : null;
    }
}
