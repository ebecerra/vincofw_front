import {Injectable} from '@angular/core';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {AppCoreSettings} from '../configs/app.core.settings';
import {GlobalEventsService} from './global.events.service';
import {environment} from '../../../../environments/environment';

class SocketStomp {
    idProcess: string;
    stompClient: any;
    stompServer = false;
}

@Injectable({
    providedIn: 'root'
})
export class SocketClientService {
    stomp: Map<string, SocketStomp> = new Map<string, SocketStomp>();

    constructor(
        private _settings: AppCoreSettings,
        private _globalEventsService: GlobalEventsService
    ) {
    }

    connect(idProcess: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            console.log('SocketClientService: Initialize WebSocket Connection');

            let socketStomp = this.stomp.get(idProcess);
            if (socketStomp) {
                if (socketStomp.stompClient) {
                    socketStomp.stompClient.disconnect(() => {
                        console.log('SocketClientService: Disconnected before connect');
                        socketStomp.stompClient = null;
                        socketStomp.stompServer = false;
                        this.serverConnect(idProcess, socketStomp, resolve, reject);
                    });
                } else {
                    this.serverConnect(idProcess, socketStomp, resolve, reject);
                }
            } else {
                socketStomp = {
                    idProcess: idProcess,
                    stompClient: null,
                    stompServer: false
                };
                this.stomp.set(idProcess, socketStomp);
                this.serverConnect(idProcess, socketStomp, resolve, reject);
            }
        });
    }

    disconnect(idProcess: string): void {
        const socketStomp = this.stomp.get(idProcess);
        if (socketStomp && socketStomp.stompClient !== null) {
            socketStomp.stompClient.disconnect(() => {
                console.log('SocketClientService: Disconnected');
            });
        } else {
            console.log('SocketClientService: Already disconnected');
        }
    }

    onMessageReceived(message): void {
        console.log('SocketClientService: Message -> ' + message.body);
        const log = JSON.parse(message.body);
        this._globalEventsService.notifyProcessLog(log);
    }

    private serverConnect(idProcess: string, socketStomp: SocketStomp, resolve, reject): void {
        let ws: any;
        let topic: string;
        const _this = this;

        // Connect success
        const onConnect = () => {
            console.log('SocketClientService: Connection OK');
            socketStomp.stompServer = true;
            socketStomp.stompClient.subscribe(topic, sdkEvent => {
                _this.onMessageReceived(sdkEvent);
            });
            resolve();
        };

        // Connect error
        const onError = error => {
            console.log('SocketClientService: WebSocket Connection Error -> ' + error);
            socketStomp.stompServer = true;
            reject(error);
        };

        // Close Event Callback
        const onCloseEvent = event => {
            console.log('SocketClientService: WebSocket Connection Close -> ' + event.type);
            if (!socketStomp.stompServer && event.type === 'close') {
                socketStomp.stompServer = true;
                reject();
            } else {
                socketStomp.stompClient = null;
            }
        };

        try {
            socketStomp.stompServer = false;
            if (this._settings.settings.PROCESS_MESSAGES === 'RabbitMQ') {
                topic = '/queue/ProcessQueueWeb_' + idProcess;
                ws = new WebSocket(environment.rabbitUrl);
                socketStomp.stompClient = Stomp.over(ws);
                socketStomp.stompClient.connect(this._settings.settings.PROCESS_RABBITMQ_USER, this._settings.settings.PROCESS_RABBITMQ_PASSWORD, onConnect, onError, onCloseEvent);
            } else {
                topic = '/topic/process.event';
                ws = new SockJS(this._settings.getRestHostPath() + 'websocket');
                socketStomp.stompClient = Stomp.over(ws);
                socketStomp.stompClient.connect({}, onConnect, onError, onCloseEvent);
            }
        }
        catch (e) {
            console.error(e);
            reject(e);
        }
    }
}
