import {Injectable} from '@angular/core';
import {ValidationResult} from '../models/validation.result.enum';
import {AdBaseModel, SortData} from '../models/ad.base.model';
import {isNullOrUndefined} from 'util';
import {AppCacheService} from './cache.service';
import {AdColumnDtoModel} from '../models/dto/ad.column.dto.model';
import {AdFieldDtoModel} from '../models/dto/ad.field.dto.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdReferenceKeyValueModel} from '../models/dto/ad.reference.key.value.model';
import {AdReferenceDtoModel} from '../models/dto/ad.reference.dto.model';
import * as FileSaver from 'file-saver';
import {AutogenService} from './autogen.service';
import {HttpParams} from '@angular/common/http';
import {AdTabDtoModel} from '../models/dto/ad.tab.dto.model';
import {AdTableActionDtoModel} from '../models/dto/ad.table.action.dto.model';
import {AdProcessParamDtoModel} from '../models/dto/ad.process.param.dto.model';
import {AuthService} from '../authentication/auth.service';
import {AdFormEditorModel} from '../models/ad.form.editor.model';
import {AppCoreSettings} from '../configs/app.core.settings';

import * as moment_ from 'moment';

const moment = moment_;

@Injectable({
    providedIn: 'root'
})
export class UtilitiesService  {

    constructor(
        private _authService: AuthService,
        private _cacheService: AppCacheService,
        private _autogenService: AutogenService,
        private _settings: AppCoreSettings
    ) {

    }

    public static TABLES = {
        AdColumn: 'ff80808150dba1800150dba69dee0018',
        AdField: 'ff80808151382f2a0151385c9296000d',
        AdFieldGroup: 'ff80808151382f2a0151385d18e20024',
        AdProcessLog: 'ff808081514cd18b01514ce7b60f0018'
    };

    private static S4(): string {
        return (Math.floor((1 + Math.random()) * 0x10000)).toString(16).substring(1).toLowerCase();
    }

    /**
     * Return boolean value for a string
     *
     * @param value String value
     */
    public getBooleanValue(value: string): boolean {
        if (isNullOrUndefined(value)) {
            return false;
        }
        const val = value.toUpperCase();
        return  val === 'Y' || val === 'S' || val === '1' || val === 'TRUE' || val === 'YES' || val === 'SI' || val === 'SÍ';
    }

    /**
     * Validates received value is a valid, non-zero value
     *
     * @param val Value to check
     * @returns ValidationResult
     */
    public validatePositiveInt(val: any): ValidationResult {
        if (!val || val === '') {
            return ValidationResult.EmptyValue;
        }
        val = val.trim();
        if (val.match(/^\d+$/) == null || isNaN(parseInt(val, 10))) {
            return ValidationResult.InvalidValue;
        }
        val = parseInt(val, 10);
        if (val === 0) {
            return ValidationResult.ZeroValue;
        }
        return ValidationResult.OKValue;
    }

    /**
     * Validates the entered value is a float number
     *
     * @param val
     * @returns ValidationResult
     */
    public validateFloat (val: any): ValidationResult {
        if (!val || val === '') {
            return ValidationResult.EmptyValue;
        }
        val = val.trim();
        if (val.match(/^\d*\.{0,1}\d*$/) != null || val.match(/^\d*,{0,1}\d*$/) != null) {
            val = parseFloat(val);
            if (isNaN(val)) {
                return ValidationResult.InvalidValue;
            }
            if (val === 0) {
                return ValidationResult.ZeroValue;
            }
            return ValidationResult.OKValue;
        } else {
            return ValidationResult.InvalidValue;
        }
    }

    /**
     * Format server errors
     *
     * @param errors Information error
     * @param allColumns Table fields
     * @param allFields All field list
     */
    public getValidationsErrors(errors: any, allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[]): string {
        let errMsg = '';
        if (errors) {
            try {
                if (typeof errors === 'string') {
                    errors = JSON.parse(errors);
                }
                let fieldErrors = errors.fieldErrors || [];
                let objectErrors = errors.objectErrors || [];
                if (!errors.fieldErrors && errors.id) {
                    const info = JSON.parse(errors.id);
                    fieldErrors = info.fieldErrors || [];
                    objectErrors = info.objectErrors || [];
                }
                if (fieldErrors.length === 0 && objectErrors.length === 0) {
                    errMsg = this._cacheService.getTranslation('AD_ErrValidationServerError');
                } else {
                    const msgCount = fieldErrors.length + objectErrors.length;
                    errMsg += msgCount > 1 ? '<ul>' : '';
                    fieldErrors.forEach(fldErr => {
                        const column = allColumns.find(col => {
                            return col.name === fldErr.field;
                        });
                        const extraMessageInfo = this.getServerFieldErrorExtraInfo(fldErr.message, column);
                        const field = column ? this.getFieldForColumn(column, allFields) : null;
                        errMsg +=
                            (msgCount > 1 ? '<li>' : '') +
                            this.getServerFieldError(fldErr.message, ['<b style="color: yellow">' + (field ? field.caption : fldErr.field) + '</b>']) +
                            (extraMessageInfo ? '<div>' + extraMessageInfo + '</div>' : '') +
                            (msgCount > 1 ? '</li>' : '');
                    });
                    objectErrors.forEach(objErr => {
                        let msg = '';
                        if (objErr.code === 'Text') {
                            msg = objErr.message;
                        } else if (objErr.code === 'Message') {
                            msg = this._cacheService.getTranslation(objErr.message);
                        } else {
                            this.getServerObjectError(objErr.code);
                        }
                        errMsg += (msgCount > 1 ? '<li>' : '') + msg + (msgCount > 1 ? '</li>' : '');
                    });
                    errMsg += msgCount > 1 ? '</ul>' : '';
                }
            } catch (e) {
                errMsg = this._cacheService.getTranslation('AD_ErrValidationServerError');
            }
        } else {
            errMsg = this._cacheService.getTranslation('AD_ErrValidationServerError');
        }
        return errMsg;
    }

    /**
     * Get field link to column
     *
     * @param column Column
     * @param allFields All field list
     */
    public getFieldForColumn(column: AdColumnDtoModel, allFields: AdFieldDtoModel[]): AdFieldDtoModel {
        return allFields.find(fld => fld.idColumn === column.idColumn);
    }

    /**
     * Get column by name
     *
     * @param name Column name
     * @param allColumns Column list
     */
    public getColumnByName(name: string, allColumns: any[]): AdColumnDtoModel {
        return allColumns.find((col: AdColumnDtoModel | AdProcessParamDtoModel) => col.name === name);
    }

    /**
     * Get sorted visible columns in grid
     *
     * @param allColumns Column list
     * @param allFields Field list
     * @param actions Table actions list
     */
    public getGridColumns(allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[], actions: AdTableActionDtoModel[]): AdColumnDtoModel[] {
        const visibleFields = allFields.filter(field => {
            return field.showingrid;
        });
        let visibleColumns = allColumns.filter(column => {
            return visibleFields.find(field => {
                return field.idColumn === column.idColumn;
            });
        });
        visibleColumns = visibleColumns.sort((first, second) => {
            if (first.relatedField.gridSeqno < second.relatedField.gridSeqno) {
                return -1;
            }
            if (first.relatedField.gridSeqno > second.relatedField.gridSeqno) {
                return 1;
            }
            return 0;
        });
        if (actions && actions.length > 0) {
            const actionsField = new AdFieldDtoModel();
            actionsField.caption = this._cacheService.getTranslation('AD_labelColumnActions');
            actionsField.sortable = false;
            const actionsRef = new AdReferenceDtoModel({
                idReference: '$actions$',
                rtype: 'ACTIONS'
            });
            const actionsCol = new AdColumnDtoModel();
            actionsField.relatedColumn = actionsCol;
            actionsCol.relatedField = actionsField;
            actionsCol.name = '$actions$';
            actionsCol.reference = actionsRef;
            visibleColumns.push(actionsCol);
        }
        return visibleColumns;
    }

    /**
     * Get name for identifier columns in grid
     *
     * @param allColumns Column list
     * @param allFields Field list
     */
    public getIdentifierColumn(allColumns: AdColumnDtoModel[], allFields: AdFieldDtoModel[]): string {
        let field;
        if (this.getColumnByName('identifier', allColumns)) {
            field = 'identifier';
        } else if (this.getColumnByName('name', allColumns)) {
            field = 'name';
        } else if (this.getColumnByName('value', allColumns)) {
            field = 'value';
        } else {
            const columns = this.getGridColumns(allColumns, allFields, null);
            field = columns[0].name;
        }
        return field;
    }

    /**
     * Get extra information for server field error
     *
     * @param error Error
     * @param column Column information
     */
    private getServerFieldErrorExtraInfo(error: string, column: AdColumnDtoModel): string {
        const errors = error.split('.');
        switch (errors[0]) {
            case 'Size':
                let message = '';
                switch (column.ctype){
                    case 'STRING':
                        if (column.lengthMin) {
                            message += ('<div>' + this._cacheService.getTranslation('AD_ErrValidationSizeLengthMin', [column.lengthMin]) + '</div>');
                        }
                        if (column.lengthMax) {
                            message += ('<div>' + this. _cacheService.getTranslation('AD_ErrValidationSizeLengthMax', [column.lengthMax]) + '</div>');
                        }
                        break;
                    default:
                        if (column.relatedField.valuemin) {
                            message += ('<div>' + this._cacheService.getTranslation('AD_ErrValidationSizeMin', [column.relatedField.valuemin]) + '</div>');
                        }
                        if (column.relatedField.valuemax) {
                            message += ('<div>' + this._cacheService.getTranslation('AD_ErrValidationSizeMax', [column.relatedField.valuemax]) + '</div>');
                        }
                        break;
                }
                return message;
            default:
                return '';
        }
    }

    /**
     * Get server field error
     *
     * @param error Error
     * @param args Message arguments
     */
    private getServerFieldError(error: string, args: any[]): string {
        const errors = error.split('.');
        switch (errors[0]) {
            case 'NotNull':
                return this._cacheService.getTranslation('AD_ErrValidationNotNull', args);
            case 'Email':
                return this._cacheService.getTranslation('AD_ErrValidationEmail', args);
            case 'Size':
                return this._cacheService.getTranslation('AD_ErrValidationSize', args);
            case 'Unique':
                return this._cacheService.getTranslation('AD_ErrValidationUnique', args);
            case 'FkNotFound':
                return this._cacheService.getTranslation('AD_ErrValidationFkNotFound', args);
            case 'FkNotDelete':
                return this._cacheService.getTranslation('AD_ErrValidationFkNotDelete', args);
            case 'DataTruncation':
                return this._cacheService.getTranslation('AD_ErrValidationDataTruncation', args);
            case 'UploadFile':
                return this._cacheService.getTranslation('AD_ErrValidationUploadFile', args);
            case 'UploadFileSize':
                args.push(errors[3]);
                return this._cacheService.getTranslation('AD_ErrValidationUploadFileSize', args);
            case 'UploadFileType':
                args.push(errors[3]);
                return this._cacheService.getTranslation('AD_ErrValidationUploadFileType', args);
            case 'CheckValue':
                return this._cacheService.getTranslation(errors[3], args);
            case 'InvalidValue':
                return this._cacheService.getTranslation(errors[3], errors.splice(4, errors.length - 3));
        }
        return error;
    }

    /**
     * Get server object error
     *
     * @param error Error
     */
    private getServerObjectError(error: string): string {
        switch (error) {
            case 'Unique':
                return this._cacheService.getTranslation('AD_ErrValidationContraintUnique');
            case 'InvalidValue':
                return this._cacheService.getTranslation('AD_ErrValidationInvalidValue');
            case 'Unknow':
                return this._cacheService.getTranslation('AD_ErrValidationUnknow');
        }
        return error;
    }

    /**
     * Build form control
     *
     * @param rtype Reference type
     * @param mandatory Mandatory validator
     * @param valuemin Min value validator
     * @param valuemax Max value validator
     * @param lengthMin Min length validator
     * @param lengthMax Max length validator
     */
    public buildFormControl(rtype: string, mandatory: boolean, valuemin: number, valuemax: number, lengthMin: number, lengthMax: number): FormControl {
        const validators = [];
        const formCtrl = new FormControl();
        // Validator: Required
        if (mandatory && rtype !== 'PASSWORD') {
            validators.push(Validators.required);
        }
        // Validator: Min and Max
        if (rtype === 'INTEGER' || rtype === 'NUMBER') {
            if (!isNullOrUndefined(valuemin)) {
                validators.push(Validators.min(Number(valuemin)));
            }
            if (!isNullOrUndefined(valuemax)) {
                validators.push(Validators.max(Number(valuemax)));
            }
        }
        // Validator: Min length and Max length
        if (rtype === 'STRING' || rtype === 'TEXT' || rtype === 'HTML' || rtype === 'EMAIL' || rtype === 'PASSWORD') {
            if (!isNullOrUndefined(lengthMin)) {
                validators.push(Validators.minLength(lengthMin));
            }
            if (!isNullOrUndefined(lengthMax)) {
                validators.push(Validators.maxLength(lengthMax));
            }
        }
        if (validators.length > 0) {
            formCtrl.setValidators(validators);
        }
        return formCtrl;
    }

    /**
     * Get moment date from value
     *
     * @param value
     */
    public getMomentDate(value): any {
        if (typeof value === 'number') {
            return moment(value);
        }
        if (typeof value === 'string') {
            if (value.indexOf('T') === 10) {
                return moment(value.substring(0, 19), 'YYYY-MM-DDTHH:mm:ss');
            }
            if (value.indexOf('/') > 0) {
                return moment(value, value.length > 10 ? 'DD/MM/YYYY HH:mm:ss' : 'DD/MM/YYYY');
            }
            return moment(value);
        }
        if (value instanceof Date) {
            return moment(value);
        }
        return value;
    }

    /**
     * Get value using the refence type
     *
     * @param reference Reference
     * @param value Value
     */
    public getValueByReference(reference: AdReferenceDtoModel, value): string {
        switch (reference.rtype) {
            case 'DATE':
                return this.getMomentDate(value).format('DD/MM/YYYY');

            case 'TIME':
            case 'TIMESTAMP':
                return this.getMomentDate(value).format('DD/MM/YYYY HH:mm:ss');

            case 'PASSWORD':
                return '*******';
        }
        return value;
    }

    /**
     * Get form value to send to server
     *
     * @param value Form value
     * @param field Related field
     */
    public getFormValue(value: any, field: AdFieldDtoModel): any {
        if (isNullOrUndefined(value)) {
            return null;
        }
        const reference = field.relatedColumn.reference;
        switch (reference.rtype) {
            case 'DATE':
                return this.getMomentDate(value).format('YYYY-MM-DD');

            case 'TIME':
            case 'TIMESTAMP':
                return this.getMomentDate(value).format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';

            case 'PASSWORD':
                return  this._authService.encryptPassword(this._authService.getLoggedIdClient() + '/' + this._authService.getLoggedIdUser(), value);

            case 'LIST':
                if (reference.multiple) {
                    if (field.readonly) {
                        return value;
                    }
                    return value.join(',');
                } else {
                    if (field.readonly) {
                        const refValue = reference.refList.find(rl => rl.name === value);
                        if (refValue) {
                            return refValue.value;
                        }
                    }
                    return value;
                }

            case 'TABLE':
            case 'TABLEDIR':
                return value && value.trim() !== '' ? value : null;

        }
        if (this.isEmptyValue(value) && !field.relatedColumn.mandatory) {
            value = null;
        }
        return value;
    }

    /**
     * Set form value
     *
     * @param autogenForm Form
     * @param key Form field
     * @param reference Reference
     * @param readonly Read only field
     * @param currentItem Current form values
     * @param relatedColumn Related column
     */
    public setFormValue(autogenForm: FormGroup, key: string, reference: AdReferenceDtoModel, readonly: boolean, currentItem: any, relatedColumn: AdColumnDtoModel): void {
        switch (reference.rtype) {
            case 'DATE':
            case 'TIME':
            case 'TIMESTAMP':
                if (readonly) {
                    autogenForm.get(key).setValue(this.getMomentDate(currentItem[key]).format(reference.rtype === 'DATE' ? 'DD/MM/YYYY' : 'DD/MM/YYYY HH:mm:ss'));
                } else {
                    autogenForm.get(key).setValue(this.getMomentDate(currentItem[key]));
                }
                break;

            case 'PASSWORD':
                autogenForm.get(key).setValue('');
                break;

            case 'TABLE':
                if (typeof currentItem[key] === 'string') {
                    this._cacheService.getReferenceValue(currentItem[key], reference.idReference, currentItem, relatedColumn).then(value => {
                        if (readonly) {
                            autogenForm.get(key).setValue(value);
                        } else {
                            const option = new AdReferenceKeyValueModel();
                            option.key = currentItem[key];
                            option.value = currentItem[key] ? value : '';
                            autogenForm.get(key).setValue(option);
                        }
                    });
                } else {
                    autogenForm.get(key).setValue(currentItem[key]);
                }
                break;

            case 'TABLEDIR':
            case 'LIST':
                if (readonly) {
                    this._cacheService.getReferenceValue(currentItem[key], reference.idReference, currentItem, relatedColumn).then(value => {
                        autogenForm.get(key).setValue(value);
                    });
                } else {
                    if (reference.multiple && currentItem[key]) {
                        autogenForm.get(key).setValue(currentItem[key].split(','));
                    } else {
                        autogenForm.get(key).setValue(currentItem[key]);
                    }
                }
                break;

            default:
                autogenForm.get(key).setValue(currentItem[key]);
        }
    }

    /**
     * Check if value is empty
     *
     * @param value Value
     */
    public isEmptyValue(value: any): boolean {
        if (!isNullOrUndefined(value)) {
            if (typeof value === 'string') {
                return value.trim() === '';
            } if (value instanceof moment) {
                return !moment(value).isValid();
            }
            return false;
        }
        return true;
    }

    /**
     * Get filters condition
     *
     * @param filters Filters
     * @param columns Column references
     * @param strict Allways filter exact value
     */
    public getFilterParams(filters: any, columns: any[], strict: boolean = false): string {
        let q = '';
        if (!isNullOrUndefined(filters)) {
            const keys = Object.keys(filters);
            // Single filters
            keys.forEach(current => {
                if (!this.isEmptyValue(filters[current])) {
                    let value = filters[current];
                    if (current.indexOf('$from$') === -1 && current.indexOf('$to$') === -1) {
                        if (current === 'search') {
                            value = strict ? value : '%' + value + '%';
                        } else {
                            const column = this.getColumnByName(current, columns);
                            if (!isNullOrUndefined(column)) {
                                switch (column.reference.rtype) {
                                    case 'STRING':
                                    case 'TEXT':
                                    case 'HTML':
                                        value = strict ? value : '%' + value + '%';
                                        break;

                                    case 'TABLE':
                                        value = typeof value === 'string' ? value : value.key;
                                        break;

                                    case 'DATE':
                                        value = this.getMomentDate(value).format('DD/MM/YYYY');
                                        break;

                                    case 'TIME':
                                    case 'TIMESTAMP':
                                        value = this.getMomentDate(value).format('DD/MM/YYYYTHH:mm:ss');
                                        break;

                                    default:
                                        if (column.relatedField && column.relatedField.filterType === 'MULTISELECT'
                                            && (column.reference.rtype === 'LIST' || column.reference.rtype === 'TABLEDIR')
                                        ) {
                                            value = value.length === 0 ? null :  '[' + value.join(';') + ']';
                                        }
                                }
                            } else {
                                value = null;
                            }
                        }
                        if (!this.isEmptyValue(value)) {
                            q += (q === '' ? '' : ',') + current + '=' + value;
                        }
                    }
                }
            });
            // Range filters
            columns.filter(col => col.relatedField && col.relatedField.filterRange).forEach(column => {
                let value = null;
                const rangeFrom = filters['$from$' + column.name];
                const rangeTo = filters['$to$' + column.name];
                if (!this.isEmptyValue(rangeFrom) || !this.isEmptyValue(rangeTo)) {
                    if (!this.isEmptyValue(rangeFrom)) {
                        if (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP') {
                            value = this.getMomentDate(rangeFrom).format('DD/MM/YYYY') + '~';
                        } else {
                            value = rangeFrom + '~';
                        }
                    }
                    if (!this.isEmptyValue(rangeTo)) {
                        if (value === null) {
                            value = '~';
                        }
                        if (column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP') {
                            value += this.getMomentDate(rangeTo).format('DD/MM/YYYY');
                        } else {
                            value += rangeTo;
                        }
                    }
                    q += (q === '' ? '' : ',') + column.name + '=' + value;
                }
            });
        }
        return q;
    }

    /**
     * Check if column is filtered by range
     *
     * @param column Column information
     */
    public isColumnRangeFilter(column: AdColumnDtoModel): boolean {
        return column.relatedField.filterRange
            && (
                column.reference.rtype === 'DATE' || column.reference.rtype === 'TIMESTAMP' ||
                column.reference.rtype === 'INTEGER' || column.reference.rtype === 'NUMBER'
            );
    }

    /**
     * Download a file from server
     *
     * @param data Blob data from server
     * @param fileName Local file name
     */
    public downloadFile(data: any, fileName: string): void {
        const blob = new Blob([data], { type: data.type });
        FileSaver.saveAs(blob, fileName);
    }

    /**
     * Download a JSON from server
     *
     * @param data Blob data from server
     */
    public downloadJson(data: any): Promise<any> {
        return new Promise<any>((resolve) => {
            const reader = new FileReader();
            reader.onload = () => {
                resolve(JSON.parse(reader.result.toString()));
            };
            reader.readAsText(data);
        });
    }

    /**
     * Delay a timeout
     *
     * @param ms Timeout in mili seconds
     */
    public delay(ms: number): Promise<void> {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    /**
     * Add a field to tab
     *
     * @param tab Tab information
     * @param field Field information
     * @param column Column information
     */
    public addTabField(tab: AdTabDtoModel, field: AdFieldDtoModel, column: AdColumnDtoModel): void {
        if (column) {
            field.relatedColumn = column;
            tab.table.columns.push(column);
        }
        tab.fields.push(field);
        if (field.idFieldGroup === null) {
            tab.ungroupedFields.push(field);
        } else {
            const group = tab.fieldGroups.find(grp => grp.idFieldGroup === field.idFieldGroup);
            if (group) {
                group.fields.push(field);
            } else {
                console.error('Not found group: ' + field.idFieldGroup);
            }
        }
    }

    /** Load table content
     *
     * @param idTable Table identifier
     * @param params Query parameters
     * @param sortOrder Sort criteria
     */
    public loadTableContent(idTable: string, params: any, sortOrder?: SortData[]): Promise<AdBaseModel[]> {
        return new Promise((resolve, reject) => {
            this._cacheService.loadTableInfo(idTable).subscribe(table => {
                if (table) {
                    const autogen = this._autogenService.bindedService(table);
                    let parameters = new HttpParams();
                    Object.keys(params).forEach(key => {
                        parameters = parameters.append(key, params[key]);
                    });
                    autogen.list(parameters, sortOrder).subscribe(response => {
                        resolve(response.content);
                    });
                } else {
                    console.error('Not found table: ' + idTable);
                    reject(this._cacheService.getTranslation('AD_ProcessTableErrId', [idTable]));
                }
            });
        });
    }

    /**
     * Load table row
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     */
    public loadTableRow(idTable, idRow): Promise<any> {
        return new Promise((resolve, reject) => {
            if (isNullOrUndefined(idRow)) {
                resolve(null);
            }
            this._cacheService.loadTableInfo(idTable).subscribe(table => {
                if (table) {
                    const autogen = this._autogenService.bindedService(table);
                    autogen.get(idRow).subscribe(response => {
                        resolve(response);
                    });
                } else {
                    console.error('Not found table: ' + idTable);
                    reject(this._cacheService.getTranslation('AD_ProcessTableErrId', [idTable]));
                }
            });
        });
    }

    /**
     * Get item values to save
     *
     * @param form Form
     * @param item Model
     */
    public getItemToSave(form: FormGroup, item: any): any {
        form.updateValueAndValidity();
        Object.keys(form.controls).forEach(key => {
            form.controls[key].markAsTouched();
            form.controls[key].markAsDirty();
        });
        if (form.valid) {
            const itemToSave: any = {};
            Object.keys(item).forEach(key => {
                if (form.get(key) !== null) {
                    itemToSave[key] = form.value[key];
                } else {
                    itemToSave[key] = item[key];
                }
            });
            return itemToSave;
        }
        return null;
    }

    /**
     * Add field dependency
     *
     * @param editor Editor
     * @param deps Dependencies
     * @param property Dependency type
     */
    public addFieldDependency(editor: AdFormEditorModel | AdFieldDtoModel, deps: string[], property: string): void {
        deps.forEach(dep => {
            if (!editor[property].find(lnst => dep === lnst)) {
                editor[property].push(dep);
            }
        });
    }

    /**
     * Initialize form with item model values
     *
     * @param form Form
     * @param item Model
     */
    public initFormValues(form: FormGroup, item: any): void {
        const value = {};
        Object.keys(form.controls).forEach(key => {
            value[key] = item[key];
        });
        form.patchValue(value);
    }


    /**
     * Generates a UUID
     *
     * @returns {string}
     */
    public generateUID(): string {
        let array, uuid = '', i, digit = '';
        if (window.crypto && window.crypto.getRandomValues) {
            array = new Uint8Array(16);
            window.crypto.getRandomValues(array);

            for (i = 0; i < array.length; i++) {
                digit = array[i].toString(16).toLowerCase();
                if (digit.length === 1) {
                    digit = '0' + digit;
                }
                uuid += digit;
            }

            return uuid;
        }

        return (UtilitiesService.S4() + UtilitiesService.S4() + UtilitiesService.S4() + UtilitiesService.S4() +
            UtilitiesService.S4() + UtilitiesService.S4() + UtilitiesService.S4() + UtilitiesService.S4());
    }

}
