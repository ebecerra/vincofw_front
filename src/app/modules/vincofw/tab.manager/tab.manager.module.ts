import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatCheckboxModule,
    MatDatepickerModule,
    MatIconModule,
    MatSnackBarModule,
    MatTabsModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {TabManagerComponent} from './tab.manager/tab.manager.component';
import {AutogenModule} from '../autogen/autogen.module';
import {ExtensionsModule} from '../../../extensions/extensions.module';
import {VincofwModule} from '../vincofw.module';
import {CustomModule} from '../../../extensions/custom/custom.module';

@NgModule({
    declarations: [
        TabManagerComponent
    ],
    entryComponents: [],
    imports: [
        MatSnackBarModule,
        AutogenModule,
        CommonModule,
        FormsModule,
        FlexLayoutModule,
        MatIconModule,
        MatTabsModule,
        MatCheckboxModule,
        MatDatepickerModule,
        VincofwModule,
        ExtensionsModule,
        CustomModule
    ],
    providers: [
    ],
    exports: [
        TabManagerComponent
    ]
})

export class TabManagerModule {
}
