import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {NotificationService} from '../../notification/notification.service';
import {AdTabmanagerTabDtoModel} from '../../models/dto/ad.tabmanager.tab.dto.model';
import {EventProcessOpenModel} from '../../models/event/event.process.execute.model';
import {AutogenService} from '../../services/autogen.service';
import {AppCacheService} from '../../services/cache.service';
import {AdMenuService} from '../../services/ad.menu.service';
import {GlobalEventsService} from '../../services/global.events.service';
import {ProcessDialogComponent} from '../../autogen/process.dialog/process.dialog.component';

@Component({
    selector: 'tab-manager',
    templateUrl: './tab.manager.component.html',
    styleUrls: ['./tab.manager.component.scss'],
    providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class TabManagerComponent implements OnInit, OnDestroy {

    tabs: AdTabmanagerTabDtoModel[] = [];
    selected = new FormControl(0);
    subscriptions: Subscription[] = [];
    idMenu: string = null;
    pendingWindowToOpen: string = null;

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private location: Location,
        private _cacheService: AppCacheService,
        private _notificationService: NotificationService,
        private _autogenService: AutogenService,
        private _activatedRoute: ActivatedRoute,
        private _globalEventsService: GlobalEventsService,
        private _menuService: AdMenuService,
        private _dialog: MatDialog
    ) {
        console.info('TabManagerComponent: constructor');
        // Close all tabs
        this.subscriptions.push(
            this._globalEventsService.closeAllTabs$.subscribe(() => {
                this.tabs = [];
                this._router.navigate(['/']);
            })
        );
        // Load menu option
        this.subscriptions.push(
            this._globalEventsService.menuLoaded$.subscribe(() => {
                if (this.idMenu) {
                    this.loadMenuOption(this.idMenu);
                    this.idMenu = null;
                }
            })
        );
        // Reload a Window
        this.subscriptions.push(
            this._globalEventsService.reloadWindow$.subscribe(idWindow => {
                const tabIdx = this.getTabIndexByWindow(idWindow);
                if (tabIdx >= 0) {
                    this.closeTab(tabIdx, false);
                    const menu = this._menuService.getMenuByWindow(idWindow);
                    this.loadMenuOption(menu.idMenu);
                }
            })
        );
        // Open a process
        this.subscriptions.push(
            this._globalEventsService.processOpen$.subscribe((process: EventProcessOpenModel) => {
                const existingTabIdx = this.getTabIndex(process.idProcess);
                if (existingTabIdx === -1) {
                    const tab = new AdTabmanagerTabDtoModel();
                    tab.idTab = process.idProcess;
                    tab.mode = 'PROCESS';
                    this.openProcess(tab, process);
                } else {
                    this.selected.setValue(existingTabIdx);
                }
            })
        );
        // App started and ready
        this.subscriptions.push(
            this._globalEventsService.appStarted$.subscribe(() => {
                const path = this.location.path(true);
                const homeIndex = path.indexOf('/home');
                console.info('TabManagerComponent: App started with path: ' + path);
                if (homeIndex === 0 || homeIndex === 1) {
                    this._cacheService.getPreference('WindowOpenOnStart').subscribe(preference => {
                        if (preference.value) {
                            const menu = this._menuService.getMenuByWindow(preference.value);
                            if (menu) {
                                this.loadMenuOption(menu.idMenu);
                            } else {
                                this.pendingWindowToOpen = preference.value;
                            }
                        }
                    });
                }
            })
        );
        // Close all tabs
        this.subscriptions.push(
            this._globalEventsService.menuLoaded$.subscribe(() => {
                if (this.pendingWindowToOpen) {
                    const menu = this._menuService.getMenuByWindow(this.pendingWindowToOpen);
                    if (menu) {
                        this.loadMenuOption(menu.idMenu);
                    } else {
                        console.error('TabManagerComponent: User not have access to window: ' + this.pendingWindowToOpen);
                    }
                    this.pendingWindowToOpen = null;
                }
            })
        );

        // Route change event
        this._activatedRoute.paramMap.subscribe(paramMap => {
            if (paramMap.has('idMenu')) {
                if (this._menuService.isLoaded()) {
                    this.loadMenuOption(paramMap.get('idMenu'));
                } else {
                    this.idMenu = paramMap.get('idMenu');
                }
            } else {
                console.warn('Not found parameter: idMenu');
            }
        });
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    /**
     * Closes the given tab
     *
     * @param tabIdx Index of the tab to close
     * @param checkEmpty
     */
    closeTab(tabIdx: number, checkEmpty: boolean = true): void {
        this.tabs = this.tabs.filter((value, index) => {
            return index !== tabIdx;
        });
        if (checkEmpty) {
            if (this.tabs.length === 0) {
                this._router.navigate(['/']);
            } else {
                let selectedIndex = this.selected.value;
                if (selectedIndex >= this.tabs.length) {
                    selectedIndex = this.tabs.length - 1;
                }
                this._router.navigate(['tab', this.tabs[selectedIndex].idTab]);
            }
        }
    }

    /**
     * Tab change event
     *
     * @param event Tab index
     */
    selectedIndexChange(event): void {
        this.selected.setValue(event);
        this._router.navigate(['tab', this.tabs[event].idTab]);
    }

    /**
     * Load menu option
     *
     * @param idMenu Menu identifier
     */
    private loadMenuOption(idMenu: string): void {
        const menu = this._menuService.getMenu(idMenu);
        if (menu) {
            const existingTabIdx = this.getTabIndex(menu.idMenu);
            if (existingTabIdx === -1) {
                const tab = new AdTabmanagerTabDtoModel();
                tab.idTab = menu.idMenu;
                tab.mode = menu.action;
                switch (menu.action) {
                    case 'WINDOW':
                        // Tab for Windows
                        this._cacheService.getWindowInfo(menu.idWindow).subscribe(window => {
                            if (!isNullOrUndefined(window)) {
                                tab.window = window;
                                tab.caption = window.name;
                                this.tabs.push(tab);
                                this.selected.setValue(this.tabs.length - 1);
                            }
                        });
                        break;

                    case 'MANUAL':
                        // Tab for manual command menu option
                        const tokens = menu.manualCommand.split(':');
                        tab.manualCommand = menu.manualCommand;
                        if (tokens.length >= 2 ) {
                            tab.manualModule = tokens.shift();
                            tab.manualWindow = tokens.join(':');
                        } else {
                            tab.manualModule = 'unknow';
                            tab.manualWindow = 'unknow';
                        }
                        tab.caption = menu.name;
                        this.tabs.push(tab);
                        this.selected.setValue(this.tabs.length - 1);
                        break;

                    case 'PROCESS':
                        // Process
                        this.openProcess(tab, { idProcess: menu.idProcess, buttonType: 'PROCESS', item: null });
                        break;

                    default:
                        console.error('Not implemented menu action: ' + menu.action);
                }
            } else {
                this.selected.setValue(existingTabIdx);
            }
        } else {
            console.error('Not found menu: ' + idMenu);
        }
    }

    /**
     * Open process execution
     *
     * @param tab Tab information
     * @param processInfo Process information
     */
    private openProcess(tab: AdTabmanagerTabDtoModel, processInfo: EventProcessOpenModel): void {
        this._cacheService.getProcessInfo(processInfo.idProcess).subscribe(process => {
            if (!isNullOrUndefined(process)) {
                if (process.uipattern === 'DIALOG' || processInfo.buttonType === 'PROCESS_QUICK') {
                    this._dialog.open(ProcessDialogComponent, {
                        width: '70%',
                        disableClose: true,
                        data: {
                            process: process,
                            buttonType: processInfo.buttonType,
                            item: processInfo.item,
                        }
                    });
                } else {
                    tab.process = process;
                    tab.caption = process.name;
                    this.tabs.push(tab);
                    this.selected.setValue(this.tabs.length - 1);
                }
            }
        });
    }

    /**
     * Get tab index
     *
     * @param tabId Tab identifier
     */
    private getTabIndex(tabId: string): number {
        let existingTabIdx = -1;
        this.tabs.find((tab, idx) => {
            if (tab.idTab === tabId) {
                existingTabIdx = idx;
                return true;
            }
            return false;
        });
        return existingTabIdx;
    }

    /**
     * Get tab index
     *
     * @param idWindow Window identifier
     */
    private getTabIndexByWindow(idWindow: string): number {
        let existingTabIdx = -1;
        this.tabs.find((tab, idx) => {
            if (tab.window && tab.window.idWindow === idWindow) {
                existingTabIdx = idx;
                return true;
            }
            return false;
        });
        return existingTabIdx;
    }

}
