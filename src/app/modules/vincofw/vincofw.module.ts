import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HelpShowComponent} from './components/help.show/help.show.component';
import {VincoButtonComponent} from './components/vinco.button/vinco.button.component';
import {MenuSelectComponent} from './components/menu.select/menu.select.component';
import {PanelComponent} from './notification/panel/panel.component';
import {NotificationComponent} from './notification/notification/notification.component';
import {ConfirmComponent} from './notification/confirm/confirm.component';
import {ScrollToDirective} from './directives/scroll.to.directive';
import {MaterialModule} from '../../material.module';
import {PipesModule} from '../../pipes/pipes.module';
import {QuestionComponent} from './notification/question/question.component';
import {NgxJsonViewerComponent} from './components/externals/ngx-json-viewer/ngx-json-viewer.component';

@NgModule({
    declarations: [
        ScrollToDirective,
        NotificationComponent,
        ConfirmComponent,
        QuestionComponent,
        HelpShowComponent,
        VincoButtonComponent,
        MenuSelectComponent,
        PanelComponent,
        NgxJsonViewerComponent
    ],
    entryComponents: [
        ConfirmComponent,
        QuestionComponent,
        NotificationComponent,
        HelpShowComponent
    ],
    imports: [
        PipesModule,
        CommonModule,
        BrowserAnimationsModule,
        MaterialModule
    ],
    exports: [
        HelpShowComponent,
        VincoButtonComponent,
        MenuSelectComponent,
        PanelComponent,
        NgxJsonViewerComponent
    ],
    providers: [ ]
})
export class VincofwModule {
}
