import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Applications',
        translate: 'NAV.APPLICATIONS',
        type     : 'collapsable',
        relatedMenu: null,
        children : [
            {
                id       : 'sample',
                title    : 'Sample',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'collapsable',
                icon     : 'email',
                relatedMenu: null,
                children : [
                    {
                        id       : 'sample',
                        title    : 'Sample',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        icon     : 'email',
                        url      : '/sample',
                        relatedMenu: null,
                        badge    : {
                            title    : '25',
                            translate: 'NAV.SAMPLE.BADGE',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    }
                ]
            }
        ]
    },
    {
        id       : 'applications2',
        title    : 'Applications2',
        translate: 'NAV.APPLICATIONS',
        type     : 'collapsable',
        relatedMenu: null,
        children : [
            {
                id       : 'sample2',
                title    : 'Sample2',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'collapsable',
                icon     : 'email',
                relatedMenu: null,
                children : [
                    {
                        id       : 'sample3',
                        title    : 'Sample3',
                        translate: 'NAV.SAMPLE.TITLE',
                        type     : 'item',
                        icon     : 'email',
                        url      : '/sample2',
                        relatedMenu: null,
                        badge    : {
                            title    : '25',
                            translate: 'NAV.SAMPLE.BADGE',
                            bg       : '#F44336',
                            fg       : '#FFFFFF'
                        }
                    }
                ]
            }
        ]
    }
];
