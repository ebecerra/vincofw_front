import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {FuseProgressBarService} from '../../@fuse/components/progress-bar/progress-bar.service';
import {NotificationService} from '../modules/vincofw/notification/notification.service';
import {AppCacheService} from '../modules/vincofw/services/cache.service';

@Injectable()
export class RequestLoadingInterceptor implements HttpInterceptor {
    constructor(
        private _fuseProgressBarService: FuseProgressBarService,
        private _notificationService: NotificationService,
        private _appCacheService: AppCacheService,
        private _router: Router
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this._fuseProgressBarService.setMode('indeterminate');
        this._fuseProgressBarService.show();
        const that = this;
        return next.handle(req.clone()).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    that._fuseProgressBarService.hide();
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                that._fuseProgressBarService.hide();
                if (error.status === 201) {
                    // return of(error);
                }
                let msg = null;
                if (error.status === 403) {
                    msg = 'AD_GlobalUserNotAuth';
                } else if (error.status === 500) {
                    if (typeof error.error === 'string' && error.error.indexOf('com.auth0.jwt.exceptions.TokenExpiredException') > 0) {
                        that._router.navigate(['/login']);
                    } else {
                        msg = 'AD_GlobalServerConnectionError';
                    }
                } else {
                    msg = 'AD_GlobalServerConnectionError';
                }
                if (msg !== null) {
                    this._notificationService.showError(this._appCacheService.getTranslation(msg));
                }
                return throwError(error);
            })
        );
    }
}
