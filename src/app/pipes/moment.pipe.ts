import {Pipe, PipeTransform} from '@angular/core';
import * as moment_ from 'moment';
import {Moment} from 'moment';

const moment = moment_;

@Pipe({
    name: 'momentPipe'
})
export class MomentPipe implements PipeTransform {

    transform(value: Date|Moment, ...args: any[]): any {
        const [format] = args;
        return moment(value).format(format);
    }
}
