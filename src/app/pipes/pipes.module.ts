import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReferenceValuePipe} from './reference.value.pipe';
import {ReferenceRowValuePipe} from './reference.row.value.pipe';
import {TranslatePipe} from './translate.pipe';
import {SafeHtmlPipe} from './safe.html.pipe';
import {SafeStylePipe} from './safe.style.pipe';
import {MomentPipe} from './moment.pipe';

@NgModule({
    declarations: [
        ReferenceValuePipe,
        ReferenceRowValuePipe,
        TranslatePipe,
        SafeHtmlPipe,
        SafeStylePipe,
        MomentPipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ReferenceValuePipe,
        ReferenceRowValuePipe,
        TranslatePipe,
        SafeHtmlPipe,
        SafeStylePipe,
        MomentPipe
    ]
})
export class PipesModule {
}
